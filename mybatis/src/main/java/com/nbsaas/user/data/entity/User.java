package com.nbsaas.user.data.entity;

import com.baomidou.mybatisplus.extension.activerecord.Model;


public class User extends Model<User> {

    private Integer age;

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public static Object getId(Object o) {
        return o;
    }
}
