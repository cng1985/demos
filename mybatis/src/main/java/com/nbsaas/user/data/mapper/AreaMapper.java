package com.nbsaas.user.data.mapper;

import com.nbsaas.user.data.entity.Area;
import com.nbsaas.user.data.entity.AreaExample;
import java.util.List;

public interface AreaMapper {
    long countByExample(AreaExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Area record);

    int insertSelective(Area record);

    List<Area> selectByExample(AreaExample example);

    Area selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Area record);

    int updateByPrimaryKey(Area record);
}