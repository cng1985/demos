package com.nbsaas.user.app;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.nbsaas.user.data.entity.Area;
import com.nbsaas.user.data.entity.User;

import static com.baomidou.mybatisplus.core.toolkit.Wrappers.lambdaQuery;

public class MybatisApp {

    public static void main(String[] args) {

        UpdateWrapper<User> updateWrapper = new UpdateWrapper<>();
        updateWrapper.lambda().set(User::getAge, 20)
                .eq(User::getAge, 18);
        LambdaQueryWrapper<User> queryWrapper=new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getAge,18);
    }
    public static  <R> R getId(R o) {
        return o;
    }
}
