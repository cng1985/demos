package com.nbsaas.user.app;

import com.nbsaas.user.data.entity.User;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Test {

    public static <T, U extends Comparable<? super U>> Comparator<T> comparing(
            Function<? super T, ? extends U> keyExtractor)
    {
        Objects.requireNonNull(keyExtractor);
        return (Comparator<T> & Serializable)
                (c1, c2) -> keyExtractor.apply(c1).compareTo(keyExtractor.apply(c2));
    }
    public static <T, U> Function<T,U> test(
            Function<T,U> keyExtractor)
    {
        Objects.requireNonNull(keyExtractor);
        return keyExtractor;
    }

    public static void main(String[] args) {
        List<Integer> nums = new ArrayList<>();
        nums.add(1);
        nums.add(2);
        nums.add(3);
        nums.add(4);
        nums.add(5);
        nums.add(6);
        nums.add(7);

        Integer total = nums.stream().reduce(10, (a, b) -> a + b);
        System.out.println(total);
        nums.stream().reduce( (a, b) -> a + b);

        Map<Integer, List<Integer>> ls = nums.stream().collect(Collectors.groupingBy(item -> item % 3 ));

        nums.stream().sorted(Integer::compareTo);

        List<User> users=new ArrayList<>();
        users.add(new User());
        users.stream().sorted(Comparator.comparing(User::getAge));

        users.stream().map(User::getAge).collect(Collectors.toList());

        Comparator<User> s=  Test.comparing(User::getAge);
        Function<User, Integer> s1 = Test.test(User::getAge);
        Method[] fs=  s1.getClass().getMethods();
        for (Method f : fs) {
            if ("apply".equals(f.getName())){
                Parameter[] ps=f.getParameters();
                for (Parameter p : ps) {
                    System.out.println(p.getName());
                    System.out.println(p.getClass().getName());

                }
                Class<?>[] ts = f.getParameterTypes();
                for (Class<?> t : ts) {
                    System.out.println(t.getName());
                }

                Type[] tts = f.getGenericParameterTypes();
                for (Type tt : tts) {
                    System.out.println(tt.getTypeName());
                }
            }
        }
        Integer num = s1.apply(new User());
        System.out.println(num);

        Integer nu1=num;
        System.out.println(nu1.intValue());

    }
}
