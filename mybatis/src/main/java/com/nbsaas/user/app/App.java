package com.nbsaas.user.app;

import com.nbsaas.user.data.entity.Area;
import com.nbsaas.user.data.entity.AreaExample;
import com.nbsaas.user.data.mapper.AreaMapper;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.scripting.LanguageDriver;
import org.apache.ibatis.session.*;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        SqlSessionFactoryBuilder builder=new SqlSessionFactoryBuilder();
        Configuration config=new Configuration();
        PooledDataSource dataSource=new PooledDataSource();
        dataSource.setDriver("com.mysql.jdbc.Driver");
        dataSource.setUrl("jdbc:mysql://127.0.0.1:3306/demo1?characterEncoding=UTF-8");
        dataSource.setUsername("root");
        dataSource.setPassword("123.com");
        Environment event=new Environment("hello",new JdbcTransactionFactory(),dataSource);
        config.setEnvironment(event);
        config.addMappers("com.nbsaas.user.data.mapper");
        LanguageDriver languageDriver=config.getDefaultScriptingLanguageInstance();


        SqlSessionFactory factory=  builder.build(config);
        SqlSession session= factory.openSession(TransactionIsolationLevel.SERIALIZABLE);
        AreaMapper areaMapper= session.getMapper(AreaMapper.class);
        Area area=new Area();
        areaMapper.insert(area);
        area.setName("test");
        System.out.println(area.getId());
        session.commit();
        session.close();
    }
}
