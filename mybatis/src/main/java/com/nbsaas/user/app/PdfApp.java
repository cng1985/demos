package com.nbsaas.user.app;

import com.spire.pdf.FileFormat;
import com.spire.pdf.PdfDocument;
import com.spire.pdf.actions.PdfJavaScriptAction;

public class PdfApp {

    public static void main(String[] args) {
        PdfDocument pdf = new PdfDocument();
        pdf.loadFromFile("E:\\代码_20210609153836.pdf");

        //通过JavaScript设置过期时间，过期警告信息并关闭文档
        String javaScript = "var rightNow = new Date();"
                + "var endDate = new Date('June 9, 2021 15:45:59');"
                + "if(rightNow.getTime() > endDate)"
                + "app.alert('该文档已过期，请重新联系管理员获取！',1);"
                + "this.closeDoc();";

        //根据JS创建PdfJavaScriptAction
        PdfJavaScriptAction js = new PdfJavaScriptAction(javaScript);

        //将PdfJavaScriptAction设置为文档打开后的动作
        pdf.setAfterOpenAction(js);

        //保存文档
        pdf.saveToFile("E:\\ExpiryDate2.pdf", FileFormat.PDF);
        pdf.dispose();
    }
}
