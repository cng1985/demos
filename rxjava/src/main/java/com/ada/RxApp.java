package com.ada;

import io.reactivex.*;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.ReplaySubject;

import java.util.concurrent.Callable;


public class RxApp {

    public static void main(String[] args) {

        DemoObservable demoObservable=new DemoObservable();
        Observable x = Observable.create(demoObservable);
        x.subscribe(new Consumer() {
            @Override
            public void accept(Object o) throws Exception {
                System.out.println(o);
            }
        });
        demoObservable.post("1");
        demoObservable.post("2");
        demoObservable.post("3");
        demoObservable.post("4");


        Observable<String> observable=Observable.just("ada");
        observable.subscribe(new Observer<String>() {
            @Override
            public void onSubscribe(Disposable disposable) {
                System.out.println("onSubscribe");

            }

            @Override
            public void onNext(String s) {
                System.out.println("onNext:" + s);
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("onError");

            }

            @Override
            public void onComplete() {
                System.out.println("onComplete");

            }
        });
        observable.retry();

    }
}
