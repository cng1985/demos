package com.ada;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

public class HelloWorld {

    public static void main(String[] args) {
        Flowable.just("Hello world").subscribe(System.out::println);

        List<Integer> list=new ArrayList<>();
        list.add(1);
        list.add(3);
        list.add(-1);
        list.add(11);
        test(list.toArray(new Integer[list.size()]));
    }

    public static void test(Integer ...nums){
            Flowable.fromArray(nums).filter(item->item>0).subscribe(new Subscriber<Integer>() {
                @Override
                public void onSubscribe(Subscription subscription) {
                    subscription.request(1L);
                    //throw new RuntimeException("test");
                }

                @Override
                public void onNext(Integer item) {

                    System.out.println("Next: " + item);

                }

                @Override
                public void onError(Throwable error) {
                    System.err.println("Error: " + error.getMessage());
                }

                @Override
                public void onComplete() {
                    System.err.println("over" );

                }

            });
    }
}
