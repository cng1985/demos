package com.ada.consumer;

import com.ada.domain.User;
import io.reactivex.functions.Consumer;

public class UserConsumer implements Consumer<User> {
  @Override
  public void accept(User o) throws Exception {
    System.out.println("age:" + o.getAge());
  }
}
