package com.ada;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

public class FlowableApp {
    public static void main(String[] args) {

        Flowable.fromCallable(() -> {
            Thread.sleep(1000); //  imitate expensive computation
            return "Done";
        })
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.single())
                .subscribe(System.out::println, Throwable::printStackTrace);

        try {
            System.out.println("************");
            Thread.sleep(2000); // <--- wait for the flow to finish

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
