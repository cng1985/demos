package com.ada;

import java.util.Observable;
import java.util.Observer;

public class ObserverX implements Observer {
    @Override
    public void update(Observable o, Object arg) {

        System.out.println(arg);
    }

    public static void main(String[] args) {
        Observable observable=new Observable();
        observable.addObserver(new ObserverX());
        observable.notifyObservers("123");
        observable.notifyObservers("456");
        observable.hasChanged();
        observable.notifyObservers();
    }
}
