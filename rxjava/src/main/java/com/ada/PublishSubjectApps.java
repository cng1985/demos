package com.ada;

import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.PublishSubject;

/**
 * Created by ada on 2016/11/17.
 */
public class PublishSubjectApps {
    public static void main(String[] args) {

        Consumer<? super Object> observer1 = new Consumer<Object>() {
            @Override
            public void accept(@NonNull Object o) throws Exception {
                System.out.println(o);
            }
        };
//
        Consumer<? super Object> observer2 = new Consumer<Object>() {
            @Override
            public void accept(@NonNull Object o) throws Exception {
                System.out.println("xx:"+o);
            }
        };
//
        PublishSubject<Object> subject = PublishSubject.create();

//        // observer1 will receive all onNext and onCompleted events
        subject.subscribe(observer1);
        subject.onNext("one");
        subject.onNext("two");
//        // observer2 will only receive "three" and onCompleted
        subject.subscribe(observer2);
        subject.onNext("three");
        subject.onNext("xxx");
    }
}
