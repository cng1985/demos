package com.ada;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class DemoObservable implements ObservableOnSubscribe {

    private ObservableEmitter emitter;

    @Override
    public void subscribe(ObservableEmitter observableEmitter) throws Exception {

        this.emitter=observableEmitter;
    }

    public void post(String data){
        emitter.onNext(data);
    }
}
