package com.ada;


import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Consumer;
import io.reactivex.subjects.AsyncSubject;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        AsyncSubject<Object> subject = AsyncSubject.create();
        Consumer<? super Object> observer=new Consumer<Object>() {
            @Override
            public void accept(@NonNull Object o) throws Exception {
                System.out.println(o);
            }
        };
        subject.subscribe(observer);
        Flowable.just("ff","ss").subscribe(observer);

        subject.onNext("one");
        subject.onNext("two");
        subject.onNext("three");

        // observer will receive "three" as the only onNext event.
        subject.onNext("one1");
        subject.onNext("two2");
        subject.onNext("three3");
        subject.onNext("1");

    }

}
