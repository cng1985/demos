package com.ada;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.ReplaySubject;

/**
 * Created by ada on 2016/11/17.
 */
public class ReplaySubjectApps {
    public static void main(String[] args) {

        ReplaySubject<Object> subject = ReplaySubject.create();
        subject.subscribe(s-> System.out.println(s));
//        subject.onNext("one");
//        subject.onNext("two");
//        subject.onNext("three");
        subject.blockingSubscribe(new Observer<Object>() {
            @Override
            public void onSubscribe(Disposable disposable) {

            }

            @Override
            public void onNext(Object o) {

            }

            @Override
            public void onError(Throwable throwable) {

            }

            @Override
            public void onComplete() {

            }
        });
//
//        // both of the following will get the onNext/onCompleted calls from above
//        subject.subscribe(observer1);
//        subject.subscribe(observer2);
//
//        subject.onNext("threexxxx");
//        subject.onCompleted();
//        subject.subscribe(observer2);
    }
}
