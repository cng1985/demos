package com.ada;

import io.reactivex.Flowable;

public class PublisherApp {
  
  public static void main(String[] args) {
    // Publisher
    Flowable.just("1")
        .map(item -> Integer.valueOf(item))
        .subscribe(item -> System.out.println(item + 1));
    
    Flowable.concat(Flowable.just("2"), Flowable.just("3"))
        .subscribe(item -> System.out.println(item), item -> System.out.println(item));
  }
}
