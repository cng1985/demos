package com.ada.rx2;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

import java.util.concurrent.TimeUnit;

public class RangeApps {

    public static void main(String[] args) {

        Flowable.range(1,100);
        Flowable.interval(10, TimeUnit.MICROSECONDS).subscribe(item-> System.out.println(item));

    }
}
