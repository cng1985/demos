package com.ada.rx2;

import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class StringObservableOnSubscribe implements ObservableOnSubscribe<String> {

    private ObservableEmitter emitter;

    @Override
    public void subscribe(ObservableEmitter emitter) throws Exception {
        emitter.onNext("demo");
        this.emitter=emitter;
    }

    public void post(String demo){
        emitter.onNext(demo);
    }
}
