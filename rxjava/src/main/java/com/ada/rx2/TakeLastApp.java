package com.ada.rx2;

import io.reactivex.Flowable;

import java.util.concurrent.TimeUnit;

public class TakeLastApp {

    public static void main(String[] args) {

        System.out.println("*****************************");
        Flowable.just(1, 2, 3, 4, 5).takeLast(2).subscribe(item -> System.out.println(item));
        System.out.println("*****************************");
        Flowable.just(1, 2, 3, 4, 5).take(2).subscribe(item -> System.out.println(item));
        System.out.println("*****************************");
        Flowable.just(1, 2, 3, 4, 5).take(1, TimeUnit.SECONDS).subscribe(item -> System.out.println(item));
        System.out.println("*****************************");
        Flowable.just(1, 2, 3, 4, 5).takeLast(10000, TimeUnit.HOURS).subscribe(item -> System.out.println(item));

    }
}
