package com.ada.rx2;

import io.reactivex.Flowable;

import java.util.concurrent.TimeUnit;

public class SampleApp {
    public static void main(String[] args) {
        Flowable.fromArray(1, 2, 2, 4, 34, 52).sample(3L, TimeUnit.SECONDS).subscribe(item -> System.out.println(item));
    }
}
