package com.ada.rx2;

import io.reactivex.Flowable;

import java.util.concurrent.Callable;

public class FromCallableApp {
    public static void main(String[] args) {
      int xx=  Flowable.fromCallable(new Callable<String>() {
            @Override
            public String call() throws Exception {
                return "sdf";
            }
        }).map(x->x+"sds").to(x->1);
        System.out.println(xx);
    }
}
