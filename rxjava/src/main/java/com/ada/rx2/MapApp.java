package com.ada.rx2;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import java.util.concurrent.TimeUnit;

public class MapApp {
    public static void main(String[] args) {
        Flowable.fromArray(1, 2, 2, 4, 34, 52);
        StringObservableOnSubscribe subscribe = new StringObservableOnSubscribe();
        Observable<String> source = Observable.create(subscribe);

        source.subscribeOn(Schedulers.io())
                .sample(1, TimeUnit.SECONDS)
                .blockingSubscribe(
                        item -> System.out.println("onNext: " + item),
                        Throwable::printStackTrace,
                        () -> System.out.println("onComplete"));


        for (int i = 0; i < 10; i++) {
            subscribe.post("a" + i);
        }
    }
}
