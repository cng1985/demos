package com.ada;

import org.joda.time.DateTimeZone;
import org.joda.time.Instant;

import java.time.Duration;
import java.time.LocalDate;
import java.time.Period;

public class DateApp {

    public static void main(String[] args) {
        LocalDate now=LocalDate.now();
        LocalDate old= now.plusDays(-20L);

        Period period=   Period.between(old,now);
        System.out.printf(""+period.getDays());
        Instant instant=  Instant.now();

    }
}
