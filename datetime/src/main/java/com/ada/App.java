package com.ada;

import org.joda.time.Chronology;
import org.joda.time.DateTime;
import org.joda.time.chrono.CopticChronology;

import java.util.Calendar;
import java.util.Date;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );


        DateTime dateTime=new DateTime(2016,11,22,23,45);
        DateTime one=  dateTime.withDayOfWeek(1);
        System.out.println(dateTime.getDayOfWeek());

        System.out.println(dateTime.getWeekOfWeekyear());
        System.out.println(dateTime.getWeekyear());
        System.out.println(one.getDayOfWeek());

        Chronology coptic = CopticChronology.getInstance();

        Calendar calendar=Calendar.getInstance();
        calendar.set(Calendar.DAY_OF_WEEK,2);

        System.out.println(new DateTime(calendar.getTime()).toString("yyyy-MM-dd"));
        System.out.println(new DateTime(getMin(new Date())).toString("yyyy-MM-dd HH:mm:ss"));
        System.out.println(new DateTime(getMax(new Date())).toString("yyyy-MM-dd  HH:mm:ss"));
        System.out.println(one.withSecondOfMinute(2).toString("yyyy-MM-dd  HH:mm:ss"));
        System.out.println(one.withMillisOfSecond(999).toString("yyyy-MM-dd  HH:mm:ss SSS"));

        System.out.println(new DateTime(getMax(calendar.getTime())).toString("yyyy-MM-dd  HH:mm:ss"));
        calendar.add(Calendar.DAY_OF_YEAR,6);
        System.out.println(new DateTime(getMax(calendar.getTime())).toString("yyyy-MM-dd  HH:mm:ss"));

    }
    public static Date getMin(Date date){
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY,0);
        calendar.set(Calendar.MINUTE,0);
        calendar.set(Calendar.SECOND,1);
        return  calendar.getTime();
    }
    public static Date getMax(Date date){
        Calendar calendar=Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY,23);
        calendar.set(Calendar.MINUTE,59);
        calendar.set(Calendar.SECOND,59);
        return  calendar.getTime();
    }
}
