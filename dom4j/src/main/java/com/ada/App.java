package com.ada;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.util.Iterator;
import java.util.List;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws DocumentException {

        String file = App.class.getResource("/a.xml").getFile();
        System.out.println(file);
        // 创建saxreader对象
        SAXReader reader = new SAXReader();
        // 读取一个文件，把这个文件转换成Document对象
        Document document = reader.read(new File(file));
        // 获取根元素
        Element root = document.getRootElement();
        // 获取java元素标签 内的内容
        List list = root.selectNodes("user");
        for(Object o:list){
            System.out.println(o);
        }

        Element e = root.element("user");
        Iterator iterator = e.nodeIterator();
        while (iterator.hasNext()) {
            Object o = iterator.next();
            System.out.println(o);

        }
    }
}
