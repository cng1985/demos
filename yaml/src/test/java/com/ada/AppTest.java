package com.ada;

import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlReader;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Map;

/**
 * Unit test for simple App.
 */
public class AppTest
        extends TestCase {
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest(String testName) {
        super(testName);
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(AppTest.class);
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() {
        assertTrue(true);

        //E:\mvnspace\android-arsenal.com\projects\free.yml
       int length=0;
        YamlReader reader = null;
        try {
            reader = new YamlReader(new FileReader("E:\\mvnspace\\android-arsenal.com\\projects\\free.yml"));
            Object object = reader.read();
            System.out.println(object.getClass());
            Map map = (Map) object;
            ArrayList list = (ArrayList) map.get("categories");
            for (int i = 0; i < list.size(); i++) {
                Object o = list.get(i);
                Map mapx = (Map) o;
                System.out.println(mapx.get("name"));
                ArrayList items = (ArrayList) mapx.get("projects");
                for (int j = 0; j < items.size(); j++) {
                    Object o1 = items.get(j);
                    Map xxx = (Map) o1;
                    System.out.println(xxx.get("name"));
                    System.out.println(xxx.get("url"));
                    length++;
                }
            }
            System.out.println(length);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (YamlException e) {
            e.printStackTrace();
        }

    }
}
