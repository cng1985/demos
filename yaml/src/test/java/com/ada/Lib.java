package com.ada;

/**
 * Created by ada on 2017/4/27.
 */
public class Lib {

    private Catalog categories;

    public Catalog getCategories() {
        return categories;
    }

    public void setCategories(Catalog categories) {
        this.categories = categories;
    }
}
