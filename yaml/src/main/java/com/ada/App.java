package com.ada;

import com.esotericsoftware.yamlbeans.YamlException;
import com.esotericsoftware.yamlbeans.YamlWriter;

import java.io.StringWriter;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        StringWriter writerw=new StringWriter();
        YamlWriter writer = new YamlWriter(writerw);

        UserApp a=new UserApp();
        try {
            a.setName("ada");
            writer.write(a);
            writer.close();
            System.out.println(writerw.toString());
        } catch (YamlException e) {
            e.printStackTrace();
        }
    }
}
