package com.ada;

import java.io.Serializable;

/**
 * Created by ada on 2017/4/27.
 */
public class UserApp implements Serializable {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
