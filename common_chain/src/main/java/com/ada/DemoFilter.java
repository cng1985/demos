package com.ada;

import org.apache.commons.chain2.Filter;
import org.apache.commons.chain2.Processing;
import org.apache.commons.chain2.impl.ContextMap;

public class DemoFilter implements Filter<String,Object, ContextMap<String, Object>> {
  @Override
  public boolean postprocess(ContextMap<String, Object> context, Exception exception) {
    System.out.println("出错了"+exception);
    return true;
  }

  @Override
  public Processing execute(ContextMap<String, Object> context) {
    System.out.println("我是Demo");
    if (context!=null){
      //throw new RuntimeException("ff");
    }
    return Processing.CONTINUE;
  }
}
