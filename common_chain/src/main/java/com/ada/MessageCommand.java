package com.ada;

import org.apache.commons.chain2.Command;
import org.apache.commons.chain2.Processing;
import org.apache.commons.chain2.impl.ContextMap;

public class MessageCommand implements Command<String, Object, ContextMap<String, Object>> {
  @Override
  public Processing execute(ContextMap<String, Object> context) {
    Object msg = context.get("msg");
    System.out.println(msg);
    return Processing.CONTINUE;
  }
}
