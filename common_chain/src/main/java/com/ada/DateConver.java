package com.ada;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DateConver {


    private List<DateFormatCommand> commands=new ArrayList<>();

    public void addCommand(DateFormatCommand command){
        commands.add(command);
    }

    public Date conver(String timestr){
        Date result = null;
        for (DateFormatCommand command : commands) {
           result= command.conver(timestr);
           if (result!=null){
               System.out.println(command);
               return result;
           }
        }
        return result;
    }

    public static void main(String[] args) {
        DateConver conver=new DateConver();
        conver.addCommand(new DateFormatCommand("yyyy-MM-dd"));
        conver.addCommand(new DateFormatCommand("yyyy年MM月"));
        conver.addCommand(new DateFormatCommand("yyyy年MM"));
        conver.addCommand(new DateFormatCommand("yyyy-MM"));
        conver.addCommand(new DateFormatCommand("yyyy年"));
        conver.addCommand(new DateFormatCommand("yyyy"));
        System.out.println(conver.conver("2018/11").toLocaleString());
    }
}
