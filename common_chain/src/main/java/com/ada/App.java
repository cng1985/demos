package com.ada;

import org.apache.commons.chain2.Chain;
import org.apache.commons.chain2.impl.ChainBase;
import org.apache.commons.chain2.impl.ContextMap;

import java.text.DateFormat;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        System.out.println("Hello World!");

        Chain<String, Object, ContextMap<String, Object>> chain = new ChainBase<>();
        chain.addCommand(new CountCommand());
        chain.addCommand(new CountCommand());
        chain.addCommand(new MessageCommand());
        chain.addCommand(new DemoFilter());
        chain.addCommand(new DemoFilter());
        chain.addCommand(new DemoFilter());


        ContextMap<String, Object> map = new ContextMap<>();
        chain.execute(map);

        //DateFormat dateFormat=;
    }
}
