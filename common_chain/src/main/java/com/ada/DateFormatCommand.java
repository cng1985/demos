package com.ada;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateFormatCommand {

    public DateFormatCommand(String format) {
        this.format = format;
    }

    private String format;

    public Date conver(String timestr) {
        Date result = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        try {
            result = dateFormat.parse(timestr);
        } catch (ParseException e) {
            result = null;
        }
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DateFormatCommand that = (DateFormatCommand) o;

        return format != null ? format.equals(that.format) : that.format == null;
    }

    @Override
    public int hashCode() {
        return format != null ? format.hashCode() : 0;
    }

    @Override
    public String toString() {
        return "DateFormatCommand{" +
                "format='" + format + '\'' +
                '}';
    }
}
