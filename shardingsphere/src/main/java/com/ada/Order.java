package com.ada;

import lombok.Data;

import java.io.Serializable;


@Data
public class Order implements Serializable {

    private String status;

    private Integer user;

    private Long id;


}
