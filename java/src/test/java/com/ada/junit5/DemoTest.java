package com.ada.junit5;

import static org.junit.jupiter.api.Assertions.assertTrue;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.AssertionsKt;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class DemoTest {
  
  @BeforeAll
  public static void init() {
    
    System.out.println("init");
  }
  
  
  @BeforeEach
  public void  initData(){
    System.out.println("init each");
  }
  
  @Disabled
  @Test
  public void test1() {
    
    assertTrue(true, "hi");
  }
  
  @RepeatedTest(10)
  @Test
  void test2() {
    assertTrue(true, "hi");
  
  }
  
  @ParameterizedTest
  @ValueSource(strings = { "racecar", "radar", "able was I ere I saw elba" })
  void palindromes(String candidate) {
    assertTrue(isPalindrome(candidate));
  }
  
  private boolean isPalindrome(String candidate) {
    System.out.println(candidate);
    return true;
  }
}
