package com.ada.java8;

import java.util.Optional;

/**
 * @author 陈联高
 * @version 1.01 2017年02月127日
 */
public class OptionalApps {
    public static void main(String[] args) {
        Optional<String> optional = Optional.ofNullable("a");
        if (optional.isPresent()){
            System.out.println("is not null");
        }else{
            System.out.println("null");
        }
        optional.orElse("demo");
        System.out.println(optional.get());
        Optional<String> upperName=  optional.flatMap((r) -> Optional.of("ada"));
        System.out.println(upperName.get());
    }
}
