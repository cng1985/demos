package com.ada.guava;

import org.junit.Test;

import static com.google.common.base.MoreObjects.firstNonNull;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Created by ada on 2017/5/8.
 */
public class MoreObjectsTest {

    @Test
    public void testFirstNonNullBothNotNull(){
        String value = "foo";
        String returned = firstNonNull(value,"bar");
        assertThat(returned,is(value));
    }

    @Test
    public void testFirstNonNullFirstNull(){
        String value = "bar";
        String returned = firstNonNull(null,value);
        assertThat(returned,is(value));
    }

    @Test
    public void testFirstNonNullSecondNull(){
        String value = "bar";
        String returned = firstNonNull(value,null);
        assertThat(returned,is(value));
    }

    @Test(expected = NullPointerException.class)
    public void testBothNull(){
        //Never do this
        firstNonNull(null,null);
    }
}