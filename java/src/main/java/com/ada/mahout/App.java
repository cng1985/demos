package com.ada.mahout;

import lombok.SneakyThrows;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

import java.sql.SQLException;
import java.util.List;

public class App {
    @SneakyThrows
    public static void main(String[] args) {
        // MySQL数据库连接信息
        String databaseUrl = "jdbc:mysql://192.168.1.108:3306/demo";
        String databaseUser = "root";
        String databasePassword = "root";

        try {
            // 连接到数据库并创建DataModel
            DataModel model = new MySQLDataModel(databaseUrl, databaseUser, databasePassword);

            // 定义用户相似度计算方法
            UserSimilarity similarity = new PearsonCorrelationSimilarity(model);

            // 定义用户邻居算法，这里选择最近的 2 个邻居
            UserNeighborhood neighborhood = new NearestNUserNeighborhood(2, similarity, model);

            // 创建基于用户的推荐引擎
            UserBasedRecommender recommender = new GenericUserBasedRecommender(model, neighborhood, similarity);

            // 获取用户ID为1的推荐列表，最多推荐5个物品
            List<RecommendedItem> recommendations = recommender.recommend(1, 5);

            // 打印推荐结果
            for (RecommendedItem recommendation : recommendations) {
                System.out.println("推荐物品ID：" + recommendation.getItemID() + "，推荐评分：" + recommendation.getValue());
            }
        } catch (TasteException | SQLException e) {
            e.printStackTrace();
        }
    }
}
