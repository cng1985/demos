package com.ada.mahout;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class UserBasedRecommendationExample {
    public static void main(String[] args) {
        try {
            // 加载数据文件，每一行代表用户对物品的评分，格式为 userId, itemId, rating
            DataModel model = new FileDataModel(new File("E:\\datas\\a.csv"));


            printDatas(model.getUserIDs());
            printDatas(model.getItemIDs());



            // 定义用户相似度计算方法
            UserSimilarity similarity = new PearsonCorrelationSimilarity(model);

            // 定义用户邻居算法，这里选择最近的 2 个邻居
            UserNeighborhood neighborhood = new NearestNUserNeighborhood(3, similarity, model);

            // 创建基于用户的推荐引擎
            UserBasedRecommender recommender = new GenericUserBasedRecommender(model, neighborhood, similarity);

            // 获取用户ID为1的推荐列表，最多推荐5个物品
            long[] us = recommender.mostSimilarUserIDs(2, 11);
            for (long u : us) {
                System.out.println(u);
            }
            List<RecommendedItem> recommendations = recommender.recommend(2, 5);

            // 打印推荐结果
            for (RecommendedItem recommendation : recommendations) {
                System.out.println("推荐物品ID：" + recommendation.getItemID() + "，推荐评分：" + recommendation.getValue());
            }
        } catch (IOException | TasteException e) {
            e.printStackTrace();
        }
    }

    private static void printDatas(LongPrimitiveIterator users) {
        while (users.hasNext()) {
            Long next = users.next();
            System.out.println(next);
        }
        System.out.println("***********************************");
    }
}

