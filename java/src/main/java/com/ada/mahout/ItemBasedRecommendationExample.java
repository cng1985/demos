package com.ada.mahout;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.recommender.GenericItemBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.ItemBasedRecommender;
import org.apache.mahout.cf.taste.similarity.ItemSimilarity;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class ItemBasedRecommendationExample {
    public static void main(String[] args) {
        try {
            // Load data from CSV file (userId, itemId, rating)
            DataModel model = new FileDataModel(new File("E:\\datas\\a.csv"));

            // Define item similarity calculation method (Pearson correlation)
            ItemSimilarity similarity = new PearsonCorrelationSimilarity(model);

            // Create item-based recommender
            ItemBasedRecommender recommender = new GenericItemBasedRecommender(model, similarity);

            // Get recommended items for item with ID 1, maximum 5 recommendations
            List<RecommendedItem> recommendations = recommender.recommend(1, 5);

            // Print recommended items
            for (RecommendedItem recommendation : recommendations) {
                System.out.println("Recommended item ID: " + recommendation.getItemID() + ", Score: " + recommendation.getValue());
            }
        } catch (IOException | TasteException e) {
            e.printStackTrace();
        }
    }
}
