package com.ada.mahout;

import org.apache.mahout.cf.taste.common.Refreshable;
import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.common.FastIDSet;
import org.apache.mahout.cf.taste.impl.common.LongPrimitiveIterator;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.model.PreferenceArray;

import java.sql.*;
import java.util.Collection;
import java.util.NoSuchElementException;

public class MySQLDataModel implements DataModel {

    private final Connection connection;

    public MySQLDataModel(String databaseUrl, String databaseUser, String databasePassword) throws Exception {
        connection = DriverManager.getConnection(databaseUrl, databaseUser, databasePassword);
    }

    @Override
    public LongPrimitiveIterator getUserIDs() {
        // Implement this method to return an iterator over user IDs from your database
        try (
                PreparedStatement stmt = connection.prepareStatement("SELECT DISTINCT user_id FROM user_ratings");
                ResultSet rs = stmt.executeQuery()) {

            return new LongPrimitiveIterator() {
                @Override
                public void skip(int i) {

                }

                @Override
                public long nextLong() {
                    try {
                        if (rs.next()) {
                            return rs.getLong("user_id");
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    throw new NoSuchElementException();
                }

                @Override
                public long peek() {
                    try {
                        if (rs.isBeforeFirst() || rs.isAfterLast()) {
                            return 0L;
                        }
                        return rs.getLong("user_id");
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    throw new NoSuchElementException();
                }

                @Override
                public boolean hasNext() {
                    try {
                        return !rs.isAfterLast();
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    return false;
                }

                @Override
                public Long next() {
                    try {
                        if (rs.next()) {
                            return rs.getLong("user_id");
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    throw new NoSuchElementException();
                }
            };

        } catch (SQLException e) {
            e.printStackTrace();
        }

        throw new UnsupportedOperationException("Failed to create user ID iterator");
    }

    @Override
    public PreferenceArray getPreferencesFromUser(long userID) {
        // Implement this method to return user preferences (item IDs and ratings) from your database
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public FastIDSet getItemIDsFromUser(long userID) {
        // Implement this method to return item IDs that the user has interacted with
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public LongPrimitiveIterator getItemIDs() {
        // Implement this method to return an iterator over all item IDs from your database
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public PreferenceArray getPreferencesForItem(long itemID) {
        // Implement this method to return user preferences for a specific item from your database
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public Float getPreferenceValue(long userID, long itemID) {
        // Implement this method to return the preference value for a user-item pair from your database
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public Long getPreferenceTime(long userID, long itemID) {
        // Implement this method to return the timestamp of the preference for a user-item pair from your database
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public int getNumItems() {
        // Implement this method to return the total number of items from your database
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT COUNT(DISTINCT item_id) FROM user_ratings");
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int getNumUsers() {
        // Implement this method to return the total number of users from your database
        try {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT COUNT(DISTINCT user_id) FROM user_ratings");
            if (resultSet.next()) {
                return resultSet.getInt(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    @Override
    public int getNumUsersWithPreferenceFor(long l) throws TasteException {
        return 0;
    }

    @Override
    public int getNumUsersWithPreferenceFor(long l, long l1) throws TasteException {
        return 0;
    }

    @Override
    public void setPreference(long userID, long itemID, float value) {
        // Implement this method to set a user's preference in your database
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public void removePreference(long userID, long itemID) {
        // Implement this method to remove a user's preference from your database
        throw new UnsupportedOperationException("Not implemented yet");
    }

    @Override
    public boolean hasPreferenceValues() {
        return true; // Change to false if your data doesn't have preference values (e.g., implicit feedback)
    }

    @Override
    public float getMaxPreference() {
        return 5.0f; // Change to the actual maximum preference value in your data
    }

    @Override
    public float getMinPreference() {
        return 1.0f; // Change to the actual minimum preference value in your data
    }


    // Helper method to close the database connection
    public void closeConnection() {
        if (connection != null) {
            try {
                connection.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void refresh(Collection<Refreshable> collection) {

    }
}
