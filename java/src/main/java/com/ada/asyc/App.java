package com.ada.asyc;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

public class App {

    public static void main(String[] args) throws ExecutionException, InterruptedException {

        CompletableFuture<Void> future = CompletableFuture.runAsync(() -> {
            // 异步执行的任务

            for (int i = 0; i < 10; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
                System.out.println(Thread.currentThread().getName() + " : " + i);
            }

        });
        CompletableFuture<String> lef = CompletableFuture.supplyAsync(() -> {
            System.out.println("1111");
            return "ada";
        });

        System.out.println(lef.get());

       // future.get();
    }
}
