package com.ada.lang;

import java.math.BigDecimal;

public class MathApp {

    public static void main(String[] args) {

        BigDecimal num1=new BigDecimal("3157.00000");

        BigDecimal num2=new BigDecimal("3157.00");

        System.out.println(num1.compareTo(num2));
    }
}
