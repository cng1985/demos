package com.ada.lang;

import lombok.Data;
import lombok.ToString;

@Data
@ToString(callSuper = true)
public class Worker extends Person{

    private Integer age;
}
