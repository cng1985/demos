package com.ada.lang;

import org.apache.commons.lang3.builder.DiffBuilder;
import org.apache.commons.lang3.builder.DiffResult;
import org.apache.commons.lang3.builder.ToStringStyle;

public class App {

    public static void main(String[] args) {
        User u1 = new User();
        u1.setAge(18);
        u1.setName("ada");
        User u2 = new User();
        u2.setName("young");
        u2.setAge(18);
        DiffResult result = u1.diff(u2);
        System.out.println(result.toString(ToStringStyle.DEFAULT_STYLE));
        System.out.println(result.toString(ToStringStyle.MULTI_LINE_STYLE));
        System.out.println(result.toString(ToStringStyle.NO_FIELD_NAMES_STYLE));
        System.out.println(result.toString(ToStringStyle.SHORT_PREFIX_STYLE));
        System.out.println(result.toString(ToStringStyle.SIMPLE_STYLE));

        result.forEach(item->{
            System.out.println(item.toString());
        });
        System.out.println(result.getNumberOfDiffs());

    }
}
