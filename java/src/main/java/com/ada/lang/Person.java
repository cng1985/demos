package com.ada.lang;

import lombok.Data;

@Data
public class Person {

    private String name;
}
