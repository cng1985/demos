package com.ada.lang;

import org.springframework.beans.BeanUtils;

public class WorkerApp {

    public static void main(String[] args) {

        Worker worker=new Worker();
        worker.setName("ada");
        worker.setAge(18);
        Worker tempWorker=new Worker();
        BeanUtils.copyProperties(worker, tempWorker);
        System.out.println(tempWorker);

        Temper temper=new Temper();
        BeanUtils.copyProperties(worker, temper);
        System.out.println(temper);
    }
}
