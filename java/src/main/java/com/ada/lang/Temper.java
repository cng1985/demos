package com.ada.lang;

import lombok.Data;

@Data
public class Temper {

    private String name;

    private Integer age;
}
