package com.ada.lang;

import lombok.Data;
import org.apache.commons.lang3.builder.DiffBuilder;
import org.apache.commons.lang3.builder.DiffResult;
import org.apache.commons.lang3.builder.Diffable;
import org.apache.commons.lang3.builder.ToStringStyle;

@Data
public class User implements Diffable<User> {


    private String name;

    private Integer age;

    @Override
    public DiffResult diff(User user) {
        return new DiffBuilder(this, user, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("name", this.name, user.name)
                .append("age", this.age, user.age)
                .build();
    }
}
