package com.ada.solr;

import org.apache.solr.client.solrj.SolrClient;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.common.SolrInputDocument;

import java.io.IOException;

public class App {

    public static void main(String[] args) throws SolrServerException, IOException {

        String solrUrl = "http://localhost:8983/solr";
        SolrClient solrClient = new HttpSolrClient.Builder(solrUrl).build();

        SolrInputDocument doc = new SolrInputDocument();
        doc.addField("id", "2");
        doc.addField("title", "JuiceFS 社区版 v1.1-Beta 发布，新增五个实用功能");
        doc.addField("content", "这是一个功能丰富的版本，带来了许多实用的新功能和改进。在这个版本中我们新增了以下功能：");

        solrClient.add("demo", doc);
        solrClient.commit("demo");
    }
}
