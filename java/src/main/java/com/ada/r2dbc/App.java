package com.ada.r2dbc;

import io.r2dbc.spi.Connection;
import io.r2dbc.spi.ConnectionFactories;
import io.r2dbc.spi.ConnectionFactory;
import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;
import reactor.core.publisher.Mono;

public class App {

    public static void main(String[] args) {
        // Notice: the query string must be URL encoded
        ConnectionFactory connectionFactory = ConnectionFactories.get("r2dbcs:mysql://root:root@192.168.1.108:3306/cartonexchange_pre?");

// Creating a Mono using Project Reactor
        Mono.from(connectionFactory.create())
                .flatMapMany(connection -> connection
                        .createStatement("SELECT firstname FROM PERSON WHERE age > $1")
                        .bind("$1", 42)
                        .execute())
                .flatMap(result -> result
                        .map((row, rowMetadata) -> row.get("firstname", String.class)))
                .doOnNext(System.out::println)
                .subscribe();

    }
}
