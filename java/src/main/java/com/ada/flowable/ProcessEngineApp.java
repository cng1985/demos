package com.ada.flowable;

import org.flowable.engine.IdentityService;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngines;
import org.flowable.idm.api.Group;
import org.flowable.idm.api.User;

public class ProcessEngineApp {
    public static void main(String[] args) {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        IdentityService service = processEngine.getIdentityService();
        Group group = service.newGroup("ada");
        //service.saveGroup(group);
        User user = service.newUser("ada");
        user.setTenantId("1");
        user.setPassword("123");
        user.setFirstName("ada");
        service.setUserInfo("ada","ada1","1");

        service.setAuthenticatedUserId("ada");
        service.saveGroup(service.newGroup("managers"));
        service.createMembership("ada","managers");
    }
}
