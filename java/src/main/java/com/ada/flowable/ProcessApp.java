package com.ada.flowable;

import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngines;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.repository.ProcessDefinition;

import java.util.List;

public class ProcessApp {

    public static void main(String[] args) {
        ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();
        RepositoryService repositoryService = processEngine.getRepositoryService();

        List<ProcessDefinition> ps = repositoryService.createProcessDefinitionQuery().latestVersion()
                .list();
        for (ProcessDefinition p : ps) {
            System.out.println(p.getName());
        }
    }
}
