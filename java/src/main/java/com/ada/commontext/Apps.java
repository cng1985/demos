package com.ada.commontext;

import org.apache.commons.text.CaseUtils;
import org.apache.commons.text.RandomStringGenerator;

public class Apps {

    public static void main(String[] args) {
        RandomStringGenerator generator= new RandomStringGenerator.Builder()
                .withinRange('a', 'z').build();
        for (int i = 0; i < 100; i++) {
            System.out.println(generator.generate(30));
        }

        String str="kingdee_account_no_name_age";
        int num=100000000;
        System.out.println(testTime(num, new Runnable() {
            @Override
            public void run() {
                CaseUtils.toCamelCase(str,true,'_');
            }
        }));

        System.out.println(testTime(num, new Runnable() {
            @Override
            public void run() {
                getStringBuffer(str);
            }
        }));


        System.out.println(testTime(num, new Runnable() {
            @Override
            public void run() {
                getStringBuffer2(str);
            }
        }));
        System.out.println(CaseUtils.toCamelCase("kingdee_account_no",true,'_'));
    }
    public static long testTime(int nums, Runnable runnable) {
        Long time = System.currentTimeMillis();
        for (int i = 0; i < nums; i++) {
            runnable.run();
        }
        time = System.currentTimeMillis() - time;
        return time;
    }
    private static String getStringBuffer(String str) {
        String[] ls = str.split("_");
        StringBuilder buffer = new StringBuilder();
        for (String l : ls) {
            if (l.length() > 1) {
                buffer.append(Character.toUpperCase(l.charAt(0)));
                for (int i = 1; i < l.length(); i++) {
                    buffer.append(l.charAt(i));
                }
            }
        }
        return buffer.toString();
    }
    private static String getStringBuffer2(String str) {
        char[] cs= str.toCharArray();
        StringBuilder buffer = new StringBuilder();
        boolean up=false;
        for (int i = 0; i < cs.length; i++) {
            char c=cs[i];
            if (c=='_'){
                up=true;
            }else{
                if (up){
                    buffer.append(Character.toUpperCase(c));
                }else{
                    buffer.append(c);
                }
                up=false;
            }
        }
        return buffer.toString();
    }
}
