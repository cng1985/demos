package com.ada.commontext;

import com.nbsaas.boot.chain.Chain;
import com.nbsaas.boot.chain.impl.ChainBase;
import com.nbsaas.boot.rest.request.RequestObject;
import com.nbsaas.boot.rest.response.ResponseObject;

public class CommandApp {

    public static void main(String[] args) {

        Chain<RequestObject,StringBuffer> chain=new ChainBase<>();
        chain.addCommand(new InfCommand());
        chain.addCommand(new ShowCommand());
        ResponseObject<StringBuffer> res = chain.execute(new RequestObject());
        System.out.println(res.getCode());


    }
}
