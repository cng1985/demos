package com.ada.bigdecimal;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class App {

    public static void main(String[] args) {
        BigDecimal num=new BigDecimal("0");
        System.out.println(BigDecimal.ZERO.equals(num));
        System.out.println(BigDecimal.ZERO.compareTo(num));

        BigDecimal n1=new BigDecimal(1230123.0133123123);
        System.out.println(n1.scale());
        BigDecimal nx=new BigDecimal(1230123.05433123123123123);
        System.out.println(nx.scale());
        System.out.println(nx.setScale(1,RoundingMode.HALF_UP));

        System.out.println(n1.setScale(2,RoundingMode.HALF_UP));
       // System.out.println(n1.setScale(2,RoundingMode.HALF_UP).doubleValue());

        BigDecimal n2=new BigDecimal(10.01321);
       BigDecimal n3= n1.multiply(n2,new MathContext(20, RoundingMode.HALF_UP));
        System.out.println(n3);
        BigDecimal n4 = n3.divide(new BigDecimal(0.011), new MathContext(10, RoundingMode.HALF_UP));
        System.out.println(n4);
        System.out.println(n4.round(new MathContext(3,RoundingMode.HALF_UP)));
        System.out.println(new BigDecimal(-103.066161).abs().round(new MathContext(2,RoundingMode.HALF_UP)));
        System.out.println(new BigDecimal(-1231123.066161).abs().round(new MathContext(3,RoundingMode.HALF_UP)));
        System.out.println(new BigDecimal("0.0").compareTo(new BigDecimal("0"))==0);

    }
}
