package com.ada.bigdecimal;

import java.math.BigInteger;

public class BigIntegerApp {

    public static void main(String[] args) {
        BigInteger bigInteger=new BigInteger("7");
        for (int i = 0; i < 3; i++) {
            System.out.println(bigInteger.testBit(i));
        }
    }
}
