package com.ada.bigdecimal;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class MathApp {

    public static void main(String[] args) {

        BigDecimal num=new BigDecimal("9");
        BigDecimal n1=num.divide(new BigDecimal(11), new MathContext(100000,RoundingMode.HALF_UP));
        System.out.println(n1);
        System.out.println(n1.doubleValue());
        System.out.println(n1.setScale(1,RoundingMode.HALF_UP));
        System.out.println(Math.PI);
    }
}
