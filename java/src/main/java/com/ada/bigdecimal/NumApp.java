package com.ada.bigdecimal;

import java.math.BigDecimal;
import java.math.MathContext;

public class NumApp {

    public static void main(String[] args) {

        System.out.println(new BigDecimal("11").divide(new BigDecimal("0.01"), MathContext.DECIMAL128).doubleValue());
    }
}
