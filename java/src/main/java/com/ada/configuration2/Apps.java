package com.ada.configuration2;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;

public class Apps {
    public static void main(String[] args) {
        Configurations configs = new Configurations();
        try
        {
            Configuration config = configs.properties(new File("config.properties"));
            String dbHost = config.getString("database.host");
            int dbPort = config.getInt("database.port");
            String dbUser = config.getString("database.user");
            String dbPassword = config.getString("database.password", "secret");  // provide a default
            long dbTimeout = config.getLong("database.timeout");
            System.out.println(dbHost);
        }
        catch (ConfigurationException cex)
        {
            // Something went wrong
        }

    }
}
