package com.ada.configuration2;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.DatabaseConfiguration;
import org.apache.commons.configuration2.builder.BasicConfigurationBuilder;
import org.apache.commons.configuration2.builder.DatabaseBuilderParametersImpl;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbcp2.DataSourceConnectionFactory;

import javax.sql.DataSource;

public class JdbcApp {
    public static void main(String[] args) {

        BasicDataSource dbs=null;
        if(dbs==null){
            dbs=new BasicDataSource ();
            dbs.setUsername("tongna");
            dbs.setPassword("tongna");
            dbs.setUrl("jdbc:mysql://192.168.0.199:3306/demo1?serverTimezone=UTC  ");
            dbs.setDriverClassName("com.mysql.jdbc.Driver");
            dbs.setInitialSize(10);
            dbs.setMaxIdle(5);
            dbs.setMinIdle(3);
            DataSourceConnectionFactory dscf = new DataSourceConnectionFactory(dbs);
            System.out.println("初始化实例了");
        }

        BasicConfigurationBuilder<DatabaseConfiguration> builder =
                new BasicConfigurationBuilder<DatabaseConfiguration>(DatabaseConfiguration.class);
        DataSource dataSource=dbs;
        DatabaseBuilderParametersImpl parameters=  new DatabaseBuilderParametersImpl();
        parameters.setDataSource(dataSource)
                .setTable("myconfig")
                .setKeyColumn("appkey")
                .setValueColumn("value");
        builder.configure(parameters );
        Configuration config = null;
        try {
            config = builder.getConfiguration();
            for (int i = 0; i < 10000; i++) {
                String value = config.getString("foo");
                System.out.println(value);
                config.setProperty("x1"+i,"ff"+i);
                //Thread.sleep(1500);
            }

        } catch (ConfigurationException e) {
            e.printStackTrace();
        }
    }
}
