package com.ada.script;

import javax.script.*;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.extensions.TestSetup;
import junit.extensions.RepeatedTest;
import junit.extensions.TestDecorator;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class ScriptPerformanceTest extends TestCase {
    public static final int Times = 10000;
    private final String juelScript = "${1 + 2}";
    private final String jexlScript = "1 + 2";
    private final String nashornScript = "1 + 2";
    private final String beanshellScript = "1 + 2";

    private ScriptEngineManager manager;
    private ScriptEngine juelEngine;
    private ScriptEngine jexlEngine;
    private ScriptEngine nashornEngine;
    private ScriptEngine beanshellEngine;

    public ScriptPerformanceTest(String testName) {
        super(testName);
    }

    public void setUp() {
        manager = new ScriptEngineManager();
        juelEngine = manager.getEngineByName("juel");
        jexlEngine = manager.getEngineByName("JEXL");
        nashornEngine = manager.getEngineByName("nashorn");
        beanshellEngine = manager.getEngineByName("beanshell");
    }

    public void testJUELPerformance() throws ScriptException {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < Times; i++) {
            juelEngine.eval(juelScript);
        }
        long endTime = System.currentTimeMillis();
        System.out.println("JUEL Execution Time: " + (endTime - startTime) + "ms");
    }

    public void testJEXLPerformance() throws ScriptException {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < Times; i++) {
            jexlEngine.eval(jexlScript);
        }
        long endTime = System.currentTimeMillis();
        System.out.println("JEXL Execution Time: " + (endTime - startTime) + "ms");
    }

    public void testNashornPerformance() throws ScriptException {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < Times; i++) {
            nashornEngine.eval(nashornScript);
        }
        long endTime = System.currentTimeMillis();
        System.out.println("Nashorn Execution Time: " + (endTime - startTime) + "ms");
    }

    public void testBeanshellPerformance() throws ScriptException {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < Times; i++) {
            beanshellEngine.eval(beanshellScript);
        }
        long endTime = System.currentTimeMillis();
        System.out.println("Beanshell Execution Time: " + (endTime - startTime) + "ms");
    }

    public static void main(String[] args) {
        TestRunner.run(new ScriptPerformanceTest("juel").suite());
    }

    public  Test suite() {
        TestSetup setup = new TestSetup(new TestDecorator(new TestSuite(ScriptPerformanceTest.class)) {
            protected void runTest() throws Throwable {
                setUp();
                testJUELPerformance();
                testJEXLPerformance();
                testNashornPerformance();
                testBeanshellPerformance();
            }
        });
        return setup;
    }
}
