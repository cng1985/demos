package com.ada.script;

import javax.script.*;
import java.util.List;

public class SupportedScriptLanguages {
    public static void main(String[] args) {
        ScriptEngineManager manager = new ScriptEngineManager();
        List<ScriptEngineFactory> engineFactories = manager.getEngineFactories();

        if (engineFactories.isEmpty()) {
            System.out.println("No scripting engines found.");
        } else {
            System.out.println("Available scripting engines:");
            for (ScriptEngineFactory factory : engineFactories) {
                System.out.println("Engine Name: " + factory.getEngineName());
                System.out.println("Engine Version: " + factory.getEngineVersion());
                System.out.println("Language Name: " + factory.getLanguageName());
                System.out.println("Language Version: " + factory.getLanguageVersion());
                System.out.println("Extensions: " + factory.getExtensions());
                System.out.println("Mime Types: " + factory.getMimeTypes());
                System.out.println("Names: " + factory.getNames());
                System.out.println("----------------------------------------");
            }
        }
    }
}
