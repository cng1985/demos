package com.ada.date;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class App {

  public static void main(String[] args) {
    Calendar calendar=Calendar.getInstance();
    calendar.add(Calendar.YEAR,9000000);
    System.out.println(calendar.getTime().getTime());
    System.out.println(calendar.getTime().toLocaleString());

    SimpleDateFormat format=new SimpleDateFormat("yyyy/MM");
    System.out.printf(format.format(new Date()));
    System.out.printf("");
  }
}
