package com.ada.date;

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class PeriodApp {

    public static void main(String[] args) {

        LocalDate today = LocalDate.now();
        System.out.println("Today : " + today);
        LocalDate birthDate = LocalDate.of(2021, Month.OCTOBER, 19);
        System.out.println("BirthDate : " + birthDate);

        Period p = Period.between(birthDate, today);
        System.out.printf("年龄 : %d 年 %d 月 %d 日", p.getYears(), p.getMonths(), p.getDays());

        long daysDiff = ChronoUnit.DAYS.between( today,birthDate);
        System.out.println("两天之间的差在天数   : " + daysDiff);
        Date productionDate = new Date();
        LocalDate production = productionDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        System.out.println(production.toString());

        LocalDate expirationD = production.plus(10, ChronoUnit.DAYS);
        System.out.println(expirationD.toString());

    }
}
