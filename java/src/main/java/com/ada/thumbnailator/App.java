package com.ada.thumbnailator;

import net.coobird.thumbnailator.Thumbnails;

import java.io.File;
import java.io.IOException;

public class App {

    public static void main(String[] args) throws IOException {

        File newFile=new File("d:\\a1.png");
        File file=new File("d:\\1.png");
        //        .size(160, 160)
        Thumbnails.of(file).size(300, 300).outputQuality(1).toFile(newFile);

        //Thumbnails.of(file).scale(1f).outputQuality(0.1f).toFile(newFile);
    }
}
