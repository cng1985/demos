package com.ada.qdox;

import com.thoughtworks.qdox.JavaProjectBuilder;
import com.thoughtworks.qdox.model.JavaClass;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

/** javaparser
 * @author ada
 * @date 2022/3/30
 */
public class App {
    public static void main(String[] args) throws IOException {
        JavaProjectBuilder javaProjectBuilder = new JavaProjectBuilder();
        javaProjectBuilder.addSource(new File("E:\\code\\maven\\demos\\java\\src\\main\\java\\com\\ada\\permission\\Permission.java"));
        Collection<JavaClass> classes= javaProjectBuilder.getClasses();
        for (JavaClass aClass : classes) {
            System.out.println(aClass);
            aClass.getBeanProperties();
        }

    }
}
