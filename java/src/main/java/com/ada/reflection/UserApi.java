package com.ada.reflection;

import java.util.List;

public interface UserApi {

    <T> List<T> list();
}
