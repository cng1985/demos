package com.ada.reflection;

import com.google.gson.Gson;

public class MyClass {

    public static void main(String[] args) {
        System.out.println(new Gson().fromJson("{}",User.class));
        GenericClass<MyClass> gc = new GenericClass<>();
        MyClass myClass = gc.genericMethod();
        System.out.println(myClass.getClass()); // 输出：class MyClass
    }

}