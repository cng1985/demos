package com.ada.reflection;

import java.lang.reflect.*;

public class GenericTypeTest<T> {
    private T t;

    public GenericTypeTest(T t) {
        this.t = t;
    }

    public static void main(String[] args) {
        GenericTypeTest<String> test = new GenericTypeTest<String>("hello");
        Type type = test.getClass().getTypeParameters()[0];
        if (type instanceof TypeVariable) {
            TypeVariable<?> tv = (TypeVariable<?>) type;
            Type[] bounds = tv.getBounds();
            if (bounds.length > 0 && bounds[0] instanceof Class) {
                System.out.println("The actual type is: " + bounds[0]);
            }
        }
    }
}
