package com.ada.reflection;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.List;

public class GenericClass<T> {
    public T genericMethod() {
      List<T> ts=  new Gson().fromJson("[{}]",new TypeToken<List<T>>() {}.getType());

        return ts.get(0);
    }

    private <V> V createInstance(Type type) {
        try {
            Class<V> clazz = (Class<V>) (type instanceof Class ? type : ((ParameterizedType) type).getRawType());
            return clazz.newInstance();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}