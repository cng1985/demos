package com.ada.reflection;

import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

public class DataUser<T> implements UserApi{
    @Override
    public <T> List<T> list() {
        Class<?>[] typeArguments = TypeResolver.resolveRawArguments(DataUser.class, getClass());
        System.out.println(typeArguments[0].getName());
        //Class<T> tClass=new
        Class<?>[] typeArgs = TypeResolver.resolveRawArguments(UserApi.class, DataUser.class);
        Method method = null;
        try {
            method = this.getClass().getMethod("list");
            Type returnType = method.getGenericReturnType();
           Class<?> cc= TypeResolver.resolveRawClass(returnType, DataUser.class);
            System.out.println(cc.getName());
            if (returnType instanceof ParameterizedType) {
                ParameterizedType pType = (ParameterizedType) returnType;
                System.out.print("返回泛型类型:");
                Stream.of(pType.getActualTypeArguments()).forEach(System.out::print);
                System.out.println();


                //Class<Domain> domainClass = (Class<Domain>) returnType.getActualTypeArguments()[0];
                ParameterizedType parameterizedType = (ParameterizedType) returnType;
                Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
                for (Type actualTypeArgument : actualTypeArguments) {
                    if (actualTypeArgument instanceof Class) {
                        Class<?> actualType = (Class<?>) actualTypeArgument;
                        // 处理获取到的泛型类型
                        System.out.println(actualType);
                    }
                    System.out.println("， 真实的泛型的类型为：" + actualTypeArgument.getTypeName());
                }
            } else {
            }

            List<T> res=new ArrayList<>();
            return res;
        } catch (NoSuchMethodException e) {
           // throw new RuntimeException(e);
        }

        return null;
    }
}
