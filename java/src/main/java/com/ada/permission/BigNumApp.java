package com.ada.permission;

import java.math.BigInteger;
import java.util.BitSet;

public class BigNumApp {

    public static void main(String[] args) {

        BigInteger permission1 = new BigInteger("0");
        permission1=permission1.setBit(0);
        permission1=permission1.clearBit(1);
        permission1=permission1.setBit(2);
        System.out.println(permission1.toString(2));


        BigInteger permission = new BigInteger("0");
        BigInteger numBig = permission.setBit(2);
        numBig = numBig.setBit(1);
        numBig = numBig.setBit(5);
        numBig = numBig.setBit(13);
        numBig = numBig.setBit(66);
        System.out.println("原理：" + numBig);
        //2.取值验证（返回Boolean型）
        boolean flag1 = numBig.testBit(2);      //true
        boolean flag2 = numBig.testBit(5);      //true
        boolean flag3 = numBig.testBit(13);     //true
        boolean flag4 = numBig.testBit(66);     //true
        boolean flag5 = numBig.testBit(27);     //false

        numBig = numBig.setBit(3);
        System.out.println(numBig.testBit(3));

        numBig = numBig.clearBit(3);
        System.out.println(numBig.testBit(3));
        numBig=numBig.setBit(100);
        System.out.println(numBig.toString(2));

        BitSet bits=new BitSet();
        bits.set(1);
        bits.set(200);
        bits.clear(1);
        System.out.println(bits.get(1));
        System.out.println(bits.length());
        System.out.println(bits.toLongArray().length);


    }
}
