package com.ada.permission;

import java.math.BigInteger;

public class BigApp {

    public static void main(String[] args) {

        BigInteger num=new BigInteger("0");
        for (int i = 0; i < 128; i++) {
            System.out.println(num.setBit(i));
        }

        System.out.println(num.toString());


        StringBuffer buffer=new StringBuffer();
        buffer.append(9);
        for (int i = 0; i < 500000; i++) {
            buffer=buffer.append("9");
        }

        System.out.println(num.multiply(new BigInteger(buffer.toString())).toString(2).length());

    }
}
