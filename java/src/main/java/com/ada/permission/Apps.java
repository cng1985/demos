package com.ada.permission;

public class Apps {

    public static void main(String[] args) {

        long temp = 0;
        for (int i = 0; i < 128; i++) {
            Permission per = permission(i, "" + i);
            System.out.println(permission(i, "" + i));
            temp = addPermission(temp, per);
            System.out.println(temp);
            if (temp < 0) {
                break;
            }
        }
        Permission add = permission(0, "add");
        Permission delete = permission(1, "delete");
        Permission update = permission(2, "update");
        Permission read = permission(30, "read");
        long value = addPermission(0, read);
        value = addPermission(value, update);
        value = addPermission(value, add);
        value = addPermission(value, delete);
        System.out.println(value);
        value = removePermission(value, read);
        System.out.println(value);
        System.out.println(check(value, read));
        System.out.println(check(value, update));
        System.out.println(check(value, add));
        System.out.println(check(value, delete));

    }

    public static Permission permission(int index, String name) {
        Permission permission = new Permission();
        permission.setIndex(index);
        permission.setName(name);
        long initNum = 1l;
        permission.setValue(initNum << index);
        return permission;
    }

    public static boolean check(long value, Permission permission) {
        boolean result = false;
        long temp = value & permission.getValue();
        temp = temp >> permission.getIndex();
        if (temp == 1) {
            result = true;
        }
        return result;
    }

    public static long addPermission(long value, Permission permission) {
        long result = 0;
        if (!check(value, permission)) {
            result = value + permission.getValue();
        }
        return result;
    }

    public static long removePermission(long value, Permission permission) {
        long result = 0;
        if (check(value, permission)) {
            result = value - permission.getValue();
        }
        return result;
    }

}
