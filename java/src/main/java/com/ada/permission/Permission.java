package com.ada.permission;

import java.io.Serializable;

public class Permission implements Serializable {

  private String name;

  private long value;

  private int index;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public long getValue() {
    return value;
  }

  public void setValue(long value) {
    this.value = value;
  }

  public int getIndex() {
    return index;
  }

  public void setIndex(int index) {
    this.index = index;
  }

  @Override
  public String toString() {
    return "Permission{" +
        "name='" + name + '\'' +
        ", value=" + value +
        ", index=" + index +
        '}';
  }
}
