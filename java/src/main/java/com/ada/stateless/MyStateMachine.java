package com.ada.stateless;

import org.squirrelframework.foundation.fsm.impl.AbstractStateMachine;

public class MyStateMachine extends AbstractStateMachine<MyStateMachine,MyState,MyEvent,MyContext> {

  public void  showMessage(MyState from,MyState to,MyEvent event,MyContext context){
    System.out.println(String.format("showMessage %s %s",from,to));
  }

  public void  intoState(MyState from,MyState to,MyEvent event,MyContext context){
    System.out.println(String.format("intoState %s %s",from,to));
  }

  public void  outoState(MyState from,MyState to,MyEvent event,MyContext context){
    System.out.println(String.format("outoState %s %s",from,to));
  }
}
