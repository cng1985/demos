package com.ada.stateless;

import org.squirrelframework.foundation.fsm.ConverterProvider;
import org.squirrelframework.foundation.fsm.StateMachineBuilder;
import org.squirrelframework.foundation.fsm.StateMachineBuilderFactory;
import org.squirrelframework.foundation.fsm.StateMachinePerformanceMonitor;

public class StateMachineApp {

  public static void main(String[] args) {
    StateMachineBuilder<MyStateMachine, MyState, MyEvent, MyContext> builder =
      StateMachineBuilderFactory.create(MyStateMachine.class, MyState.class, MyEvent.class, MyContext.class);

    builder.externalTransition().from(MyState.A).to(MyState.B).on(MyEvent.GoToB).callMethod("showMessage");
    builder.externalTransition().from(MyState.B).to(MyState.C).on(MyEvent.GoToC).callMethod("showMessage");
    builder.externalTransition().from(MyState.C).to(MyState.A).on(MyEvent.GoToA).callMethod("showMessage");
    builder.onEntry(MyState.A).callMethod("intoState");
    builder.onExit(MyState.A).callMethod("outoState");

    MyStateMachine fsm = builder.newStateMachine(MyState.B);
    MyContext context=new MyContext();

    final StateMachinePerformanceMonitor performanceMonitor =
      new StateMachinePerformanceMonitor("Sample State Machine Performance Info");
    fsm.addDeclarativeListener(performanceMonitor);
    for (int i = 0; i < 2; i++) {
      fsm.fire(MyEvent.GoToB, context);

      System.out.println("Current state is " + fsm.getCurrentState());
      fsm.fire(MyEvent.GoToC, context);
      System.out.println("Current state is " + fsm.getCurrentState());
      fsm.fire(MyEvent.GoToA, context);
      System.out.println("Current state is " + fsm.getCurrentState());

    }
    fsm.removeDeclarativeListener(performanceMonitor);
    //System.out.println(performanceMonitor.getPerfModel());

    //ConverterProvider.INSTANCE.register(MyEvent.class, new MyEventConverter());
  }
}
