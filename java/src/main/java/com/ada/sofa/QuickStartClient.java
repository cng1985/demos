package com.ada.sofa;

import com.ada.rmi.CallBackRequest;
import com.ada.rmi.HelloRegistryFacade;
import com.alipay.sofa.rpc.config.ConsumerConfig;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class QuickStartClient {
    public static void main(String[] args) {
        ConsumerConfig<HelloService> consumerConfig = new ConsumerConfig<HelloService>()
            .setInterfaceId(HelloService.class.getName()) // Specify the interface
            .setProtocol("bolt") // Specify the protocol.setDirectUrl
            .setDirectUrl("bolt://127.0.0.1:12200"); // Specify the direct connection address
        // Generate the proxy class
        HelloService helloService = consumerConfig.refer();


        try {

            Long time=System.currentTimeMillis();
            int num=10000;
            for (int i = 0; i < num; i++) {
                CallBackRequest request=new CallBackRequest();
                StringBuffer buffer=new StringBuffer();
                for (int j = 0; j < 1000; j++) {
                    buffer.append("ada"+i);
                }
                request.setName(buffer.toString());
                //request.setCallback(item->item+":66");
                String res= helloService.sayHello(buffer.toString());
                //System.out.println(res);
            }
            time=System.currentTimeMillis()-time;
            System.out.println(time);
            System.out.println(num/(time/1000.0));
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
}