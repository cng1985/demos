package com.ada.java8;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class DDApp {
    public static void main(String[] args) {

        List<String> atemps = new ArrayList<>();
        atemps.add("a");
        atemps.add("b");

        List<String> btemps = new ArrayList<>();
        btemps.add("1");
        btemps.add("2");

        List<String> ctemps = new ArrayList<>();
        ctemps.add("A");
        ctemps.add("B");

        List<String> dtemps = new ArrayList<>();
        dtemps.add("D");
        dtemps.add("F");
        dtemps.add("G");

        List<String> aas = links(atemps, btemps, ctemps, dtemps);
        Set<String> tes = new HashSet<>();
        tes.addAll(aas);
        System.out.println(aas.size());
        System.out.println(tes.size());

        for (String aa : aas) {
            System.out.println(aa);
        }

        String str = "贰零贰叁年零捌月贰拾捌日";
        System.out.println(isChinese(str));
        System.out.println(isChinese("2023-04-06"));
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(str, DateTimeFormatter.ofPattern("yyyy年MM月dd日"));
        String formattedDate = date.format(formatter);
        System.out.println(formattedDate); // 输出：2023-08-28
    }

    public static boolean isChinese(String str) {
        String regex = "^[\u4E00-\u9FA5]+$";
        return str.matches(regex);
    }

    private static List<String> getStrings(List<String> atemps, List<String> btemps) {
        List<String> ss = new ArrayList<>();
        for (String atemp : atemps) {

            for (String btemp : btemps) {
                ss.add(atemp + "-" + btemp);
            }
        }
        return ss;
    }

    private static List<String> links(List<String>... as) {
        if (as.length < 2) {
            return null;
        }
        List<String> temps = getStrings(as[0], as[1]);
        for (int i = 2; i < as.length; i++) {
            temps = getStrings(temps, as[i]);
        }
        return temps;
    }
}
