package com.ada.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.function.BinaryOperator;
import java.util.stream.Collectors;

public class App {

  public static void main(String[] args) {

    List<Integer> nums = new ArrayList<>();
    nums.add(1);
    nums.add(2);
    nums.add(3);
    nums.add(4);
    nums.add(5);

    List<String> ls = nums.stream().map(new NumConver()).collect(Collectors.toList());
    for (String num : ls) {
      System.out.println(num);
    }
    Integer total = nums.stream().reduce(10, (a, b) -> a + b);
    System.out.println(total);
    Optional<Integer> max = nums.stream().reduce(Integer::max);


    List<String> lss = nums.stream().map(NumConver::conver).collect(Collectors.toList());

  }
}
