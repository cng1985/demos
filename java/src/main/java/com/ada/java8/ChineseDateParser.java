package com.ada.java8;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public class ChineseDateParser {



    public static void main(String[] args) {
        String chineseDate = "贰零贰叁年零捌月捌日";
        StringBuffer buffer = chineData(chineseDate);
        System.out.println(buffer.toString());
    }

    private static StringBuffer chineData(String chineseDate) {
        StringBuffer buffer=new StringBuffer();
        try {
            String[] dates = chineseDate.split("");
            for (String date : dates) {
                buffer.append(CHINESE_NUMBER_MAP.get(date));
            }
        }catch (Exception ignored){
        }
        return buffer;
    }
    private static final Map<String, Object> CHINESE_NUMBER_MAP = new HashMap<>();
    static {
        CHINESE_NUMBER_MAP.put("零", 0);
        CHINESE_NUMBER_MAP.put("壹", 1);
        CHINESE_NUMBER_MAP.put("贰", 2);
        CHINESE_NUMBER_MAP.put("叁", 3);
        CHINESE_NUMBER_MAP.put("肆", 4);
        CHINESE_NUMBER_MAP.put("伍", 5);
        CHINESE_NUMBER_MAP.put("陆", 6);
        CHINESE_NUMBER_MAP.put("柒", 7);
        CHINESE_NUMBER_MAP.put("捌", 8);
        CHINESE_NUMBER_MAP.put("玖", 9);
        CHINESE_NUMBER_MAP.put("拾", "");
        CHINESE_NUMBER_MAP.put("年", "-");
        CHINESE_NUMBER_MAP.put("月", "-");
        CHINESE_NUMBER_MAP.put("日", "");

    }


}
