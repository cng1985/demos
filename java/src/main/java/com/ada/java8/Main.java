package com.ada.java8;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class Main {
    public static void main(String[] args) {
        String str = "贰零贰叁年零捌月贰拾捌日";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = parseChineseDate(str, formatter);
        System.out.println(date); // 输出：2023-08-28
    }

    public static LocalDate parseChineseDate(String str, DateTimeFormatter formatter) {
        int year = 0, month = 0, day = 0;
        String[] strs = str.split("年|月|日");
        for (String s : strs) {
            for (int i = 0; i < s.length(); i++) {
                char c = s.charAt(i);
                if (c >= '零' && c <= '玖') {
                    int num = c - '零';
                    switch (i) {
                        case 0:
                            if (s.length() == 2) { // 处理两位数字的情况
                                year = year * 10 + num;
                            } else {
                                year = year * 1000 + num * 100;
                            }
                            break;
                        case 1:
                            year = year + num * 10;
                            break;
                        case 2:
                            year = year + num;
                            break;
                        case 3:
                            month = num * 10;
                            break;
                        case 4:
                            month = month + num;
                            break;
                        case 5:
                            day = num * 10;
                            break;
                        case 6:
                            day = day + num;
                            break;
                        default:
                            break;
                    }
                }
            }
        }
        // 判断月份是否合法
        if (month < 1 || month > 12) {
            return null;
        }
        return LocalDate.of(year, month, day);
    }
}
