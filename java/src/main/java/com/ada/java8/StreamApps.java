package com.ada.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

/**
 * @author 陈联高
 * @version 1.01 2017年02月127日
 */
public class StreamApps {
    public static void main(String[] args) {
        List<String> names = new ArrayList<String>();
        names.add("ada");
        names.add("young");
        List<String> ynames = names.stream().filter(Objects::nonNull).collect(Collectors.toList());
        ynames.stream().filter(name -> name.indexOf("a") > -1).forEach(name -> System.out.println(name));
        System.out.println("************");
        ynames.stream().map(s -> s + "1").forEach(s -> System.out.println(s));
        System.out.println("************");

        List<Integer> nums = new ArrayList<>();
        nums.add(1);
        nums.add(2);
        nums.add(3);
        nums.add(4);
        nums.add(5);
        Integer x = nums.stream().reduce(0,  Integer::max);
        System.out.println(x);
        IntStream.range(1, 5).forEach(System.out::println);

        Random seed = new Random();
        Supplier<Integer> random = seed::nextInt;
        Stream.generate(random).limit(10).forEach(System.out::println);
        //Stream.generate(random).limit(10).collect(Collectors::groupingBy(s->s))
    }
    public static Integer num(Integer num){
        return 1;
    }
}
