package com.ada.logger4j2;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Apps {
    private static final Logger logger = LogManager.getLogger("a");
    public static void main(String[] args) {
        logger.info("Hello, World!");



        if (logger.isDebugEnabled()) {
            logger.debug("Logging in user " + 1 + " with birthday " + 2);
        }
    }
}
