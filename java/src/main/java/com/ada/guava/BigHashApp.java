package com.ada.guava;

import com.google.common.hash.Hashing;

import java.nio.charset.Charset;

public class BigHashApp {

    public static void main(String[] args) {
        for (int i = 100; i < 10000; i++) {
            int bucket = Hashing.consistentHash(100, i+1);
            System.out.println(bucket);
        }
    }
}
