package com.ada.guava.reflection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

public class MathAddImpl implements InvocationHandler {
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (args!=null && args.length==2) {
            return (int)args[0]+(int)args[1];
        }
        return null;
    }
}
