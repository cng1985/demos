package com.ada.guava.reflection;

import com.google.common.collect.Lists;
import com.google.common.reflect.Reflection;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.ArrayList;

public class App {

  public static void main(String[] args) {
    InvocationHandler invocationHandler=new InvocationHandler() {
      @Override
      public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return null;
      }
    };
    Foo foo = Reflection.newProxy(Foo.class, invocationHandler);
    foo.show();

    ArrayList<String> stringList = Lists.newArrayList();
  }
}
