package com.ada.guava.reflection;

import com.google.common.reflect.Reflection;

import java.util.Comparator;
import java.util.Random;
import java.util.function.BiFunction;

public class MapApp {

    public static void main(String[] args) {

        int num=10000;

        Long time = System.currentTimeMillis();
        MathAdd last = null;
        for (int i = 0; i < num; i++) {
            MathAdd foo = Reflection.newProxy(MathAdd.class, new MathAddImpl());
            if (foo==last) {
                System.out.println("same");
            }
            last= foo;
            foo.add(i, new Random().nextInt(10000));
        }
        time = System.currentTimeMillis() - time;
        System.out.println(time);


        Long time1 = System.currentTimeMillis();

        for (int i = 0; i < num; i++) {
            BiFunction<Integer,Integer,Integer> func = (a, b) -> a + b;
            int numx =func.apply(i , new Random().nextInt(10000)) ;
        }
        time1 = System.currentTimeMillis() - time1;
        System.out.println(time1);
    }
}
