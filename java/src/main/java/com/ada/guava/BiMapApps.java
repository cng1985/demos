package com.ada.guava;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;

/**
 * Created by ada on 2017/5/9.
 */
public class BiMapApps
{
    public static void main(String[] args) {
        BiMap<String,String> bimap= HashBiMap.create();
        bimap.put("tom", "28983123@qq.com");
        bimap.put("jack", "56483123@qq.com");
        bimap.put("lucy", "11113123@qq.com");
        bimap.put("lucy", "11113123@qq.com1");

        //inverse()进行反转
        String user=bimap.inverse().get("28983123@qq.com");
        System.out.println(user);
        System.out.println(bimap.inverse().inverse()==bimap);
    }
}
