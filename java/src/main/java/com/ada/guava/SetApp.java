package com.ada.guava;

import com.google.common.collect.Sets;

import java.util.HashSet;

/**
 * Created by ada on 2017/5/8.
 */
public class SetApp {
    public static void main(String[] args) {
        HashSet setA = Sets.newHashSet(1, 2, 3, 4, 5);
        HashSet setB = Sets.newHashSet(4, 5, 6, 7, 8);
        Sets.SetView<Integer> union = Sets.union(setA, setB);
        System.out.println("union:");
        for (Integer integer : union)
            System.out.println(integer);
        Sets.SetView<Integer> difference = Sets.difference(setB, setA);
        System.out.println("difference:");
        for (Integer integer : difference)
            System.out.println(integer);
        Sets.SetView<Integer> intersection = Sets.intersection(setA, setB);
        System.out.println("intersection:");
        for (Integer integer : intersection)
            System.out.println(integer);
    }
}
