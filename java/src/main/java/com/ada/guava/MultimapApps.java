package com.ada.guava;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.util.*;

/**
 * Created by ada on 2017/5/9.
 */
public class MultimapApps {

    public static void main(String[] args) {
        Map<String,String> cours=new HashMap<String,String>();
        cours.put("高等数学", "tom");
        cours.put("离散数学", "tom");
        cours.put("线性代数", "jack");
        cours.put("数学分析", "lucy");
        cours.put("概率论", "tom");

        //teachers--老师-课程
        Multimap<String,String> teachers= ArrayListMultimap.create();

        //使用迭代器遍历cours
        Iterator<Map.Entry<String,String>> it=cours.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry<String,String> entry=it.next();
            String key=entry.getKey();
            String value=entry.getValue();

            //教师课程对换
            teachers.put(value,key);
        }

        //查看Multimap
        Set<String> keySet=teachers.keySet();
        for(String key:keySet){
            Collection<String> col=teachers.get(key);
            System.out.println(key+"  "+col);
        }

    }
}
