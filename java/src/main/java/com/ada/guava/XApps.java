package com.ada.guava;

import com.google.common.base.Optional;

import java.util.LinkedList;
import java.util.List;
import static com.google.common.base.Strings.emptyToNull;

public class XApps {

    public static void main(String[] args) {
        List<String> list = new LinkedList<String>();
        list.add("A");
        list.add(null);
        list.add("C");
        list.add(null);
        list.add("E");
        list.add("");
        Optional<String> possible;
        for(int i = 0 ; i < list.size(); i++){
            possible = Optional.fromNullable(emptyToNull(list.get(i)));
            System.out.println("index: "+i+" -  value: "+possible.or("no value"));
        }
    }
}
