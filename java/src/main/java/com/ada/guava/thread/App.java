package com.ada.guava.thread;

import com.google.common.util.concurrent.ListenableFuture;
import com.google.common.util.concurrent.ListeningExecutorService;
import com.google.common.util.concurrent.MoreExecutors;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class App {

  public static void main(String[] args) {
    ListeningExecutorService executorService = MoreExecutors.listeningDecorator(Executors.newCachedThreadPool());
    final ListenableFuture<Integer> listenableFuture = executorService.submit(new Callable<Integer>() {
      @Override
      public Integer call() throws Exception {
        System.out.println("call execute..");
        TimeUnit.SECONDS.sleep(1);
        return 7;
      }
    });
    listenableFuture.addListener(new Runnable() {
      @Override
      public void run() {
        try {
          System.out.println("get listenable future's result " + listenableFuture.get());
        } catch (InterruptedException e) {
          e.printStackTrace();
        } catch (ExecutionException e) {
          e.printStackTrace();
        }
      }
    }, executorService);
  }
}
