package com.ada.guava;

import com.google.common.util.concurrent.RateLimiter;

/**
 * Created by ada on 2017/6/5.
 */
public class RateLimiterApps {
    private static RateLimiter limiter=RateLimiter.create(30);

    private static int num=0;
    public static void main(String[] args) {

        limiter.setRate(10);
        for (int i = 0; i < 100; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {

                    limiter.acquire();
                    System.out.println(""+ limiter.getRate());
                    num++;
                }
            }).start();
        }

    }
}
