package com.ada.guava;

import com.google.common.graph.*;

import java.util.Optional;
import java.util.Set;

public class GraphApps {

    public static void main(String[] args) {
        MutableGraph<Integer> graph = GraphBuilder.undirected().build();
        graph.addNode(5);
        graph.addNode(8);
        graph.putEdge(1,56);
        graph.putEdge(2,56);
      Set x=  graph.edges();
        for (Object o : x) {
            System.out.println(o);
        }

        MutableValueGraph<City, Distance> roads = ValueGraphBuilder.directed().build();
        roads.putEdgeValue(City.name("西安"),City.name("重庆"),Distance.num(650L));
        roads.putEdgeValue(City.name("西安"),City.name("成都"),Distance.num(850L));
        roads.putEdgeValue(City.name("成都"),City.name("重庆"),Distance.num(250L));
        roads.putEdgeValue(City.name("长沙"),City.name("重庆"),Distance.num(450L));
        roads.putEdgeValue(City.name("重庆"),City.name("长沙"),Distance.num(650L));

        Set<EndpointPair<City>> endpointPairs=   roads.edges();
        for (EndpointPair<City> endpointPair : endpointPairs) {
            System.out.println(endpointPair);
        }
        Optional<Distance> distance= roads.edgeValue(City.name("西安"),City.name("长沙"));
        System.out.println(distance.isPresent());


    }
}
