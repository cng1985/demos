package com.ada.guava;

import com.google.common.base.Joiner;
import com.google.common.base.Splitter;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Created by ada on 2017/5/8.
 */
public class JoinerApps {
    public static void main(String[] args) {
        String[] subdirs = { "usr", "local", "lib" };
        String directory = Joiner.on("/").join(subdirs);
        System.out.println(directory);
        String str = "";

        //我们的写代码方式
        if(subdirs!=null&&subdirs.length>0){
            for (int i = 0; i <subdirs.length ; i++) {
                if(i!=subdirs.length-1){
                    str = str + subdirs[i]+"/";
                }else{
                    str  = str+ subdirs[i];
                }
            }
        }
        System.out.println("str:"+str);


        //google 工程师的写代码方式
        Iterable iterable= (Iterable) Arrays.asList(subdirs);
        Iterator iterator=  iterable.iterator();
        StringBuffer xx=new StringBuffer();
        xx.append(iterator.next());
        while (iterator.hasNext()){
            xx.append("/");
            xx.append(iterator.next());
        }
        System.out.println("xx:"+xx);

        StringBuffer yyy=new StringBuffer();
        iterator=  iterable.iterator();
        while (iterator.hasNext()){
            yyy.append(iterator.next());
            if (iterator.hasNext()){
                yyy.append("/");
            }
        }
        System.out.println("yyy:"+yyy);

        String[] splitRegular = directory.split(",");
        Iterable<String> split = Splitter.on("/").omitEmptyStrings().trimResults().split(directory);
        split.forEach(x->{
            System.out.println(x);
        });
    }
}
