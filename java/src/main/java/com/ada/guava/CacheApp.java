package com.ada.guava;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class CacheApp {

    public static void main(String[] args) throws InterruptedException, ExecutionException {
        Cache<Integer, Integer> numCache = CacheBuilder.newBuilder()
                .expireAfterWrite(2, TimeUnit.SECONDS)
                .build();

        numCache.put(1, 100);
        System.out.println(numCache.getIfPresent(1));
        //Thread.sleep(3000);
        System.out.println(numCache.getIfPresent(1));

        LoadingCache<Integer,Integer> cache = CacheBuilder.newBuilder().
                expireAfterWrite(5L, TimeUnit.MINUTES).
                maximumSize(5000L).
                build(new CacheLoader<Integer, Integer>() {
                    @Override
                    public Integer load(Integer key) throws Exception {
                        System.out.println("no cache");
                        return 0;
                    }
                });

        System.out.println(cache.get(1));
        cache.put(1,100);
        System.out.println(cache.getIfPresent(1));
        System.out.println(cache.getIfPresent(1));

    }
}
