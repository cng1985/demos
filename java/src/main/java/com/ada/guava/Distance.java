package com.ada.guava;

public class Distance {

    public static Distance num(Long num) {
        Distance distance = new Distance();
        distance.num = num;
        return distance;
    }

    private Long num;

    public Long getNum() {
        return num;
    }

    public void setNum(Long num) {
        this.num = num;
    }
}
