package com.ada.guava;

public class City {

    private String name;

    public static City name(String name) {
        City city = new City();
        city.name = name;
        return city;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "City{" +
                "name='" + name + '\'' +
                '}';
    }
}
