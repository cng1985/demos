package com.ada.guava;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;

import java.util.List;
import java.util.Set;

public class CartesianProductUtil {
    public static void main(String[] args) {
        ImmutableSet<Character> charList = ImmutableSet.of('a', 'b', 'c');
        Set<List<Character>> set = Sets.cartesianProduct(charList, charList, charList);
        for (List<Character> characters : set) {
            System.out.println(characters);
        }
    }
}
