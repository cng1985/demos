package com.ada.guava;

import com.google.common.base.Function;
import com.google.common.collect.Maps;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ada on 2017/5/8.
 */
public class FunctionsApps {
    public static void main(String[] args) {
        HashMap<String, String> fromMap = new HashMap<String, String>();
        fromMap.put("ada","ada");
        Map<String, Object> x = Maps.transformValues(fromMap, s -> {return s+"x";});
        System.out.println(x.get("ada"));

    }
}
