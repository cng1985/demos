package com.ada.guava;


import java.util.Optional;

public class Demo {
    private String name;

    private void work(){
        System.out.println("haha i am work");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void main(String[] args) {
        Demo demo = null;
        Optional<Demo> x = Optional.ofNullable(demo);
        x.orElse(new Demo()).work();
    }
}
