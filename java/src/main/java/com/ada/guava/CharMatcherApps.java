package com.ada.guava;

import com.google.common.base.CharMatcher;

/**
 * Created by ada on 2017/5/8.
 */
public class CharMatcherApps {
    public static void main(String[] args) {
        String string = CharMatcher.digit().retainFrom("some text 89983 and more");
        System.out.println(string);
        string = CharMatcher.digit().removeFrom("some text 89983 and more");
        System.out.println(string);
    }
}
