package com.ada.json;

import java.util.*;
import java.util.regex.*;
import org.json.*;

public class DDLToJsonConverter {
    public static void main(String[] args) {
        String ddl = "CREATE TABLE `com_base_kehu` (" +
                     "`Id` INT(11) NOT NULL AUTO_INCREMENT," +
                     "`pid` INT(11) NULL DEFAULT NULL," +
                     "`tn_id` INT(11) NULL DEFAULT '0' COMMENT '租户ID'," +
                     // 省略其余字段，为了简洁...
                     "`wx_unikey` VARCHAR(100) NULL DEFAULT '' COMMENT '微信公众号的unikey'," +
                     "PRIMARY KEY (`Id`)" +
                     ") " +
                     "COMMENT='客户档案表(含职能客户定义)'" +
                     "COLLATE='utf8_general_ci'" +
                     "ENGINE=InnoDB;";

        JSONObject json = convertDDLToJson(ddl);
        System.out.println(json.toString(2));
    }

    public static JSONObject convertDDLToJson(String ddl) {
        JSONObject tableJson = new JSONObject();
        
        // Extract table name
        Pattern tableNamePattern = Pattern.compile("CREATE TABLE `(\\w+)`");
        Matcher tableNameMatcher = tableNamePattern.matcher(ddl);
        if (tableNameMatcher.find()) {
            tableJson.put("table_name", tableNameMatcher.group(1));
        }

        // Extract table comment
        Pattern tableCommentPattern = Pattern.compile("COMMENT='(.*?)'");
        Matcher tableCommentMatcher = tableCommentPattern.matcher(ddl);
        if (tableCommentMatcher.find()) {
            tableJson.put("table_comment", tableCommentMatcher.group(1));
        }

        // Extract columns
        JSONArray columnsArray = new JSONArray();
        Pattern columnPattern = Pattern.compile("`(\\w+)` (\\w+)(\\((\\d+|\\d+,\\d+)\\))? (NOT NULL|NULL)? (DEFAULT (NULL|'[^']*'|\\d+(?:\\.\\d+)?))?( COMMENT '([^']*)')?");
        Matcher columnMatcher = columnPattern.matcher(ddl);
        while (columnMatcher.find()) {
            JSONObject columnJson = new JSONObject();
            columnJson.put("name", columnMatcher.group(1));
            columnJson.put("type", columnMatcher.group(2));
            columnJson.put("length", columnMatcher.group(4) != null ? columnMatcher.group(4) : JSONObject.NULL);
            columnJson.put("nullable", columnMatcher.group(5) != null && columnMatcher.group(5).equals("NULL"));
            columnJson.put("default", columnMatcher.group(7) != null ? columnMatcher.group(7).replace("DEFAULT ", "").replace("'", "") : JSONObject.NULL);
            columnJson.put("comment", columnMatcher.group(9) != null ? columnMatcher.group(9) : JSONObject.NULL);
            columnsArray.put(columnJson);
        }
        tableJson.put("columns", columnsArray);

        // Extract primary key
        Pattern primaryKeyPattern = Pattern.compile("PRIMARY KEY \\(`(\\w+)`\\)");
        Matcher primaryKeyMatcher = primaryKeyPattern.matcher(ddl);
        if (primaryKeyMatcher.find()) {
            tableJson.put("primary_key", primaryKeyMatcher.group(1));
        }

        // Extract table options (collate and engine)
        Pattern tableOptionsPattern = Pattern.compile("COLLATE='(\\w+)'\\s+ENGINE=(\\w+)");
        Matcher tableOptionsMatcher = tableOptionsPattern.matcher(ddl);
        if (tableOptionsMatcher.find()) {
            tableJson.put("collate", tableOptionsMatcher.group(1));
            tableJson.put("engine", tableOptionsMatcher.group(2));
        }

        return tableJson;
    }
}
