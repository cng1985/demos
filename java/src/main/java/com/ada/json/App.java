package com.ada.json;

import jodd.io.FileUtil;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class App {

    public static void main(String[] args) throws IOException {

        // 我要你扮演一个专业DBA。我将提供给你数据表结构以及我的需求，你的目标是告知我性能最优的可执行的SQL语句，并尽可能的向我解释这段SQL语句，如果有更好的优化建议也可以提出来。
        //我要你扮演一个专业DBA。我将提供给你数据表结构,我们的数据库是mysql，你需要帮忙创建表，创建表的时候采用合适的英文单词，字段需要中文注释。
        //箱盒汇  E:\codes\maven\boxCloud\documents\设计\数据库设计.mdj
        //票据系统  E:\codes\maven\billCloud\documents\设计\数据库设计.mdj
        String body = FileUtil.readString("E:\\codes\\maven\\boxCloud\\documents\\设计\\数据库设计.mdj");

        JSONObject object = new JSONObject(body);
        JSONArray elements = object.getJSONArray("ownedElements");
        for (int i = 0; i < elements.length(); i++) {
            JSONObject obj = elements.getJSONObject(i);
            //客户关系管理 设计协助模块2 用户中心
            System.out.println(obj.getString("name"));
            if (obj.has("ownedElements")){
                JSONArray objs = obj.getJSONArray("ownedElements");
                for (int j = 0; j < objs.length(); j++) {
                    JSONObject tobj = objs.getJSONObject(j);
                    printEntity(tobj);

                }

            }
            System.out.println(obj.getString("name")+"*******************************");
        }
    }

    private static void printEntity(JSONObject tobj) {
        if ("ERDEntity".equals(tobj.getString("_type"))){
            String entityName=tobj.getString("name");
            if (!entityName.equals("打样订单分摊表")){
                return;
            }

            System.out.println();
            System.out.print("帮我设计一个这样的表,数据库采用mysql，表名必须是英文，字段必须是英文，必须有中文注释。 ");
            // System.out.print("帮我设计一个这样的jpa实体 ");
            System.out.print(entityName);


            List<String> names=new ArrayList<>();
            if (tobj.has("columns")){
                JSONArray columns = tobj.getJSONArray("columns");
                for (int k = 0; k < columns.length(); k++) {
                    JSONObject col = columns.getJSONObject(k);
                    names.add(col.getString("name"));
                }
            }
            System.out.print("("+names.stream().collect(Collectors.joining(","))+")");
            System.out.print(" id采用bigint");
            System.out.print("默认有create_user  bigint 创建人id,create_name 添加人姓名,create_date 创建时间，update_user bigint更新人id，update_name 更新人姓名，update_date 更新时间,得有注释");
            System.out.print("。 create_name长度为50，update_name长度为50");
            System.out.print(",生成mysql创建表语句");
             // System.out.print(" 表名前缀增加design");
            //System.out.print(" 生成对应的jpa实体");
            //System.out.print(" 默认有create_user  bigint 创建人id,create_name 添加人姓名,create_date 创建时间，update_user bigint更新人id，update_name 更新人姓名，update_date 更新时间,得有注释");
            System.out.println();
        }
    }
}
