package com.ada;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class PiCalculator {
    public static void main(String[] args) {
        int scale = 10000; // 计算精度
        BigDecimal pi = BigDecimal.ZERO;
        BigDecimal piTerm = BigDecimal.ZERO;
        int sign = 1;

        for (int i = 0; i < scale; i++) {
            BigDecimal dividend = BigDecimal.valueOf(sign);
            BigDecimal divisor = BigDecimal.valueOf(2 * i + 1);
            piTerm = dividend.divide(divisor, scale, RoundingMode.HALF_EVEN);
            pi = pi.add(piTerm);
            sign = -sign;
        }

        pi = pi.multiply(BigDecimal.valueOf(4));
        System.out.println(pi.setScale(10,RoundingMode.HALF_UP));
    }
}
