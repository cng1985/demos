package com.ada.modelmapper;

import com.ada.mapstruct.*;
import org.mapstruct.Mapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.BeanUtils;

public class AppBean {

    public static void main(String[] args) {

        Long time=System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            UserMapper userMapper=new UserMapperImpl();
            Mapper mapper=   userMapper.getClass().getAnnotation(Mapper.class);
            mapper.componentModel();
        }
        time=System.currentTimeMillis()-time;
        System.out.println(time);
    }
}
