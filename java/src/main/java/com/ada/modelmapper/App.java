package com.ada.modelmapper;

import com.ada.mapstruct.Car;
import com.ada.mapstruct.User;
import com.ada.mapstruct.UserSimple;
import org.modelmapper.ModelMapper;

public class App {

    public static void main(String[] args) {

        Long time=System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            User user=new User();
            user.setAge(11);
            user.setName("ada");
            user.setCar(Car.builder().name("dd").buyYear(12).build());
            ModelMapper modelMapper = new ModelMapper();
            UserSimple orderDTO = modelMapper.map(user, UserSimple.class);
        }
        time=System.currentTimeMillis()-time;
        System.out.println(time);
    }
}
