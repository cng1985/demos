package com.ada.trie;

import java.util.HashMap;
import java.util.Map;

class TrieNode {
    private Map<Character, TrieNode> children = new HashMap<>();
    private boolean isEnd = false;

    public void put(char c, TrieNode node) {
        children.put(c, node);
    }

    public TrieNode get(char c) {
        return children.get(c);
    }

    public boolean containsKey(char c) {
        return children.containsKey(c);
    }

    public void setEnd(boolean end) {
        isEnd = end;
    }

    public boolean isEnd() {
        return isEnd;
    }
}

public class SensitiveWordFilter1 {
    private TrieNode root = new TrieNode();

    // 添加敏感词到Trie树
    public void addSensitiveWord(String word) {
        TrieNode node = root;
        for (char c : word.toCharArray()) {
            if (!node.containsKey(c)) {
                node.put(c, new TrieNode());
            }
            node = node.get(c);
        }
        node.setEnd(true);
    }

    // 检测文本中是否包含敏感词
    public boolean containsSensitiveWord(String text) {
        TrieNode node = root;
        for (char c : text.toCharArray()) {
            if (node.containsKey(c)) {
                node = node.get(c);
                if (node.isEnd()) {
                    return true; // 匹配到敏感词
                }
            } else {
                node = root; // 未匹配，重新从根节点开始
            }
        }
        return false; // 未匹配到任何敏感词
    }

    public static void main(String[] args) {
        SensitiveWordFilter1 filter = new SensitiveWordFilter1();
        filter.addSensitiveWord("敏感词1");
        filter.addSensitiveWord("敏感词2");

        String text1 = "这是一个包含敏感词1的文本。";
        String text2 = "这是一个正常的文本。";

        if (filter.containsSensitiveWord(text1)) {
            System.out.println("文本1包含敏感词。");
        } else {
            System.out.println("文本1没有包含敏感词。");
        }

        if (filter.containsSensitiveWord(text2)) {
            System.out.println("文本2包含敏感词。");
        } else {
            System.out.println("文本2没有包含敏感词。");
        }
    }
}
