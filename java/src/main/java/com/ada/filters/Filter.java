package com.ada.filters;


public interface Filter {

  void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain);

}
