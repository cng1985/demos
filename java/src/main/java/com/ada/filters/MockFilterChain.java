package com.ada.filters;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class MockFilterChain implements FilterChain {


  private final List<Filter> filters;

  private Iterator<Filter> iterator;

  public void rest() {
    iterator = filters.iterator();
  }

  public void addFilter(Filter filter) {
    filters.add(filter);
  }


  public MockFilterChain(Filter... filters) {
    this.filters = initFilterList(filters);
  }

  private static List<Filter> initFilterList(Filter... filters) {
    return Arrays.asList(filters);
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response) {
    if (this.iterator == null) {
      this.iterator = this.filters.iterator();
    }

    if (this.iterator.hasNext()) {
      Filter nextFilter = (Filter) this.iterator.next();
      nextFilter.doFilter(request, response, this);
    }
  }
}
