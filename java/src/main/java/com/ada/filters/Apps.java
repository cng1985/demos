package com.ada.filters;

public class Apps {

  public static void main(String[] args) {

    ServletFilterProxy filterProxy=new ServletFilterProxy(new DemoServlet());
    FilterChain chain=new MockFilterChain(new FilterOne(),new FilterTwo(),filterProxy);
    chain.doFilter(null,null);
    System.out.println("******");
    ((MockFilterChain) chain).rest();
    chain.doFilter(null,null);
  }
}
