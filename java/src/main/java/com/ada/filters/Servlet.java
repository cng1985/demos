package com.ada.filters;


public interface Servlet {

  void service(ServletRequest var1, ServletResponse var2);

}
