package com.ada.filters;

public class FilterTwo implements Filter {
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) {
    System.out.println("filter two");
    System.out.println(request.getClass().getSimpleName());
    filterChain.doFilter(request,response);
    System.out.println("filter one two");

  }
}
