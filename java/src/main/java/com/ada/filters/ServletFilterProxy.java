package com.ada.filters;


public class ServletFilterProxy implements Filter {

  private final Servlet delegateServlet;

  public ServletFilterProxy(Servlet servlet) {
    this.delegateServlet = servlet;
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) {

    System.out.println("exe");
    delegateServlet.service(request, response);
  }
}
