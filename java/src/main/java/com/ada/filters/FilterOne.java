package com.ada.filters;

public class FilterOne implements Filter {
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) {
    System.out.println("filter one");
    ServletRequestWrapper requestWrapper=new ServletRequestWrapper();
    filterChain.doFilter(requestWrapper,response);
    System.out.println("filter one end");
  }
}
