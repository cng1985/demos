package com.ada.filters;

import java.util.HashMap;
import java.util.Map;

public class ServletRequest {

  private Map<String, String> params = new HashMap<>();

  public String getParam(String key) {
    return params.get(key);
  }

  public String setParam(String key, String value) {
    return params.put(key, value);
  }
}
