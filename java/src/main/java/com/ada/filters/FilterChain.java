package com.ada.filters;


public interface FilterChain {

  void doFilter(ServletRequest var1, ServletResponse var2);

}
