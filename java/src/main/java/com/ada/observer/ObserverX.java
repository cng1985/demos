package com.ada.observer;

import java.util.Observable;
import java.util.Observer;

/**
 * 观察者
 * Created by ada on 2017/5/8.
 */
public class ObserverX implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        System.out.println(arg);
    }
}
