package com.ada.observer;

import java.util.Observable;
import java.util.Observer;

/**
 * Created by ada on 2017/5/8.
 */
public class ObservableX extends Observable {
    @Override
    public synchronized void addObserver(Observer o) {
        super.addObserver(o);
        setChanged();
    }

    public static void main(String[] args) {
        Observable observable=new ObservableX();
        observable.addObserver(new ObserverX());
        observable.addObserver(new ObserverX());
        observable.addObserver(new ObserverX());
        observable.hasChanged();
        observable.notifyObservers("hi");
    }
}
