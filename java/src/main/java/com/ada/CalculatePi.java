package com.ada;

import java.util.Random;

public class CalculatePi {
    public static void main(String[] args) {
        int numPoints = 1000000;
        Random rand = new Random();
        int countInsideCircle = 0;

        for (int i = 0; i < numPoints; i++) {
            double x = rand.nextDouble();
            double y = rand.nextDouble();
            double distance = Math.sqrt(x * x + y * y);
            if (distance <= 1.0) {
                countInsideCircle++;
            }
        }

        double pi = 4.0 * countInsideCircle / numPoints;
        System.out.println("Approximated value of Pi: " + pi);
    }
}
