package com.ada.sm2;

import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.KeyGenerationParameters;
import org.bouncycastle.crypto.generators.ECKeyPairGenerator;
import org.bouncycastle.crypto.params.*;
import org.bouncycastle.jce.ECNamedCurveTable;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.jce.spec.ECNamedCurveParameterSpec;
import org.bouncycastle.jce.spec.ECNamedCurveSpec;

import java.security.Security;
import java.security.SecureRandom;

public class SM2KeyPairGenerationExample {
    public static void main(String[] args) {
        // 添加Bouncy Castle作为安全提供者
        Security.addProvider(new BouncyCastleProvider());

        // 使用预定义的SM2曲线参数生成SM2密钥对
        ECKeyPairGenerator keyPairGenerator = new ECKeyPairGenerator();
        ECNamedCurveParameterSpec ecParams = ECNamedCurveTable.getParameterSpec("sm2p256v1");
        keyPairGenerator.init(new KeyGenerationParameters(new SecureRandom() ,256));
        AsymmetricCipherKeyPair keyPair = keyPairGenerator.generateKeyPair();
        AsymmetricKeyParameter privateKey = keyPair.getPrivate();
        AsymmetricKeyParameter publicKey = keyPair.getPublic();

        // 输出密钥对信息

    }
}
