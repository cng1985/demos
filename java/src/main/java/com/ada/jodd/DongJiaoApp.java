package com.ada.jodd;

import jodd.http.HttpRequest;
import org.json.JSONArray;
import org.json.JSONObject;

public class DongJiaoApp {

    public static void main(String[] args) {

        //https://new.dongjiaoapp.com/api/adszone/getAdsByMark?mark=home_banner
        //https://new.dongjiaoapp.com/api/js/city_list
        //https://new.dongjiaoapp.com/api/js/h5_index
        //https://new.dongjiaoapp.com/api/index/h5_itemall
        //https://new.dongjiaoapp.com/api/js/comment_list?id=393&page=2&token=
        //https://new.dongjiaoapp.com/api/user/recommend
        HttpRequest request = HttpRequest.post("https://new.dongjiaoapp.com/api/js/h5_index");
        request.form("sort", "0");
        request.form("uid", "1");
        //request.form("lat", "34.293180");
        //request.form("lon", "108.947120");
        request.form("city_id","1988");
        request.form("page", "4");
        //request.form("status","1");
        request.form("page_size", "10");

        String txt = request.send().bodyText();

        JSONObject object = new JSONObject(txt);
        JSONObject data = object.getJSONObject("data");
        if (data != null) {
            JSONArray list = data.getJSONArray("list");
            if (list.length()>0){
                System.out.println(list.getJSONObject(0));
            }
        }
       // System.out.println(object.toString(1));
    }
}
