package com.ada.jodd;

import jodd.util.StringUtil;
import org.springframework.util.StringUtils;

public class App {

    public static void main(String[] args) {
        String one = StringUtil.trimLeft("  ewr r  rf  ");
        String two = StringUtils.trimAllWhitespace("  ewr r  rf  ");
        System.out.println(one);
        System.out.println(two);

    }
}
