package com.ada.jodd;

import java.lang.Integer;

import java.lang.reflect.Field;

public class BeanApp {
    public static void main(String[] args) {
        Field[] fields=  Demo.class.getDeclaredFields();
        for (Field field : fields) {
            System.out.println(field.getName());
            System.out.println(field.getType().getName());
        }
    }
}
