package com.ada.jodd;

import lombok.Data;

@Data
public class Domain {

    private Integer code;

    private String data;

    private String msg;
}
