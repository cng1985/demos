package com.ada.jodd;

import lombok.Data;

import java.util.List;

@Data
public class Demo {

    private List<DataSimple> result;

    private User user;

    private Integer age;

    private int weight;

}
