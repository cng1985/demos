package com.ada.jodd;

import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class RequestApp {

    public static void main(String[] args) {

        String xToken = getXtoken();
        for (int i = 0; i < 1; i++) {
            HttpRequest request = HttpRequest.post("https://user.api.it120.cc/user/copyData/typeB");
            System.out.printf(xToken);
            request.header("X-Token", xToken);
            request.query("merchantId", "27");
            HttpResponse response = request.send();
            String respJson = response.bodyText();
            System.out.println(respJson);
        }
    }

    private static void addProduct(String xToken) {
        HttpRequest request = HttpRequest.post("https://user.api.it120.cc//user/apiExtShopGoods/save");
        System.out.printf(xToken);
        request.header("X-Token", xToken);
        request.query("name", "服装");
        request.query("minPrice", "10");
        request.query("content","1");
        request.query("status", "0");
        request.query("stores","100");
        //request.query("content", "服装");
        request.query("categoryId", "229835");


        //request.query("shopId", "28021");

        HttpResponse response = request.send();
        String respJson = response.bodyText();
        System.out.println(respJson);
    }

    private static void extracted(String xToken) {
        HttpRequest request = HttpRequest.post("https://user.api.it120.cc/user/apiExtShopGoodsCategory/save");
        request.header("X-Token", xToken);
        request.query("name", "服装");
        //request.query("shopId", "28021");

        HttpResponse response = request.send();
        String respJson = response.bodyText();
        System.out.println(respJson);
    }

    private static String getXtoken() {
        HttpRequest request = HttpRequest.post("https://user.api.it120.cc/login/key");
        request.query("merchantKey", "615fe6fce706673da69af72ca5a84ab1");
        request.query("merchantNo", "1906090738497569");

        //request.contentType("application/json");
        request.charset("utf-8");
        Map<String, String> map = new HashMap<>();
        map.put("merchantKey", "615fe6fce706673da69af72ca5a84ab1");
        map.put("merchantNo", "1906090738497569");

        String json = JsonUtil.toJson(map);
        System.out.println(json);
        //参数详情
        if (json != null) {
            // request.body(json);
        }

        HttpResponse response = request.send();
        String respJson = response.bodyText();
        Domain domain = JsonUtil.object(respJson, Domain.class);
        String xtoken=domain.getData();
        return xtoken;
    }

    private static void formPost() {
        HttpRequest httpRequest = HttpRequest
                .post("https://user.api.it120.cc/login/key");

        HttpResponse httpResponse = httpRequest.send();
        httpResponse.charset("utf-8");
        System.out.printf(httpResponse.bodyText());
        List<DataSimple> simples = JsonUtil.object(httpResponse.bodyText(), Demo.class).getResult();
        for (DataSimple simple : simples) {
            System.out.println(simple.getId());
        }
    }
}
