package com.ada.ognl;

import ognl.*;

import java.lang.reflect.Member;
import java.util.Map;

public class App {
    public static void main(String[] args) throws OgnlException {

        String name="ada";
        System.out.println(name.length());
        OgnlContext context = new OgnlContext(new DefaultClassResolver(), new DefaultTypeConverter(), new AbstractMemberAccess() {
            @Override
            public boolean isAccessible(Map map, Object o, Member member, String s) {
                return true;
            }
        });
//            利用context来获取root对象
        Object root = context.getRoot();
//            Ognl中的静态方法getValue(expression, context, root, resultType)可以用来获取数据
        Object value = Ognl.getValue("'helloworld'.length()", context, root);  //expression就是方法表达式
        System.out.println(value.toString());
        context.put("name","ada66");
        value = Ognl.getValue("@java.lang.Math@random()", context, root); //这里注意获取静态方法表达式是固定表达： @类名@方法名
        System.out.println(value.toString());
        value = Ognl.getValue("#name", context, root);
        System.out.println(value);
    }
}
