package com.ada.secure;

import org.bouncycastle.util.encoders.Hex;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;

public class DSAExample {
    public static void main(String[] args) throws Exception {
        // Generate DSA key pair
        KeyPair keyPair = generateDSAKeyPair();

        // Data to be signed
        String data = "Hello, DSA!";
        byte[] dataBytes = data.getBytes();

        // Sign the data using the private key
        byte[] signature = signData(dataBytes, keyPair.getPrivate());

        // Verify the signature using the public key
        boolean verified = verifySignature(dataBytes, signature, keyPair.getPublic());

        System.out.println("Data: " + data);
        System.out.println("Signature: " + Hex.toHexString(signature));
        System.out.println("Signature Verified: " + verified);
    }

    // Generate DSA key pair
    public static KeyPair generateDSAKeyPair() throws Exception {
        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("DSA");
        keyPairGenerator.initialize(1024); // DSA key size in bits
        return keyPairGenerator.generateKeyPair();
    }

    // Sign data using DSA private key
    public static byte[] signData(byte[] data, PrivateKey privateKey) throws Exception {
        Signature dsa = Signature.getInstance("SHA256withDSA");
        dsa.initSign(privateKey);
        dsa.update(data);
        return dsa.sign();
    }

    // Verify DSA signature using public key
    public static boolean verifySignature(byte[] data, byte[] signature, PublicKey publicKey) throws Exception {
        Signature dsa = Signature.getInstance("SHA256withDSA");
        dsa.initVerify(publicKey);
        dsa.update(data);
        return dsa.verify(signature);
    }
}
