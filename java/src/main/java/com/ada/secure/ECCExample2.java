package com.ada.secure;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.*;

public class ECCExample2 {
    public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchProviderException {

        Security.addProvider(new BouncyCastleProvider()); // 添加Bouncy Castle提供者

        KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("EC");
        keyPairGenerator.initialize(256); // ECC key size
        KeyPair keyPair = keyPairGenerator.generateKeyPair();

        System.out.println("ECC Private Key: " + keyPair.getPrivate());
        System.out.println("ECC Public Key: " + keyPair.getPublic());


        String plainText = "Hello, ECC!";
        // Encrypt using ECC public key
        Cipher cipher = Cipher.getInstance("ECIES", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, keyPair.getPublic());
        byte[] encryptedData = cipher.doFinal(plainText.getBytes());

        // Decrypt using ECC private key
        cipher.init(Cipher.DECRYPT_MODE, keyPair.getPrivate());
        byte[] decryptedData = cipher.doFinal(encryptedData);

        System.out.println("Original: " + plainText);
        System.out.println("Encrypted: " + new String(encryptedData));
        System.out.println("Decrypted: " + new String(decryptedData));
    }
}
