package com.ada.secure;

import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.engines.CamelliaEngine;
import org.bouncycastle.crypto.modes.CFBBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.paddings.ZeroBytePadding;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;

public class CamelliaExample {
    public static void main(String[] args) throws Exception {
        Security.addProvider(new BouncyCastleProvider());

        // Initialize Camellia engine and CFB mode
        CamelliaEngine camelliaEngine = new CamelliaEngine();
        BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new CFBBlockCipher(camelliaEngine, 128), new ZeroBytePadding());

        // Key and IV
        byte[] keyBytes = new byte[32]; // 256-bit key
        byte[] iv = new byte[16]; // Initialization Vector

        // Set up key and IV
        KeyParameter keyParameter = new KeyParameter(keyBytes);
        cipher.init(true, keyParameter); // Encryption mode

        // Data to be encrypted
        String plaintext = "Hello, Camellia!";
        byte[] plaintextBytes = plaintext.getBytes();

        // Encrypt the data
        byte[] ciphertext = new byte[cipher.getOutputSize(plaintextBytes.length)];
        int len = cipher.processBytes(plaintextBytes, 0, plaintextBytes.length, ciphertext, 0);
        len += cipher.doFinal(ciphertext, len);

        System.out.println("Plaintext: " + plaintext);
        System.out.println("Ciphertext: " + new String(ciphertext));

        // Decrypt the data
        cipher.init(false, keyParameter); // Decryption mode
        byte[] decrypted = new byte[cipher.getOutputSize(len)];
        len = cipher.processBytes(ciphertext, 0, len, decrypted, 0);
        len += cipher.doFinal(decrypted, len);

        System.out.println("Decrypted: " + new String(decrypted));
    }
}
