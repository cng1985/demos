package com.ada.secure;

import org.bouncycastle.crypto.BufferedBlockCipher;
import org.bouncycastle.crypto.engines.AESEngine;
import org.bouncycastle.crypto.modes.CFBBlockCipher;
import org.bouncycastle.crypto.paddings.PaddedBufferedBlockCipher;
import org.bouncycastle.crypto.paddings.ZeroBytePadding;
import org.bouncycastle.crypto.params.KeyParameter;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.Security;
import java.security.SecureRandom;

public class SecureKeyGenerationExample {
    public static void main(String[] args) throws Exception {
        Security.addProvider(new BouncyCastleProvider());

        // Initialize AES engine and CFB mode
        AESEngine aesEngine = new AESEngine();
        BufferedBlockCipher cipher = new PaddedBufferedBlockCipher(new CFBBlockCipher(aesEngine, 128), new ZeroBytePadding());

        // Generate a random AES key
        byte[] keyBytes = generateRandomKey(128); // 128-bit key

        // Set up key and IV
        KeyParameter keyParameter = new KeyParameter(keyBytes);
        cipher.init(true, keyParameter); // Encryption mode

        // Data to be encrypted
        String plaintext = "Hello, Secure Key Generation!";
        byte[] plaintextBytes = plaintext.getBytes();

        // Encrypt the data
        byte[] ciphertext = new byte[cipher.getOutputSize(plaintextBytes.length)];
        int len = cipher.processBytes(plaintextBytes, 0, plaintextBytes.length, ciphertext, 0);
        len += cipher.doFinal(ciphertext, len);

        System.out.println("Plaintext: " + plaintext);
        System.out.println("Ciphertext: " + new String(ciphertext));

        // Decrypt the data
        cipher.init(false, keyParameter); // Decryption mode
        byte[] decrypted = new byte[cipher.getOutputSize(len)];
        len = cipher.processBytes(ciphertext, 0, len, decrypted, 0);
        len += cipher.doFinal(decrypted, len);

        System.out.println("Decrypted: " + new String(decrypted).trim());
    }

    // Generate a random key of specified length
    public static byte[] generateRandomKey(int keyLength) {
        SecureRandom random = new SecureRandom();
        byte[] keyBytes = new byte[keyLength / 8]; // Convert bits to bytes
        random.nextBytes(keyBytes);
        return keyBytes;
    }
}
