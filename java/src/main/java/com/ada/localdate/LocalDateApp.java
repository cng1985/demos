package com.ada.localdate;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class LocalDateApp {

    public static void main(String[] args) {
        DateTimeFormatter format=DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime time=   LocalDateTime.parse("2019-05-31 15:13:56",format);
        System.out.println(time);
    }
}
