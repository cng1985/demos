package com.ada.rmi;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RegistryClient {

    public static void main(String[] args) {
        try {
            Registry registry = LocateRegistry.getRegistry(1099);
            HelloRegistryFacade hello = (HelloRegistryFacade) registry.lookup("HelloRegistry");
            String response = hello.helloWorld("ada66");
            System.out.println("=======> " + response + " <=======");

            Long time=System.currentTimeMillis();
            int num=10000;
            for (int i = 0; i < num; i++) {
                CallBackRequest request=new CallBackRequest();
                StringBuffer buffer=new StringBuffer();
                for (int j = 0; j < 100; j++) {
                    buffer.append("ada"+i);
                }
                request.setName(buffer.toString());
                //request.setCallback(item->item+":66");
                String res= hello.call(request);
                //System.out.println(res);
            }
            time=System.currentTimeMillis()-time;
            System.out.println(time);
            System.out.println(num/(time/1000.0));
        } catch (NotBoundException | RemoteException e) {
            e.printStackTrace();
        }
    }
}
