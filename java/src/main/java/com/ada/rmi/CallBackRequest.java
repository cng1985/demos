package com.ada.rmi;

import lombok.Data;

import java.io.Serializable;
import java.util.function.Function;

@Data
public class CallBackRequest implements Serializable {

    private String name;

    private Function<String,String> callback;
}
