package com.ada.rmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class HelloRegistryFacadeImpl extends UnicastRemoteObject implements HelloRegistryFacade {

    public HelloRegistryFacadeImpl() throws RemoteException {
        super();
    }

    @Override
    public String helloWorld(String name) {
        return "[Registry] 你好! " + name;
    }

    @Override
    public String call(CallBackRequest request) throws RemoteException {
        if (request.getCallback() != null) {
            return request.getCallback().apply(request.getName());
        }
        return request.getName();
    }

}