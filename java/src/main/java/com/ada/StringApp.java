package com.ada;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class StringApp {

    public static void main(String[] args) {

        String hello="dfdsfsd";
        System.out.println(hello.split(",")[0]);

        String path = "/disk/test/";
        String downPath = "/disk/";


        List<Item> items = new ArrayList<>();
        items.add(Item.builder().name("elecricty").sum(123d).build());
        items.add(Item.builder().name("type").sum(123d).build());
        items.add(Item.builder().name("test").sum(123d).build());

        List<String> maps = new ArrayList<>();
        List<String> maps2 = new ArrayList<>();

        items.stream().forEach(item -> {
            maps.add(item.getName());
            maps2.add(item.getName());

        });

        BigDecimal price=new BigDecimal("125652.2565");
        NumberFormat currency = NumberFormat.getCurrencyInstance();
        currency.setMaximumFractionDigits(2);
        System.out.println(currency.format(price));
        System.out.println(currency.format(new BigDecimal("0.0165")));
        System.out.println(new BigDecimal("123").setScale(2, RoundingMode.UP));
        System.out.println(new BigDecimal("1").divide(new BigDecimal("3"), MathContext.DECIMAL64).setScale(2, RoundingMode.UP));

//        String name="ada";
//        String str = """
//                  hello
//                  i am ${name}
//
//                """;
//        System.out.println(str);
        //path.substring(path.indexOf(downPath) + Integer.parseInt(wxPayConfig.getDownLoadPathLength()), path.length())+ File.separator+ newName;

        BigDecimal num1 = new BigDecimal("10000000");
        BigDecimal num2 = new BigDecimal("3");

        BigDecimal result = num1.divide(num2, 2, RoundingMode.UP);

        System.out.println(result);  // 输出结果：3.33

        Calendar calendar=Calendar.getInstance();

        calendar.setFirstDayOfWeek(Calendar.SUNDAY);
        calendar.setTime(new Date());
        calendar.add(Calendar.DAY_OF_YEAR,0);
       //  calendar.set(Calendar.DAY_OF_YEAR,6);
        System.out.println(calendar.get(Calendar.DAY_OF_YEAR));
        System.out.println(calendar.get(Calendar.WEEK_OF_YEAR));

        System.out.println("ww:"+(calendar.get(Calendar.DAY_OF_YEAR)/7+1));
        SimpleDateFormat format=new SimpleDateFormat("yyyy-ww", Locale.CHINA);
        System.out.println(format.format(calendar.getTime()));

    }
}
