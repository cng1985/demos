package com.ada.yaml;

import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class App {

    public static void main(String[] args) throws IOException {

        Yaml yaml = new Yaml();
        System.out.println(App.class.getResource(".").getFile());
        File f = new File(App.class.getResource(".").getFile() + "test.yaml");
        //读入文件
        Object result = yaml.load(Files.newInputStream(f.toPath()));
        System.out.println(result.getClass());
        System.out.println(result);

        Config config = yaml.loadAs(Files.newInputStream(f.toPath()), Config.class);
        System.out.println(config);
    }
}
