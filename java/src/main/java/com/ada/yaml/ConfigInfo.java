package com.ada.yaml;


import lombok.Data;

import java.util.List;

@Data
public class ConfigInfo {

    private String image;

    private List<String> volumes;

    private List<String> command;
}
