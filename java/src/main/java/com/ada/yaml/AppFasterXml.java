package com.ada.yaml;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.util.Map;

public class AppFasterXml {

    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());

        String path = AppFasterXml.class.getResource("./test.yaml").getFile();
        System.out.println(path);
        Map u2 = mapper.readValue(new File(path), Map.class);
        System.out.println(u2.get("web"));
        System.out.println(u2);
        //配置文件路径
    }
}
