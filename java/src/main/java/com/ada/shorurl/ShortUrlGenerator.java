package com.ada.shorurl;

public class ShortUrlGenerator {
    private static final String CHARACTERS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final int BASE = CHARACTERS.length();
    private static final int SHORT_URL_LENGTH = 6;

    private int counter;

    public ShortUrlGenerator() {
        counter = 0;
    }

    public String generateShortUrl() {
        StringBuilder shortUrl = new StringBuilder();
        int value = counter;

        while (value > 0) {
            shortUrl.insert(0, CHARACTERS.charAt(value % BASE));
            value /= BASE;
        }

        // Pad the short URL if needed
        while (shortUrl.length() < SHORT_URL_LENGTH) {
            shortUrl.insert(0, CHARACTERS.charAt(0));
        }

        // Increment the counter
        counter++;

        return shortUrl.toString();
    }
    public static long base62ToBase10(String base62) {
        long base10 = 0;
        int power = 0;

        for (int i = base62.length() - 1; i >= 0; i--) {
            int digit = CHARACTERS.indexOf(base62.charAt(i));
            base10 += digit * Math.pow(BASE, power);
            power++;
        }

        return base10;
    }
    public static void main(String[] args) {
        ShortUrlGenerator generator = new ShortUrlGenerator();
        generator.counter=5656;
        for (int i = 0; i < 10; i++) {
            String shortUrl = generator.generateShortUrl();
            System.out.println(base62ToBase10(shortUrl));
            System.out.println("Short URL: " + shortUrl);
        }
    }
}
