package com.ada.shorurl;

import java.util.ArrayList;
import java.util.List;

public class BaseConverter {
    public static String decimalToBase(long decimal, int base) {
        if (base < 2 || base > 36) {
            throw new IllegalArgumentException("Invalid base. Base must be between 2 and 36.");
        }

        List<Character> digits = new ArrayList<>();

        while (decimal > 0) {
            long remainder = decimal % base;
            digits.add(getCharForDigit((int) remainder));
            decimal /= base;
        }

        StringBuilder baseN = new StringBuilder();

        for (int i = digits.size() - 1; i >= 0; i--) {
            baseN.append(digits.get(i));
        }

        return baseN.toString();
    }

    public static long baseToDecimal(String baseN, int base) {
        if (base < 2 || base > 36) {
            throw new IllegalArgumentException("Invalid base. Base must be between 2 and 36.");
        }

        long decimal = 0;
        int power = 0;

        for (int i = baseN.length() - 1; i >= 0; i--) {
            char c = baseN.charAt(i);
            int digit = getDigitForChar(c);
            decimal += digit * Math.pow(base, power);
            power++;
        }

        return decimal;
    }

    private static char getCharForDigit(int digit) {
        if (digit >= 0 && digit <= 9) {
            return (char) (digit + '0');
        } else if (digit >= 10 && digit <= 35) {
            return (char) (digit - 10 + 'A');
        } else {
            throw new IllegalArgumentException("Invalid digit: " + digit);
        }
    }

    private static int getDigitForChar(char c) {
        if (Character.isDigit(c)) {
            return c - '0';
        } else if (Character.isUpperCase(c)) {
            return c - 'A' + 10;
        } else if (Character.isLowerCase(c)) {
            return c - 'a' + 10;
        } else {
            throw new IllegalArgumentException("Invalid character: " + c);
        }
    }

    public static void main(String[] args) {
        long decimal = 111100;
        for (int i = 0; i < 90000000; i++) {
            extracted(decimal+i);
        }
    }

    private static void extracted(long decimal) {
        int base = 35;

        String baseN = decimalToBase(decimal, base);
        long convertedDecimal = baseToDecimal(baseN, base);

        System.out.println("Decimal: " + decimal);
        System.out.println("Base " + base + ": " + baseN);
        System.out.println("Converted Decimal: " + convertedDecimal);
    }
}
