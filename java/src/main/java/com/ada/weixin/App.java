package com.ada.weixin;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;

public class App {

    public static void main(String[] args) throws WxErrorException {
        WxMpDefaultConfigImpl config = new WxMpDefaultConfigImpl();
        config.setAppId("wxb6e2a2208c407fcb"); // 设置微信公众号的appid
        config.setSecret("e213b22e1c6cb974960da5e858704894"); // 设置微信公众号的app corpSecret
        config.setToken(""); // 设置微信公众号的token
        config.setAesKey(""); // 设置微信公众号的EncodingAESKey

        WxMpService wxService = new WxMpServiceImpl();// 实际项目中请注意要保持单例，不要在每次请求时构造实例，具体可以参考demo项目
        wxService.setWxMpConfigStorage(config);
//        WxOpenService openService = new WxOpenServiceImpl();
//        WxOpenConfigStorage conf = new WxOpenInMemoryConfigStorage();
//        conf.setComponentAppId("wxb6e2a2208c407fcb");
//        conf.setComponentAppSecret("e213b22e1c6cb974960da5e858704894");
//        conf.setComponentToken("metamall");
//        conf.setComponentVerifyTicket("26_E49UZeLTUiveulCGl9SQO0leYGHfgMLyQtfKzuHGaSXH_uc-MQ7ASFIkWSLs3TdFLupgaNCnGcaQSDRwpVzOw-WeWBdDR9C7oGHhHQt6_ZjheymDTv7G3CyoiswbrEu39PdrxG2oEM5tMAwdRQDdAFAWVI");
//        conf.setComponentAesKey("asdfghjklqwertyuiopzxcvbn123456789012345678");
//        openService.setWxOpenConfigStorage(conf);
//        WxOpenComponentService wxOpenService = new WxOpenComponentServiceImpl(openService);
//        String token = wxOpenService.getComponentAccessToken(true);
//        //wxOpenService.get()
//        System.out.printf(token);
    }
}
