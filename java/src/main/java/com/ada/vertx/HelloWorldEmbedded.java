package com.ada.vertx;

import io.vertx.core.Vertx;

public class HelloWorldEmbedded {

    public static void main(String[] args) {
        // Create an HTTP server which simply returns "Hello World!" to each request.
        Vertx.vertx().createHttpServer().requestHandler(req ->{ req.response().end("Hello World!");
            System.out.println(req.uri());
        }).listen(8080);

    }
}
