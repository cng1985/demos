package com.ada.xpath;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.io.SAXReader;
import org.dom4j.Node;
import org.jsoup.Jsoup;

import java.io.StringReader;
import java.util.List;

public class Dom4jXPathExample {
    public static void main(String[] args) throws DocumentException {
        String htmlCode = "<div class=\"TB_body_5-70-0 TB_bodyGroup_5-70-0\" data-testid=\"beast-core-table-middle-body\" alirpa-token=\"0/4\"><div><table class=\"TB_tableWrapper_5-70-0\"><colgroup><col style=\"width: 20%;\"><col style=\"width: 11%;\"><col style=\"width: 6%;\"><col style=\"width: 10%;\"><col style=\"width: 10%;\"><col style=\"width: 18%;\"><col style=\"width: 8%;\"><col style=\"width: 17%;\"></colgroup><tbody data-testid=\"beast-core-table-middle-tbody\"><tr class=\"TB_bodyGroupHeader_5-70-0 TB_tr_5-70-0\"><td class=\"TB_bodyGroupCell_5-70-0 TB_td_5-70-0\" colspan=\"8\" style=\"height: 36px; padding-top: unset; padding-bottom: unset;\"><div style=\"font-size: 12px;\"><div style=\"display: flex; justify-content: space-between; align-items: center;\"><div style=\"display: flex; justify-content: space-between; line-height: 20px; flex: 1 1 0%;\"><div style=\"display: flex; flex: 1 1 0%; color: rgba(0, 0, 0, 0.6); align-items: center;\"><span style=\"margin-right: 10px; color: rgba(0, 0, 0, 0.8); white-space: nowrap;\">订单编号：230705-153642607431875</span><div style=\"margin-right: 8px; white-space: nowrap;\"><a class=\"BTN_outerWrapper_5-70-0 BTN_textPrimary_5-70-0 BTN_small_5-70-0 BTN_outerWrapperLink_5-70-0\" data-testid=\"beast-core-button-link\"><span style=\"line-height: 14px;\">复制</span></a></div><div style=\"flex: 1 1 0%;\"></div></div><div><span style=\"color: rgba(0, 0, 0, 0.6);\">成交时间：2023-07-05 11:00</span><span></span></div></div> </div></div></td></tr><tr data-testid=\"beast-core-table-body-tr\" class=\"TB_tr_5-70-0 \"><td data-testid=\"beast-core-table-td\" colspan=\"1\" class=\"TB_td_5-70-0 TB_cellTextAlignLeft_5-70-0 TB_cellVerticalAlignMiddle_5-70-0 TB_lastCell_5-70-0\" style=\"width: 20%;\"><div class=\"goods-record-container\"><div data-testid=\"beast-core-box\" class=\"outerWrapper-1-3-1 outerWrapper-d8-1-3-10 imgWrapper\"><img data-retry-count=\"0\" data-retry-status=\"success\" data-volta=\"9e407233-7469-443a-847a-02320f67ada0\" data-bimg-src=\"\" src=\"https://img.pddpic.com/gaudit-image/2023-07-05/dead4331670b449890a3fb413b54cdf3.jpeg?imageView2/2/w/46/q/85/format/webp\" style=\"width: 46px; height: 46px; cursor: pointer;\"><div data-testid=\"beast-core-box\" class=\"outerWrapper-1-3-1 outerWrapper-d9-1-3-11 preview\">预览</div></div><div style=\"margin-left: 8px; flex: 1 1 0%;\"><div data-testid=\"beast-core-ellipsis\"><div class=\"elli_outerWrapper_5-70-0 elli_lineClamp_5-70-0 beast-core-ellipsis-1\"><style data-testid=\"beast-core-ellipsis-style\">.beast-core-ellipsis-1{-webkit-line-clamp:1;-webkit-box-orient: vertical;}</style><a class=\"_list-goodsName\" data-volta=\"7502ae94-bf01-47d3-bdc8-8405be371987\">透明胶带(≧100m)</a></div></div><p style=\"margin-top: 0px; color: rgba(0, 0, 0, 0.8); word-break: keep-all;\">ID: 498198046792</p><div></div></div></div></td><td data-testid=\"beast-core-table-td\" class=\"TB_td_5-70-0 TB_cellTextAlignLeft_5-70-0 TB_cellVerticalAlignMiddle_5-70-0 TB_lastCell_5-70-0\"><div class=\"orderState\">未发货，退款成功<em style=\"display: none;\">已缺货处理</em><a data-volta=\"d9d96a3b-6f46-47ef-8d6b-19433dcec36d\" data-tracking=\"87001\" class=\"BTN_outerWrapper_5-70-0 BTN_textPrimary_5-70-0 BTN_small_5-70-0 BTN_outerWrapperLink_5-70-0\" data-testid=\"beast-core-button-link\" style=\"margin: 0px;\"><span>退款成功</span></a></div></td><td data-testid=\"beast-core-table-td\" class=\"TB_td_5-70-0 TB_cellTextAlignLeft_5-70-0 TB_cellVerticalAlignMiddle_5-70-0 TB_lastCell_5-70-0\">1</td><td data-testid=\"beast-core-table-td\" class=\"TB_td_5-70-0 TB_cellTextAlignLeft_5-70-0 TB_cellVerticalAlignMiddle_5-70-0 TB_lastCell_5-70-0\">11.00</td><td data-testid=\"beast-core-table-td\" class=\"TB_td_5-70-0 TB_cellTextAlignLeft_5-70-0 TB_cellVerticalAlignMiddle_5-70-0 TB_lastCell_5-70-0\"><div style=\"text-align: left;\"><span class=\"BTN_outerWrapper_5-70-0 BTN_textTip_5-70-0 BTN_small_5-70-0 BTN_outerWrapperLink_5-70-0\"><span>11.00</span></span></div></td><td data-testid=\"beast-core-table-td\" class=\"TB_td_5-70-0 TB_cellTextAlignLeft_5-70-0 TB_cellVerticalAlignMiddle_5-70-0 TB_lastCell_5-70-0\"><div style=\"display: flex; flex-direction: column;\"><div data-testid=\"beast-core-box\" class=\"outerWrapper-1-3-1 outerWrapper-d10-1-3-12\"><div><div style=\"display: flex;\"><div data-testid=\"beast-core-ellipsis\"><div class=\"elli_outerWrapper_5-70-0 elli_lineClamp_5-70-0 beast-core-ellipsis-1\"><style data-testid=\"beast-core-ellipsis-style\">.beast-core-ellipsis-1{-webkit-line-clamp:1;-webkit-box-orient: vertical;}</style><span>****</span></div></div><div data-testid=\"beast-core-box\" class=\"outerWrapper-1-3-1 outerWrapper-d11-1-3-13\"><div><a class=\"BTN_outerWrapper_5-70-0 BTN_textPrimary_5-70-0 BTN_small_5-70-0 BTN_outerWrapperLink_5-70-0\" data-testid=\"beast-core-button-link\" style=\"margin-left: 8px;\"><span>查看</span></a></div></div></div></div></div><div data-testid=\"beast-core-box\" class=\"outerWrapper-1-3-1 outerWrapper-d12-1-3-14\"><div data-testid=\"beast-core-ellipsis\"><div class=\"elli_outerWrapper_5-70-0 elli_lineClamp_5-70-0 beast-core-ellipsis-2\"><style data-testid=\"beast-core-ellipsis-style\">.beast-core-ellipsis-2{-webkit-line-clamp:2;-webkit-box-orient: vertical;}</style>**** **** **** ****</div></div></div><div></div></div></td><td data-testid=\"beast-core-table-td\" class=\"TB_td_5-70-0 TB_cellTextAlignLeft_5-70-0 TB_cellVerticalAlignMiddle_5-70-0 TB_lastCell_5-70-0\"><div style=\"display: flex; flex-direction: column;\">月***<span data-volta=\"ca98b81e-fbfc-4049-b3d8-8f232c546534\"><a data-tracking=\"93668\" class=\"BTN_outerWrapper_5-70-0 BTN_textPrimary_5-70-0 BTN_small_5-70-0 BTN_outerWrapperLink_5-70-0\" data-testid=\"beast-core-button-link\" style=\"margin: 0px;\"><span>发起聊天</span></a></span></div></td><td data-testid=\"beast-core-table-td\" class=\"TB_td_5-70-0 TB_cellTextAlignLeft_5-70-0 TB_cellVerticalAlignMiddle_5-70-0 TB_rightmostTd_5-70-0 TB_lastCell_5-70-0\"><div data-testid=\"beast-core-box\" class=\"outerWrapper-1-3-1 outerWrapper-d13-1-3-15\"><div class=\"Space_outWrapper_5-70-0 Space_alignStart_5-70-0 Space_horizontal_5-70-0\" style=\"row-gap: 0px;\"><div style=\"margin-right: 8px;\"><a data-volta=\"6475f0a8-9307-4f54-93d2-5934ba4c07f9\" data-tracking=\"87003\" class=\"BTN_outerWrapper_5-70-0 BTN_textPrimary_5-70-0 BTN_small_5-70-0 BTN_outerWrapperLink_5-70-0\" data-testid=\"beast-core-button-link\"><span>查看详情</span></a></div><div><a data-volta=\"89aeb4c6-42b7-4147-a497-aa23886c205c\" class=\"BTN_outerWrapper_5-70-0 BTN_textPrimary_5-70-0 BTN_small_5-70-0 BTN_outerWrapperLink_5-70-0\" data-testid=\"beast-core-button-link\"><span>添加备注</span></a></div></div></div></td></tr></tbody></table></div></div>";
        org.jsoup.nodes.Document doc = Jsoup.parse(htmlCode);
        doc.select("colgroup").remove();
        doc.outputSettings().syntax(org.jsoup.nodes.Document.OutputSettings.Syntax.xml);
        String fixedHtml = doc.html();
        // 创建DOM4J的SAXReader对象
        SAXReader reader = new SAXReader();
        Document document = reader.read(new StringReader(fixedHtml));

        // 提取订单编号
        Node orderNumberNode = document.selectSingleNode("//span[contains(text(), '订单编号')]/text()");
        String orderNumber = orderNumberNode.getText().trim();
        System.out.println("订单编号: " + orderNumber);

        // 提取交易时间
        Node transactionTimeNode = document.selectSingleNode("//span[contains(text(), '成交时间')]/text()");
        String transactionTime = transactionTimeNode.getText().trim();
        System.out.println("交易时间: " + transactionTime);

        // 提取产品名称
        Node productNameNode = document.selectSingleNode("//a[@class='_list-goodsName']/text()");
        String productName = productNameNode.getText().trim();
        System.out.println("产品名称: " + productName);

        // 提取产品ID
        Node productIDNode = document.selectSingleNode("//p[contains(text(), 'ID:')]/text()");
        String productID = productIDNode.getText().trim();
        System.out.println("产品ID: " + productID);

        // 提取订单状态
        Node orderStatusNode = document.selectSingleNode("//div[@class='orderState']/text()");
        String orderStatus = orderStatusNode.getText().trim();
        System.out.println("订单状态: " + orderStatus);

        // 提取数量
        Node quantityNode = document.selectSingleNode("//td[@data-testid='beast-core-table-td'][3]/text()");
        String quantity = quantityNode.getText().trim();
        System.out.println("数量: " + quantity);

        // 提取单价
        Node unitPriceNode = document.selectSingleNode("//td[@data-testid='beast-core-table-td'][4]/text()");
        String unitPrice = unitPriceNode.getText().trim();
        System.out.println("单价: " + unitPrice);
    }
}
