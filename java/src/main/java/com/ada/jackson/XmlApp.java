package com.ada.jackson;

import com.ada.jackson.domain.User;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XmlApp {
  public static void main(String[] args) {
    JsonFactory factory = new JsonFactory();
// configure, if necessary:
    factory.enable(JsonParser.Feature.ALLOW_COMMENTS);
    List<User> users = getUsers();
    ObjectMapper mapper = new XmlMapper();
    mapper.enable(SerializationFeature.INDENT_OUTPUT);
    mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);
    try {
      String body = mapper.writeValueAsString(users);
      System.out.println(body);
      JsonNode root = mapper.readTree(body);
      System.out.println(root.toString());
      List<User> usercpnver = mapper.readValue(body, List.class);
      System.out.println(usercpnver);
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private static List<User> getUsers() {
    List<User> users = new ArrayList<>();
    for (int i = 0; i < 10; i++) {
      User user = new User();
      user.setAge(i);
      user.setName("ada" + i);
      users.add(user);
    }
    return users;
  }
}
