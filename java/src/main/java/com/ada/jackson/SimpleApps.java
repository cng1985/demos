package com.ada.jackson;

import com.ada.jackson.domain.User;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SimpleApps {

  public static void main(String[] args) {

    SimpleUser user=new User();
    ObjectMapper mapper = new ObjectMapper();
    try {
      System.out.println(mapper.writeValueAsString(user));
    } catch (JsonProcessingException e) {
      e.printStackTrace();
    }

  }
}
