package com.ada.jackson;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

public class ObjectMapperApps {
    public static void main(String[] args) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode node= mapper.createObjectNode();
        node.put("age",33);
        System.out.println(node.toString());

        JsonNode xnote= mapper.readTree(node.toString());
        System.out.println(xnote.get("age").asInt());
    }
}
