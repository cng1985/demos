package com.ada.jwt;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;

import java.util.Calendar;
import java.util.Date;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import java.security.Key;


public class App {

    public static void main(String[] args) {
        try {
            Algorithm algorithm = Algorithm.HMAC256("11111");
            Calendar calendar=Calendar.getInstance();
            calendar.add(Calendar.DAY_OF_YEAR,1);
            String token = JWT.create()
                    .withIssuer("auth0")
                    .withJWTId("123")
                    .withKeyId("456")
                    .withSubject("sdfsdf")
                    .withClaim("ada",5698L)
                    .withExpiresAt(calendar.getTime())
                    .sign(algorithm);


            JWTVerifier verifier = JWT.require(algorithm)
                    // specify an specific claim validations
                    .withIssuer("auth0")
                    // reusable verifier instance
                    .build();

            DecodedJWT decodedJWT = verifier.verify(token);
            System.out.println(decodedJWT.getSubject());
            System.out.println(decodedJWT.getId());

            System.out.println(token);
            System.out.println(JWT.decode(token).getExpiresAt());

            System.out.println(JWT.decode(token).getId());
            System.out.println(JWT.decode(token).getSubject());
            System.out.println(JWT.decode(token).getClaim("ada").asLong());

        } catch (JWTCreationException exception){
            // Invalid Signing configuration / Couldn't convert Claims.
        }
//
        String subject = "user123"; // 主题
        String issuer = "your-app-name"; // 发行者
        long expiration = System.currentTimeMillis() + 3600000; // 令牌有效期，1小时
        String secretKey = "your-secret-key"; // 私钥，用于签署令牌

        Algorithm.HMAC256(secretKey);


//        SecretKey key = Jwts.SIG.HS256.key().build();
//
//        String jws = Jwts.builder().subject("Joe").signWith(key).compact();
    }
}
