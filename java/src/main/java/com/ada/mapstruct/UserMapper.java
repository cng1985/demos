package com.ada.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper( UserMapper.class );

    @Mapping(target = "name",source = "name")
    @Mapping(target = "carName",source = "car.name")
    @Mapping(target = "buyYear",source = "car.buyYear")
    UserSimple convert(User user);


}
