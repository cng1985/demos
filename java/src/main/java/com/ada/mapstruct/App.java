package com.ada.mapstruct;

public class App {

    public static void main(String[] args) {

        Long time=System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            User user = User.builder().name("ada").age(18).build();
            user.setCar(Car.builder().name("大众").buyYear(10).build());
            UserSimple simple = UserMapper.INSTANCE.convert(user);
        }
        time=System.currentTimeMillis()-time;
        System.out.println(time);
    }
}
