package com.ada.mapstruct;


import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
public class Car implements Serializable {

    private String name;

    private Integer buyYear;

}
