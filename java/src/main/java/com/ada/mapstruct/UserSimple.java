package com.ada.mapstruct;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
public class UserSimple implements Serializable {

    private String name;

    private Integer age;


    private String carName;

    private Integer buyYear;

    private Integer carBuyYear;

}
