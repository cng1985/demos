package com.ada.word;

import com.deepoove.poi.XWPFTemplate;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

public class App {

    public static void main(String[] args) throws IOException {
        XWPFTemplate template = XWPFTemplate.compile("e:\\title.docx").render(
                new HashMap<String, Object>(){{
                    put("title", "Hi, poi-tl Word模板引擎");
                }});
        template.writeAndClose(new FileOutputStream("e:\\output.docx"));
    }
}
