package com.ada.lock;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class App {

    public static void main(String[] args) {
        Lock lock=new ReentrantLock();
        lock.lock();
        lock.lock();
        lock.unlock();
        lock.unlock();
    }
}
