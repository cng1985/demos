package com.ada.jsonl;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class JsonlReadExample {
    public static void main(String[] args) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        JsonFactory jsonFactory = objectMapper.getFactory();
        int i=0;
        //https://world.openfoodfacts.org/api/v0/product/737628064502.json

        try (BufferedReader reader = new BufferedReader(new FileReader("E:\\datas\\openfoodfacts-products.jsonl"))) {
            String line;
            while ((line = reader.readLine()) != null) {

                //System.out.println(jsonNode.toPrettyString());

                // Now you can process the JSON object in jsonNode
                // For example:
                try {
                    JsonParser jsonParser = jsonFactory.createParser(line);
                    JsonNode jsonNode = objectMapper.readTree(jsonParser);
                    String name = jsonNode.get("product_name").asText();
                    String id = jsonNode.get("_id").asText();

                    // Do something with the extracted data
                    System.out.println("Name: " + name +"  id: "+id);

                    jsonParser.close();
                    if (name.length()>1){
                        i++;
                    }
                }catch (Exception e){

                }


            }
        }


        System.out.println(i);
    }
}
