package com.ada.vfs;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;

import java.io.*;
import java.nio.ByteBuffer;

public class Apps {

    public static void main(String[] args) {
        try {
            FileSystemManager fsm = VFS.getManager();
            String[] sc = fsm.getSchemes();
            for (String s : sc) {
                System.out.println(s);

            }

            FileObject file=   fsm.resolveFile("http://www.cnblogs.com/lovebread/archive/2009/11/23/1609122.html");
            System.out.println(file.getFileSystem().getRootName());
            InputStreamReader inputStream=new InputStreamReader(file.getContent().getInputStream());
            BufferedReader     reader = new BufferedReader(inputStream);
            reader.lines().forEach(s-> System.out.println(s));
            String[] attrs = file.getContent().getAttributeNames();
            for (String attr : attrs) {
                System.out.println(attr);
            }
            ByteBuffer buffer = ByteBuffer.allocate(48);

        } catch (FileSystemException e) {
            e.printStackTrace();
        }
    }
}
