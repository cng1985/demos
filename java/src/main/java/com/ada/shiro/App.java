package com.ada.shiro;

import org.apache.shiro.authc.credential.DefaultPasswordService;
import org.apache.shiro.authc.credential.PasswordService;
import org.apache.shiro.codec.Base64;
import org.apache.shiro.crypto.RandomNumberGenerator;
import org.apache.shiro.crypto.SecureRandomNumberGenerator;
import org.apache.shiro.crypto.hash.Md5Hash;
import org.apache.shiro.crypto.hash.Sha256Hash;
import org.apache.shiro.crypto.hash.Sha512Hash;
import org.apache.shiro.crypto.hash.SimpleHash;
import org.apache.shiro.util.ByteSource;

public class App {

    public static void main(String[] args) {

        PasswordService service = new DefaultPasswordService();

        String password = service.encryptPassword("123456");
        System.out.println(password);
        boolean state = service.passwordsMatch("123456", password);
        System.out.println(state);

        String hashAlgorithmName = "SHA-256";
        Object credentials = "123456";
        Object salt = ByteSource.Util.bytes("user");;
        int hashIterations = 1024*10000;
        SimpleHash result = new SimpleHash(hashAlgorithmName, credentials, salt, hashIterations);
        System.out.println(result.getSalt());
        System.out.println(result.toBase64());
        RandomNumberGenerator generator=new SecureRandomNumberGenerator();
        System.out.println(generator.nextBytes(32).toBase64().length());
        String salt1="tpnLoBEDtwGpaWKJCjI2IwMP9Ilfm4XoSIlBWcv1szo=";
        System.out.println(ByteSource.Util.bytes(Base64.decode(salt1)).toBase64());

        Md5Hash md5Hash=new Md5Hash("123456");
        System.out.println(md5Hash.toBase64());
        Sha256Hash sha256Hash=new Sha256Hash("123456",ByteSource.Util.bytes(Base64.decode(salt1)),2048);
        System.out.println(sha256Hash.toBase64());

        a(-1);




    }
    public static void a(int i){
        //如果i<0那么就会抛出异常
        assert i>0;
        System.out.println(i);
    }
}
