package com.ada.classloader;

import java.io.IOException;
import java.net.URL;
import java.util.Enumeration;

public class App {

    public static void main(String[] args) throws IOException {
        Enumeration<URL> dirs = Thread.currentThread().getContextClassLoader().getResources("com");
        while(dirs.hasMoreElements()) {
            System.err.println(dirs.nextElement());
        }
        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        while (classLoader != null) {
            System.out.println("ClassLoader: " + classLoader);
            listLoadedClasses(classLoader);
            classLoader = classLoader.getParent();
        }
        try {
            testClassLoad(classLoader);

        } catch (Exception e) {

        }
    }
    public static void listLoadedClasses(ClassLoader classLoader) {
        Class<?>[] classes = ClassUtils.getAllLoadedClasses(classLoader);
        if (classes != null) {
            for (Class<?> clazz : classes) {
                System.out.println(clazz.getName());
            }
        }
    }
        private static void testClassLoad(ClassLoader classLoader) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
            Class<?> c = classLoader.loadClass("com.ada.classloader.Request");
            System.out.println(c.getName());
            Object obj = c.newInstance();
            if (obj instanceof Request) {
                ((Request) obj).setName("ada");
                System.out.println(((Request) obj).getName());
            }
            System.out.println(classLoader.toString());
        }
    }
