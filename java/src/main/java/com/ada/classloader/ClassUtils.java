package com.ada.classloader;

import java.io.IOException;
import java.lang.Class;
import java.util.ArrayList;
import java.util.List;

public class ClassUtils {
    public static Class<?>[] getAllLoadedClasses(ClassLoader classLoader) {
        // 获取ClassLoader加载的Class的方式因ClassLoader实现而异，这里以简单方式演示
        List<Class<?>> classes = new ArrayList<>();
        String[] classNames = new String[0];
        try {
            classNames = classLoader.getResources("").toString().split(",");
        } catch (IOException e) {
        }
        for (String className : classNames) {
            try {
//                    Class<?> clazz = Class.forName(className.trim());
//                    System.out.println(clazz.getName());
                System.out.println(className);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return classes.toArray(new Class[0]);
    }
}
