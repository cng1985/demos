package com.ada.classloader;

import java.net.URL;
import java.net.URLClassLoader;

/**
 * @author ada
 * @date 2022/4/18
 */
public class MyURLClassLoader extends URLClassLoader {
    public MyURLClassLoader(URL[] urls) {
        super(urls);
    }

    public void  add(URL url){
        this.addURL(url);
    }
}
