package com.ada.games;

import java.io.*;
import java.net.*;
import java.util.ArrayList;


public class Server 
{
    public static final int PORT = 8180;
    public static ArrayList playerList = new ArrayList();
    public static ArrayList groupList = new ArrayList();
    public static void main(String[] args)throws IOException 
    {
        ServerSocket s = new ServerSocket(PORT);
        System.out.println("Welcome using ChessWZQ1.0 server (Kahn test)...");
        System.out.println("Server Started at port "+PORT+"...");

        try 
		{
            while(true) 
			{
                // ��ʼ����ʱ������ֹ:
                Socket socket = s.accept();
                try {
                    ServeOneClient server = new ServeOneClient(socket);
                    Player client = new Player();
                    client.selfSocket = server;
                    playerList.add(client);
                    //System.out.println("create a socket frome server");
                } 
				catch(IOException e) 
				{
                    // ���ʧ�ܹرն˿�,
                    // �����̻߳�ر���

                    socket.close();
                }
            }
        } 
		finally 
		{
            s.close();
        }
    }

}
