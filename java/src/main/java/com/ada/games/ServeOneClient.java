package com.ada.games;

import java.io.*;
import java.net.*;

public class ServeOneClient extends Thread
{
    private Socket socket;
    private String player=null;
    protected ObjectInputStream in;
    protected ObjectOutputStream out;
    public ServeOneClient(Socket s) throws IOException 
	  {
        socket = s;
        //System.out.println("server socket begin ...");

        in =new ObjectInputStream(
            socket.getInputStream());
        
        out = new ObjectOutputStream(
            socket.getOutputStream());
        //System.out.println("server socket in and out ...");

       //������е������׳��쳣�������߸���ر��׽��֣������̻߳Ὣ��ر�
        start(); 
      }
    public void run() 
	    {

        Message obj=new Message();
        while (true) 
		{
            try {
                obj =(Message)in.readObject();
                
                doMessage(obj);
            }catch(ClassNotFoundException er){}
            catch(IOException e){}
        }
    /*
        System.out.println("closing...");
        try {
            socket.close();
        }catch (IOException e) {}
     */

    }
    /**
     * ������Ϣ
     * msg
     * 1= �ɹ������� 2=�׽��ֹر�
     */
    public int doMessage(Message msg)
	{
        //System.out.println("doMessage begin...type="+msg.type);
        switch(msg.type)
		{
            case 1:{//�µ����ӵ������
                sendNewPlayer(msg); //�ͻ��˱��뷵������type==10
                addPlayer(msg);     // msg.msg == ����ҵ�����
                break;
            }
            case 2:{// ������
                putChessman(msg);
                break;
            }
            case 3:{//������������Ϸ
                requestAnother(msg);
                break;
            }
            case 4:{
                denyRequest(msg);
                break;
            }
            case 5:{
                acceptRequest(msg);
                break;
            }
            case 6:{//ʤ������
                checkVictory(msg);
                break;
            }
            case 7:{//�Ͽ�����
                getdisconnect(msg);
                break;
            }
            case 8:{//������Ϸ
                break;
            }
            case 12:{//��Ϣ����
                boolean flag=true;
                setting(msg,flag);
                break;
            }
            case 13:{
                boolean flag=false;
                setting(msg,flag);
                break;
            }
            case 19:{
                playerRefresh(msg);
                break;
            }
            case 20:{
                try{
                    this.out.writeObject(msg);
                }catch(IOException e){
                    e.printStackTrace();
                }
                break;
            }
            default:{
            }
        }
        return 0; // ���
    }
    /**
     * �ж���Ϸ���������еĿͻ������
     * type = 7 �����߽�Ҫ�ر���Ϸ    
     */
    public void getdisconnect(Message msg)
	{
        Group gg = null;
        Player pp = null;
        String str=null;
        //����������ӵ�����һ����
        for(int i=0;i<Server.groupList.size();i++)
		{
            gg = (Group)Server.groupList.get(i);
            if(this.equals(gg.selfSocket)==true)
			{
               
                msg.type=6; // gg.player win
                try{
                    gg.playerSocket.out.writeObject(msg);
                }
				catch(IOException e)
				{
                    e.printStackTrace();
                }
                sendLeftMsg(gg.self);
                //�����б�
                Server.groupList.remove(gg);
                return;
            }
            if(this.equals(gg.playerSocket)==true)
			{
                msg.type=6;
                try{
                    gg.selfSocket.out.writeObject(msg);
                }catch(IOException e)
				{
                    e.printStackTrace();
                }
                sendLeftMsg(gg.player);
                Server.groupList.remove(gg);
                return;
            }
        }

        // ����Ͽ��������������Ƿ����б���
        for(int i=0;i<Server.playerList.size();i++)
		{
            pp = (Player)Server.playerList.get(i);
            if(this.equals(pp.selfSocket)==true)
			{
                break;
            }
        }
        sendLeftMsg(pp.self);
        Server.playerList.remove(pp); // ��ȥ�Ͽ���
        updateClient();
    }
    private void sendLeftMsg(String str)
	{
        char cc;
        for(int i=0;i<50;i++)
		{
            cc=str.charAt(i);
            if(cc!='\0')
                System.out.print(cc);
            else break;
        }
        System.out.print(" has left server ...\n");

    }
    /**
     * �ܾ��������
     * type ==4  msg == �ܾ��ߵ�����
     */
    public void denyRequest(Message msg)
	{
        String denyName=null;
        Player pp=null;
        for(int i=0;i<Server.playerList.size();i++)
		{
            pp = (Player)Server.playerList.get(i);
            if(this.equals(pp.selfSocket)==true)
			{
                denyName = new String(pp.self);
                break;
            }
        }
        for(int i=0;i<Server.playerList.size();i++)
		{
            pp = (Player)Server.playerList.get(i);
            if(arrayMatchString(msg.msg,pp.self)==true)
			{
                Message ms = new Message();
                ms.type=4;
                strToCharArray(denyName,ms.msg);
                try{// requestor 's socket send msg to it's client
                    pp.selfSocket.out.writeObject(ms);
                }
				catch(IOException er)
				{
                    er.printStackTrace();
                }
                break;
            }
        }

    }
    /**
     * B��������A ����� �����б�
     * type ==5 msg == A������
    */
    public void acceptRequest(Message msg)
	{
        Player pps=null,ppd=null;//ppd = B pps = A
        String acceptName=null;
        for(int i=0;i<Server.playerList.size();i++)
		{
            ppd = (Player)Server.playerList.get(i);
            if(this.equals(ppd.selfSocket)==true)
			{
                break;
            }
        }
        for(int i=0;i<Server.playerList.size();i++)
		{
            pps = (Player)Server.playerList.get(i);
            if(arrayMatchString(msg.msg,pps.self)==true)
			{
                break;
            }
        }

        Message ss = new Message();
        ss.type=14; // B���������ɫ
        ss.color=msg.color;
        try{
            ppd.selfSocket.out.writeObject(ss);
        }catch(IOException e)
		{
            e.printStackTrace();
        }
        ss.type=5; // B����A������
        strToCharArray(ppd.self,ss.msg);
        try{
            pps.selfSocket.out.writeObject(ss);
        }catch(IOException e)
		{
            e.printStackTrace();
        }
        //�ϴ��б���ʾ���������ʾ�����б�
        Group p1 = new Group();
        p1.self=new String(pps.self);
        p1.selfSocket = pps.selfSocket;
        p1.selfColor = pps.color;
        p1.player = new String(ppd.self);
        p1.playerSocket = ppd.selfSocket;
        if(p1.selfColor==1)
		{
            p1.playerColor = 2;
        }
		else
		{
            p1.playerColor = 1;
        }
        p1.Setting = pps.setting;
        Server.groupList.add(p1);

        ///System.out.println(p1.self+p1.selfColor+" player "+p1.player+p1.playerColor);

        if(Server.playerList.size()==2)
		{
            msg.type=15;
            try{
                pps.selfSocket.out.writeObject(msg);
                ppd.selfSocket.out.writeObject(msg);
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        Server.playerList.remove(pps);
        Server.playerList.remove(ppd);

        //System.out.println(" after create a group,playerlist size  = "+Server.playerList.size());

        updateClient();
    }
    /**
     * �����鴴��������뿪ʱ����CLIENT���б� 
     */
    public void updateClient()
	{
        Message msg = new Message();
        Player pp = null,ppm = null;
        for(int i=0;i<Server.playerList.size();i++)
		{
            pp = (Player)Server.playerList.get(i);
            msg.type=15;   //���¿ͻ�������б�
            try{  //����б�
   //             System.out.println("clear "+pp.self+"'s list box");
                pp.selfSocket.out.writeObject(msg);
            }catch(IOException e)
			{
                e.printStackTrace();
            }
            for(int j=0;j<Server.playerList.size();j++)
			{
                ppm=(Player)Server.playerList.get(j);
                strToCharArray(ppm.self,msg.msg);
                msg.type=9;
                try{
                    //System.out.println("updating ..."+pp.self+" list box about"+ppm.self);
                    pp.selfSocket.out.writeObject(msg);
                }
				catch(IOException e)
				{
                    e.printStackTrace();
                }
            }
        }
        //�Ժ�ͬһ����ҵ��б������
    }
    /**
     * �ж�arr[] �Ƿ���� str   
     */
    private boolean arrayMatchString(char []arr,String str)
	{
        for(int i=0; i<50 && str.charAt(i)!='\0';i++)
		{
            if(arr[i]!=str.charAt(i))
                return false;
        }
        return true;
    }
    /**
     * type ==3
     */
    public void requestAnother(Message msg)
	{
        Player pp = null;  // ������
        Player spp = null; // ������
        String senderName=null;
        // ��÷����ߵ�����
        for(int i=0;i<Server.playerList.size();i++)
		{
            spp = (Player)Server.playerList.get(i);
            if(this.equals(spp.selfSocket)==true)
			{
                senderName = new String(spp.self);
                //System.out.println("requestor 's name = "+senderName);
                break;
            }
        }
        for(int i=0;i<Server.playerList.size();i++)
		{
            pp = (Player)Server.playerList.get(i);
            if(arrayMatchString(msg.msg,pp.self)==true)
			{
                Message ms = new Message();
                strToCharArray(senderName,ms.msg);
                ms.type=3;
                if(spp.color==1)
				{
                    ms.color = 2; //set another's color
                }
                else
				{
                    ms.color = 1;
                }
                ms.setting=spp.setting;
                try{// ���B���׽��ַ��� msg ��B�Ŀͻ���
                    pp.selfSocket.out.writeObject(ms);
                    //System.out.println("type= "+ms.type+"  "+pp.self+ " send msg to name = "+ms.msg[0]+"with B's color"+ms.color);
                }catch(IOException er)
				{
                    er.printStackTrace();
                }
                break;
            }
        }
    }
    // ��'\0'���������ַ���ת��������
    public void strToCharArray(String str,char [] arr)
	{
        int i=0;
        for(i=0;i<str.length()&&i<49;i++)
		{
            arr[i] = str.charAt(i);
        }
        arr[i]='\0';
    }
    /**
     *������Ϣ��־
     * msg
     */
    public void setting(Message msg,boolean flag)
	{
        int i=0;
        Player pp=null;
        for(i=0;i<Server.playerList.size();i++)
		{
            pp =(Player) Server.playerList.get(i);
            if(this.equals(pp.selfSocket)==true)
			{
                if(flag==true)
                    pp.setting=msg.setting;
                else
                    pp.color=msg.color;
                //System.out.println("setting "+pp.setting+"color = "+pp.color);
            }
        }

    }

    /**
     * ������Ľ����˹��˺�����ռ�Ŀռ�
      */
    public String arrayToString(char [] arr)
	{
        int i=0,length=0;
        while(arr[i]!='\0' && i<50)
		{
            i++;
        }
        length=i;
        char [] ss = new char[length];
        for(i=0;i<length;i++)
		{
            ss[i]=arr[i];
        }
        String str = new String(ss);
        return str;
        //System.out.println("arraytoString "+str+"length = "+length);
    }

    /**
     * �������ҵ����е�CLIENT������б�
     * ��ȡ���������б����͵�ÿ��CLIENT��
     */
    public void sendNewPlayer(Message player)
	{
        Player pp=null;
        player.type=9;
//        System.out.println("send new Player ...");
        for(int i=0;i<Server.playerList.size();i++)
		{
            pp=(Player)Server.playerList.get(i);
            try{
                if(pp.self!=null){//������Ϣ�������ˣ������Լ�
                    //System.out.println(pp.self+" add list "+player.msg[0]+"i = "+i);
                    pp.selfSocket.out.writeObject(player);
                }
            }catch(IOException e)
			{
                e.printStackTrace();
            }
        }
    }
    /**
     * ��ҽ�����Ϸ���ȴ�
     */
    public void playerRefresh(Message player)
	{
        Player ppo = new Player();
        Player pp = null;
        ppo.color = player.color;
        ppo.self = new String(player.msg);
        ppo.selfSocket = this;
        Server.playerList.add(ppo);

        for(int i=0;i<Server.playerList.size();i++)
		{
            pp = (Player)Server.playerList.get(i);
            if(this.equals(pp.selfSocket)==false)
			{
                Message msg = new Message();
                strToCharArray(pp.self, msg.msg);
                msg.type = 9;
                msg.color = pp.color;
//                System.out.println("refresh " + pp.self + "serverlist size " +
  //                                 Server.playerList.size());
                try {
                    this.out.writeObject(msg);
                }
                catch (IOException e)
				{
                    e.printStackTrace();
                }
            }
        }
        Message ms = new Message();
        strToCharArray(ppo.self, ms.msg);
        ms.type=10;
        try{
            this.out.writeObject(ms);
        }
		catch(IOException e)
		{
            e.printStackTrace();
        }
        //Message ms = new Message();
        player.type=10;
        for(int i=0 ;i<Server.playerList.size();i++)
		{
            pp = (Player)Server.playerList.get(i);
            if(this.equals(pp.selfSocket)!=true)
			{
                try{
                    pp.selfSocket.out.writeObject(player);
                }
				catch(IOException e)
				{
                    e.printStackTrace();
                }

            }
        }

    }
    /**
     * �������ҵ����������б�
     */
    public void addPlayer(Message player)
	{
        int i=0;
        Player pp=null,tp=null;

        for(i=0;i<Server.playerList.size();i++)
		{
            pp=(Player)Server.playerList.get(i);
            if(this.equals(pp.selfSocket)==true)
			{
                //System.out.println("match socket ok and send to itself...");
                pp.self = new String(player.msg);
                try{
                    for (int j = 0; j < Server.playerList.size(); j++) 
					{
                        Message temp = new Message();
                        tp = (Player) Server.playerList.get(j);
                        if (tp.self != null) 
						{
                            strToCharArray(tp.self, temp.msg);
                            //temp.coordinateX=(byte)j;
                            temp.type = 10; //reply for type==1
                            //System.out.println("host "+pp.self+" add list to client name = "+temp.coordinateX+temp.msg[0]);
                            pp.selfSocket.out.writeObject(temp);
                        }
                    }
                   // out.writeObject(player);
                }
				catch(IOException e)
				{
                    e.printStackTrace();
                }
                break;
            }
        }/*
        System.out.print("welcome ");
        int k=0;
        while(true){
            if(player.msg[k]!='\0')
                System.out.print(player.msg[k++]);
            else break;
        }
        System.out.println();*/
        //System.out.println(" at "+pp.selfSocket.socket.toString());
    }
    public Socket getSocket()
	{
        return socket;
    }
    /**
     * ���MSG�������Ƿ��ʤ
     * type=6 msg = ��ʤ�ߵ�����
     */
    public void checkVictory(Message msg)
    {

    }
    /**
     * type = 2 ,(msg.coordinateX,msg.coordinateY).msg.color
     */
    public void putChessman(Message msg)
	{
        Group gg = new Group();
        ServeOneClient soc=null;
        String tName=null;
        int color=0;
        // �޸ķ�������
        for(int i=0;i<Server.groupList.size();i++)
		{
            gg = (Group)Server.groupList.get(i);
            if(this.equals(gg.selfSocket)==true)
			{
                soc = gg.playerSocket;
                tName = new String(gg.player);
                color = gg.selfColor;
                break;
            }
            if(this.equals(gg.playerSocket)==true)
			{
                soc = gg.selfSocket;
                tName = new String(gg.self);
                color = gg.playerColor;
                break;
            }
        }
        gg.board[msg.coordinateX][msg.coordinateY]=color;

        // �ж��Ƿ����˻�ʤ
        if(judge(gg,msg.coordinateX,msg.coordinateY)==true)
		{//һ����ʤ
            // ����˫�������б����ƿ�
            try{
                msg.type=6;  // ʤ��
                this.out.writeObject(msg);
                msg.type=17; // ʧ��
                soc.out.writeObject(msg); 
//                System.out.println("send failed to "+tName);
            }
			catch(IOException e)
			{
                e.printStackTrace();
            }
            Server.groupList.remove(gg); //���б����ƿ�
            return;
        }
        //  ����msg��������� 
        try{
            //System.out.println("server put chess man "+msg.coordinateX+","+msg.coordinateY);
            soc.out.writeObject(msg);
        }
		catch(IOException e)
		{
            e.printStackTrace();
        }

    }
    /**
     * �ж��Ƿ����˻�ʤ
    */
    private boolean judge(Group gg,int x,int y)
	{
        int i = 0, j = 0, count = 0;
        int color=gg.board[x][y];
        // x ����
        for (i = 0, count = 0; x - i >= 0 && i < 5; i++) 
		{
            if (color == gg.board[x - i][y]) 
			{
                count++;
            }
            else {
                break;
            }
//          System.out.println("( "+x+" , "+y+" )"+"count = "+count);
            if (count == 5)
                return true;
        }
        for (i = 1; x + i < 15 && i < 5; i++) 
		{
            if (color == gg.board[x + i][y]) 
			{
                count++;
            }
            else {
                break;
            }
            if (count == 5)
                return true;
        }
        // y ����
        for (i = 0, count = 0; y - i >= 0 && i < 5; i++) 
		{
            if (color == gg.board[x][y - i])
			{
                count++;
            }
            else {
                break;
            }
//            System.out.println("( "+x+" , "+y+" )"+"count = "+count);
            if (count == 5)
                return true;
        }
        for (i = 1; y + i < 15 && i < 5; i++) 
		{
            if (color == gg.board[x][y + i]) 
			{
                count++;
            }
            else {
                break;
            }
//        System.out.println("( "+x+" , "+y+" )"+"count = "+count);
            if (count == 5)
                return true;
        }
        // '\' ����
        for (i = 0, count = 0; x - i >= 0 && y - i >= 0 && i < 5; i++) 
		{
            if (color == gg.board[x - i][y - i])
			{
                count++;
            }
            else {
                break;
            }
//            System.out.println("( "+x+" , "+y+" )"+"count = "+count);
            if (count == 5)
                return true;
        }
        for (i = 1; x + i < 15 && y + i < 15 && i < 5; i++) 
		{
            if (color == gg.board[x + i][y + i]) 
			{
                count++;
            }
            else {
                break;
            }
//          System.out.println("( "+x+" , "+y+" )"+"count = "+count);
            if (count == 5) {
                return true;
            }
        }
        // '/' ����
        for (i = 0, count = 0; x + i < 15 && y - i >= 0 && i < 5; i++) 
		{
            if (color == gg.board[x + i][y - i]) 
			{
                count++;
            }
            else {
                count = 0;
            }
//          System.out.println("( "+x+" , "+y+" )"+"count = "+count);
            if (count == 5)
                return true;
        }
        for (i = 1; x - i >= 0 && y + i < 15 && i < 5; i++) 
		{
            if (color == gg.board[x - i][y + i]) 
			{
                count++;
            }
            else {
                break;
            }
//            System.out.println("( "+x+" , "+y+" )"+"count = "+count);
            if (count == 5) 
			{
                return true;
            }
        }
        return false;
    }
   
} 
