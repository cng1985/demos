package com.ada.diff;

import de.danielbechler.diff.ObjectDifferBuilder;
import de.danielbechler.diff.node.DiffNode;
import de.danielbechler.diff.node.Visit;

public class App {

    public static void main(String[] args) {


        User u1=new User();
        u1.setName("ada");
        u1.setAge(1);
        User u2=new User();
        u2.setName("young");
        u2.setAge(18);
        DiffNode diffNode= ObjectDifferBuilder.buildDefault().compare(u1,u2);
        System.out.println(diffNode.isChanged());
        diffNode.visit(new DiffNode.Visitor() {
            @Override
            public void node(DiffNode diffNode, Visit visit) {
                System.out.println(diffNode.getPath()+">>" +diffNode.getState());
            }
        });
        System.out.println("****************");
        StringBuilder builder=new StringBuilder();
        diffNode.visit(new DiffNode.Visitor() {
            @Override
            public void node(DiffNode diffNode, Visit visit) {
                if (!diffNode.isRootNode()){
                    builder.append(diffNode.getPath());
                    builder.append("从");
                    builder.append(diffNode.canonicalGet(u1)+"变成" +diffNode.canonicalGet(u2));
                    builder.append("\n");
                }
            }
        });
        System.out.println(builder.toString());
        System.out.println("****************");

    }
}
