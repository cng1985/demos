package com.ada.diff;

import org.javers.core.Javers;
import org.javers.core.JaversBuilder;
import org.javers.core.diff.Diff;

/**
 * @author ada
 * @date 2022/4/1
 */
public class App2 {
    public static void main(String[] args) {

        User u1=new User();
        u1.setName("ada");
        u1.setAge(1);
        User u2=new User();
        u2.setName("young");
        u2.setAge(18);
        Javers javers = JaversBuilder.javers().build();
        Diff diff = javers.compare(u1, u2);
        System.out.println(diff.hasChanges());
        System.out.println(diff);
    }
}
