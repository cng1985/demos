package com.ada;

import com.ada.classloader.DiskClassLoader;
import com.ada.classloader.MyURLClassLoader;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.URL;


/**
 * @author ada
 * @date 2022/4/18
 */
public class DemoApp {

    public static void main(String[] args) throws ClassNotFoundException, InvocationTargetException, InstantiationException, IllegalAccessException, MalformedURLException {

        ClassLoader loader = Thread.currentThread().getContextClassLoader();

        DiskClassLoader classLoader = new DiskClassLoader("E:\\code\\maven\\demos\\mybatis\\target\\classes\\com\\nbsaas\\user\\data\\entity");
        Class c = classLoader.loadClass("com.nbsaas.user.data.entity.Area");
        Constructor<?>[] cs = c.getConstructors();
        System.out.printf(String.valueOf(cs.length));
        Object obj = cs[0].newInstance();
        System.out.println(obj);

        File file = new File("E:\\m2\\com\\nbsaas\\discovery\\code-make\\1.0.14\\code-make-1.0.14.jar");
        URL[] urls = new URL[1];
        urls[0] = file.toURI().toURL();
        MyURLClassLoader myURLClassLoader = new MyURLClassLoader(urls);
        Class c1 = myURLClassLoader.loadClass("com.nbsaas.codemake.App");
        System.out.println(c1);
        Object a = c1.newInstance();
    }
}
