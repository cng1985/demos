package com.ada.apps;

import java.util.HashMap;
import java.util.Map;

public class CalApp {

  private static Map<Integer, String> tiangan = new HashMap<>();
  private static Map<Integer, String> dizhi = new HashMap<>();
  private static Map<Integer, String> zhengxiao = new HashMap<>();

  static {
    tiangan.put(4,"甲");
    tiangan.put(5,"乙");
    tiangan.put(6,"丙");
    tiangan.put(7,"丁");
    tiangan.put(8,"戊");
    tiangan.put(9,"己");
    tiangan.put(0,"庚");
    tiangan.put(1,"辛");
    tiangan.put(2,"壬");
    tiangan.put(3,"癸");
    dizhi.put(4,"子");
    dizhi.put(5,"丑");
    dizhi.put(6,"寅");
    dizhi.put(7,"卯");
    dizhi.put(8,"辰");
    dizhi.put(9,"巳");
    dizhi.put(10,"午");
    dizhi.put(11,"未");
    dizhi.put(12,"申");
    dizhi.put(1,"酉");
    dizhi.put(2,"戌");
    dizhi.put(3,"亥");


    zhengxiao.put(4,"鼠");
    zhengxiao.put(5,"牛");
    zhengxiao.put(6,"虎");
    zhengxiao.put(7,"兔");
    zhengxiao.put(8,"龙");
    zhengxiao.put(9,"蛇");
    zhengxiao.put(10,"马");
    zhengxiao.put(11,"羊");
    zhengxiao.put(12,"猴");
    zhengxiao.put(1,"鸡");
    zhengxiao.put(2,"狗");
    zhengxiao.put(3,"猪");
  }

  public static void main(String[] args) {
    for (int i = 1999; i <= 2025; i++) {
      Integer tg=i%10;
      Integer dz=i%12;
      if (dz==0){
        dz=12;
      }
      System.out.println(i+"="+tiangan.get(tg)+""+dizhi.get(dz)+"="+zhengxiao.get(dz));
    }
  }

}
