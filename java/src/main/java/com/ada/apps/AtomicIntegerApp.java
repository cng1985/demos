package com.ada.apps;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.LongAdder;

/**
 * Created by ada on 2017/6/1.
 */
public class AtomicIntegerApp {
    public static void main(String[] args) {


        AtomicInteger adder=new AtomicInteger();
        for (int i = 0; i < 100; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                        for (int j = 0; j < 10; j++) {
                            adder.incrementAndGet();
                            System.out.println(adder.get());
                            System.out.println(Thread.currentThread().getName());
                        }

                }
            }).start();
        }
    }
}
