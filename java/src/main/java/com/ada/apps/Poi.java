package com.ada.apps;

import lombok.Data;

@Data
public class Poi {

    private String lat;

    private String lng;
}
