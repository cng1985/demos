package com.ada.apps;

import java.util.concurrent.atomic.LongAdder;

/**
 * Created by ada on 2017/6/1.
 */
public class LongAdderApp {
    public static void main(String[] args) {


        LongAdder adder=new LongAdder();
        for (int i = 0; i < 100; i++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                        for (int j = 0; j < 10; j++) {
                            adder.add(1l);
                            System.out.println(adder.sum());
                            System.out.println(Thread.currentThread().getName());

                        }

                }
            }).start();
        }
    }
}
