package com.ada.apps;

public class StrApp {

    public static void main(String[] args) {
        char c = 'A' ;
        int num = 10 ;
        switch(c) {
            case 'B' :
                num ++ ;
            case 'A' :
                num ++ ;
            case 'Y' :
                num ++ ;
            default :
                num -- ;
        }
        System.out.println(num) ;
    }
}
