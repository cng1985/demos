package com.ada.rabbitmq;

import com.rabbitmq.client.*;

public class RabbitProducer {

    private final static String QUEUE_NAME = "hello";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost("192.168.3.108");
        factory.setUsername("ada");
        factory.setPassword("123456");
        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {
            channel.queueDeclare(QUEUE_NAME, false, false, false, null);

            for (int i = 0; i < 100000; i++) {
                String message = "Hello World!"+i;
                channel.basicPublish("", QUEUE_NAME, null, message.getBytes("UTF-8"));
                //System.out.println(" [x] Sent '" + message + "'");
               // Thread.sleep(100);
            }

        }
    }
}