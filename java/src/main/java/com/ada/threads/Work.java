package com.ada.threads;

public interface Work {

    <T> T work();
}
