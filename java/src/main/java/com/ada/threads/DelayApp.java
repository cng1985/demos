package com.ada.threads;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class DelayApp {

    public static void main(String[] args) {

        ScheduledExecutorService service= Executors.newScheduledThreadPool(10);
        service.schedule(()->{
            System.out.println("hello");
        },10, TimeUnit.SECONDS);

    }
}
