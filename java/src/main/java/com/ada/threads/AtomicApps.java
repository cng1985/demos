package com.ada.threads;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

public class AtomicApps {
    public static void main(String[] args) {
        AtomicLong num=new AtomicLong();
        num.getAndAdd(-1L);
        System.out.println(num.get());
        for (int i = 0; i < 1000; i++) {
            Executors.newCachedThreadPool().execute(new Runnable() {
                @Override
                public void run() {
                   long t= num.incrementAndGet();
                    System.out.println(t);
                }
            });

        }
    }
}
