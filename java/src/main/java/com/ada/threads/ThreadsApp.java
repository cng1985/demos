package com.ada.threads;

import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadsApp {

    public static void main(String[] args) {
        ThreadPoolExecutor executor=    new ThreadPoolExecutor(0, 60,
                60L, TimeUnit.SECONDS,
                new SynchronousQueue<Runnable>());

        for (int i = 0; i < 1000; i++) {
            if (executor.getActiveCount()>50){
                continue;
            }
            executor.execute(()->{
                try {
                    Thread.sleep(1000);
                    System.out.println( executor.getActiveCount());
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            });

        }

        System.out.println( executor.getPoolSize());
        System.out.println( executor.getActiveCount());

    }
}
