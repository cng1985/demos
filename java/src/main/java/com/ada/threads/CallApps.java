package com.ada.threads;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicLong;

public class CallApps {
  public static void main(String[] args) {



    ExecutorService executorService = Executors.newFixedThreadPool(20);
    List<Future<String>> futures=new ArrayList<>();
    for (int i = 0; i < 100; i++) {
      PrintCallBack callBack = new PrintCallBack("" + i);
      Future<String> future = executorService.submit(callBack);
      futures.add(future);
    }
    for (Future<String> future : futures) {
      try {
        System.out.println(""+future.get());
      } catch (InterruptedException e) {
        e.printStackTrace();
      } catch (ExecutionException e) {
        e.printStackTrace();
      }
    }
    executorService.shutdown();
  }
}
