package com.ada.threads;

import java.util.concurrent.Callable;

public class PrintCallBack implements Callable<String> {

  public PrintCallBack(String no) {
    this.no = no;
  }

  private String no;

  @Override
  public String call() throws Exception {
    Thread.sleep(1000);
    return no;
  }
}
