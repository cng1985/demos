package com.ada.zip;

import net.lingala.zip4j.ZipFile;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.model.enums.EncryptionMethod;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ZipCryptoEncryptionExample {
    public static void main(String[] args) throws IOException {

        ZipParameters zipParameters = new ZipParameters();
        zipParameters.setEncryptFiles(true);
        zipParameters.setEncryptionMethod(EncryptionMethod.ZIP_STANDARD);

        List<File> filesToAdd = Arrays.asList(
                new File("E:\\data\\mapproxy.yaml"),
                new File("E:\\aa.xlsx")
        );

        ZipFile zipFile = new ZipFile("E:\\data\\filename4.zip", "password".toCharArray());
        zipFile.createSplitZipFile(filesToAdd, zipParameters, true, 10485760); // u
    }


}
