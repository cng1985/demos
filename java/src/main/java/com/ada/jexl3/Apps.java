package com.ada.jexl3;

import org.apache.commons.jexl3.JexlBuilder;
import org.apache.commons.jexl3.JexlContext;
import org.apache.commons.jexl3.JexlEngine;
import org.apache.commons.jexl3.JexlExpression;
import org.apache.commons.jexl3.JxltEngine;
import org.apache.commons.jexl3.MapContext;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Apps {
  public static void main(String[] args) {
    JexlEngine jexl = new JexlBuilder().create();

    String calculateTax ="((G1 + G2 + G3) * 0.1) + G4/3"; //e.g. "((G1 + G2 + G3) * 0.1) + G4";
    JexlExpression e = jexl.createExpression( calculateTax );
  
    // populate the context
    JexlContext context = new MapContext();
    context.set("G1",1);
    context.set("G2", 2);
    context.set("G3", 3);
    context.set("G4", 5.000000015689);
    // ...

  
    // work it out
    Number result = (Number) e.evaluate(context);
    System.out.println(result);
  }
}
