package com.ada.jexl3;

import org.apache.commons.jexl3.*;

import java.util.HashMap;
import java.util.Map;

public class DemoApp {

    public static void main(String[] args) {

        JexlEngine jexl = new JexlBuilder().create();

        String calculateTax ="a*b"; //e.g. "((G1 + G2 + G3) * 0.1) + G4";
        JexlExpression e = jexl.createExpression( calculateTax );

        Map<String,Object> map=new HashMap<>();
        map.put("a",1.15612);
        map.put("b",2);


        // populate the context
        JexlContext context = new MapContext(map);


        // work it out
        Number result = (Number) e.evaluate(context);
        System.out.println(result);
    }
}
