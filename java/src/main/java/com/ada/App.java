package com.ada;

import com.ada.eventdispatcher.Event;
import com.ada.eventdispatcher.EventDispatcher;
import com.ada.eventdispatcher.EventListener;
import com.ada.eventdispatcher.impl.EventDispatcherImpl;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.util.StringUtils;

/**
 * Hello world!
 */
public class App {
    public static void main(java.lang.String[] args) {
        String str = "1231232";


        String money = "3502236.74";
        if (StringUtils.hasText(money)) {
            //money = money.replace(".", "");
            int index = money.indexOf(".");
            int lastIndex = money.lastIndexOf(".");
            if (index!=lastIndex){
                String lastStr="";
                if (lastIndex > 0) {
                    lastStr=money.substring(lastIndex);
                    money = money.substring(0, lastIndex);
                }
                money = money.replace(".", "")+lastStr;
            }
        }
        System.out.println(money);

        System.out.println("Hello World!");
        String name = "";
        EventDispatcher dispatcher = new EventDispatcherImpl();

        dispatcher.addEventListener(new EventListener() {
            @Override
            public void onEvent(Event event) {
                System.out.println(event.getType());
            }

            @Override
            public boolean isFailOnException() {
                return false;
            }
        });
        dispatcher.dispatchEvent(new Event() {
            @Override
            public String getType() {
                return "s";
            }

        });

        String url = DigestUtils.sha1Hex("x");
        System.out.println(url);

        Float f = 1f;
        Double d = 1.0;
        long x = 1;
        int xx = 3;
        int $double;
    }
}
