package com.ada.descartes;


import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) {



        List<String> p1=new ArrayList<>();
        p1.add("红色");
        p1.add("黄色");
        p1.add("蓝色");

        List<String> p2=new ArrayList<>();
        p2.add("35");
        p2.add("36");
        p2.add("37");
        p2.add("38");
        p2.add("39");
        p2.add("40");
        p2.add("41");
        p2.add("42");
        p2.add("43");

        List<String> p3=new ArrayList<>();
        p3.add("中国");
        p3.add("美国");
        p3.add("日本");
        p3.add("德国");

        List<String> p4=new ArrayList<>();
        p4.add("A");
        p4.add("B");
        p4.add("C");
        p4.add("D");
        p4.add("F");
        p4.add("G");
        p4.add("H");
        p4.add("I");
        p4.add("J");

        List<String> result = descartes(p1, p2,p3,p4);

        for (String s : result) {
            System.out.println(s);
        }

        System.out.println(result.size());
        System.out.println(p1.size()*p2.size()*p3.size()*p4.size());

    }

    private static List<String> getStrings(List<String> p1, List<String> p2) {
        List<String> result=new ArrayList<>();
        for (String s : p1) {
            for (String s1 : p2) {
                String temp=s+"-"+s1;
                result.add(temp);
            }
        }
        return result;
    }
    private static List<String> descartes (List<String> ... lists) {
        if (lists.length<2){
            return new ArrayList<>();
        }
        List<String> last=getStrings(lists[0],lists[1]);;

        for (int i = 2; i < lists.length; i++) {
            last=getStrings(last,lists[i]);
        }
        return last;
    }
}
