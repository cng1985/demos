package com.ada.ffmpeg;

import java.io.IOException;

public class ExtractFrameExample {
    public static void main(String[] args) {
        String inputPath = "D:\\aa.mp4";
        int frameIndex = 100; // 要提取的帧的索引
        String outputPath = "D:\\mp4ame.jpg";

        ProcessBuilder processBuilder = new ProcessBuilder(
                "E:\\tools\\ffmpeg\\bin\\ffmpeg", "-i", inputPath, "-vf", "select=eq(n\\" + "," + frameIndex + ")", "-q:v", "3", "-an", "-vsync", "0", outputPath
        );

        try {
            Process process = processBuilder.start();
            process.waitFor();
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
