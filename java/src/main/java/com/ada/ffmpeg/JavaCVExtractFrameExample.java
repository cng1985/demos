package com.ada.ffmpeg;

import org.bytedeco.javacv.*;
import org.bytedeco.opencv.opencv_core.*;
import static org.bytedeco.opencv.global.opencv_imgcodecs.*;

public class JavaCVExtractFrameExample {
    public static void main(String[] args) {
        String ffmpegPath = "E:\\tools\\ffmpeg\\bin";
        String inputPath = "D:\\aa.mp4";
        int frameIndex = 100; // 要提取的帧的索引
        String outputPath = "E:\\1frame.jpg";

        try (FFmpegFrameGrabber frameGrabber = new FFmpegFrameGrabber(inputPath)) {
            frameGrabber.start();

            for (int i = 0; i < frameIndex; i++) {
                frameGrabber.grab();
            }

            Frame frame = frameGrabber.grab();
            Mat mat = new OpenCVFrameConverter.ToMat().convert(frame);
            imwrite(outputPath, mat);

            frameGrabber.stop();
        } catch (FrameGrabber.Exception e) {
            e.printStackTrace();
        }
    }
}
