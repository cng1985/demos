package com.ada.ffmpeg;

import com.github.kokorin.jaffree.ffmpeg.FFmpeg;
import com.github.kokorin.jaffree.ffmpeg.UrlInput;
import com.github.kokorin.jaffree.ffmpeg.UrlOutput;

import java.nio.file.Paths;

public class JaffreeExample {
    public static void main(String[] args) {
        String ffmpegPath = "E:\\tools\\ffmpeg\\bin";
        String inputPath = "D:\\aa.mp4";
        String outputPath = "D:\\aa.avi";

        FFmpeg.atPath(Paths.get(ffmpegPath))
            .addInput(UrlInput.fromPath(Paths.get(inputPath)))
            .addOutput(UrlOutput.toPath(Paths.get(outputPath)))
            .execute();
    }
}
