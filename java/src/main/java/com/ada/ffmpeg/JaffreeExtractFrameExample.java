package com.ada.ffmpeg;

import com.github.kokorin.jaffree.ffmpeg.FFmpeg;
import com.github.kokorin.jaffree.ffmpeg.Filter;
import com.github.kokorin.jaffree.ffmpeg.UrlInput;
import com.github.kokorin.jaffree.ffmpeg.UrlOutput;

import java.nio.file.Paths;

public class JaffreeExtractFrameExample {
    public static void main(String[] args) {
        String ffmpegPath = "E:\\tools\\ffmpeg\\bin";
        String inputPath = "D:\\aa.mp4";
        int frameIndex = 100; // 要提取的帧的索引
        String outputPath = "D:\\frame11.jpg";

        FFmpeg.atPath(Paths.get(ffmpegPath))
                .addInput(UrlInput.fromPath(Paths.get(inputPath)))
                .addOutput(UrlOutput.toPath(Paths.get(outputPath)))
                .setOverwriteOutput(true)
                .execute();
    }
}
