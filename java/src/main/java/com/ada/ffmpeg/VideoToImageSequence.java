package com.ada.ffmpeg;

import com.github.kokorin.jaffree.ffmpeg.*;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import java.nio.file.Path;
import java.nio.file.Paths;

public class VideoToImageSequence {
    public static void main(String[] args) {
        String inputPath = "D:\\aa.mp4";
        String outputDirectory = "D:\\directory";

        extractFrames(inputPath, outputDirectory);
    }

    private static void extractFrames(String inputPath, String outputDirectory) {
        Path ffmpegBinaryPath = Paths.get("E:\\tools\\ffmpeg\\bin"); // Set the path to your FFmpeg executable
        Path ffprobeBinaryPath = Paths.get("E:\\tools\\ffmpeg\\bin\\ffprobe"); // Set the path to your FFprobe executable
        Path inputPathObj = Paths.get(inputPath);
        Path outputDirectoryObj = Paths.get(outputDirectory);

        FFmpeg.atPath(ffmpegBinaryPath)
            .addInput(UrlInput.fromPath(Paths.get(inputPath)))
            .addOutput(
                    UrlOutput.toPath(outputDirectoryObj.resolve("frame_%05d.jpg"))
            )
            .execute();
    }
}
