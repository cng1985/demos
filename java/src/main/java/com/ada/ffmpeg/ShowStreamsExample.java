package com.ada.ffmpeg;

import com.github.kokorin.jaffree.ffprobe.FFprobe;
import com.github.kokorin.jaffree.ffprobe.FFprobeResult;
import com.github.kokorin.jaffree.ffprobe.Stream;

import java.nio.file.Paths;

public class ShowStreamsExample {
    public static void main(String[] args) {
        if (args.length != 1) {
            System.err.println("Exactly 1 argument expected: path to media file");
            System.exit(1);
        }

        String pathToVideo = "D:\\aa.mp4";;
        String ffmpegPath = "E:\\tools\\ffmpeg\\bin";

        FFprobeResult result = FFprobe.atPath(Paths.get(ffmpegPath))
                .setShowStreams(true)
                .setInput(Paths.get(pathToVideo))
                .execute();

        for (Stream stream : result.getStreams()) {
            System.out.println("Stream #" + stream.getIndex()
                    + " type: " + stream.getCodecType()
                    + " duration: " + stream.getDuration() + " seconds");
        }

    }
}