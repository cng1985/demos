package com.ada.mqtt;

import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

public class App1 {

    public static void main(String[] args) throws MqttException {
        String broker = "tcp://114.55.36.149:1883";
        String clientId = "a11"; // 客户端唯一标识

        MqttClient client = new MqttClient(broker, clientId, new MemoryPersistence());


        MqttConnectOptions connOpts = new MqttConnectOptions();
        connOpts.setCleanSession(true); // 清除会话
        connOpts.setUserName("test"); // 用户名
        connOpts.setPassword("123456".toCharArray()); // 密码

        client.connect(connOpts);



        //client.close();
        String topic1 = "test1";
        client.subscribe(topic1, (s, mqttMessage) -> {
            System.out.println(s);
            System.out.println(mqttMessage.getId());
            System.out.println(new String(mqttMessage.getPayload()));

        });

        for (int j = 0; j < 20; j++) {
            new Thread(()->{
                for (int i = 0; i < 10000000; i++) {
                    String topic = "test";
                    String message = "Hello, MQTT!";
                    try {
                        client.publish(topic, message.getBytes(), 0, false);
                    } catch (MqttException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }



    }
}
