package com.ada.chatgpt;


import com.plexpt.chatgpt.ChatGPTStream;
import com.plexpt.chatgpt.entity.chat.ChatCompletion;
import com.plexpt.chatgpt.entity.chat.Message;
import com.plexpt.chatgpt.listener.ConsoleStreamListener;
import com.plexpt.chatgpt.util.Proxys;

import java.net.Proxy;
import java.util.Arrays;

public class StreamApp {

    public static void main(String[] args) {

        Proxy proxy = Proxys.http("127.0.0.1", 7890);

        ChatGPTStream chatGPTStream = ChatGPTStream.builder()
                .timeout(600)
                .apiKey("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")
                .proxy(proxy)
                .apiHost("https://api.openai.com/")
                .build()
                .init();


        ConsoleStreamListener listener = new ConsoleStreamListener();
        Message message = Message.of("写一段七言绝句诗，题目是：火锅！");
        ChatCompletion chatCompletion = ChatCompletion.builder()
                .messages(Arrays.asList(message))
                .build();
        chatGPTStream.streamChatCompletion(chatCompletion, listener);
    }
}
