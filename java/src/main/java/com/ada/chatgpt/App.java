package com.ada.chatgpt;

import com.plexpt.chatgpt.ChatGPT;
import com.plexpt.chatgpt.entity.chat.ChatCompletion;
import com.plexpt.chatgpt.entity.chat.ChatCompletionResponse;
import com.plexpt.chatgpt.entity.chat.Message;
import com.plexpt.chatgpt.util.Proxys;

import java.net.Proxy;
import java.util.Arrays;

public class App {

    public static void main(String[] args) {
        //国内需要代理 国外不需要
        Proxy proxy = Proxys.http("127.0.0.1", 7890);

        //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
        //"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
        ChatGPT chatGPT = ChatGPT.builder()
                .apiKey("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")
                .proxy(proxy)
                .timeout(900)
                .apiHost("https://api.openai.com/") //反向代理地址
                .build()
                .init();

        Message system = Message.ofSystem("你现在是一个陪聊人员，解决客户遇到的问题");
        Message message = Message.of("怎么能挣钱");

        ChatCompletion chatCompletion = ChatCompletion.builder()
                .model(ChatCompletion.Model.GPT_3_5_TURBO.getName())
                .messages(Arrays.asList(system, message))
                .maxTokens(3000)
                .temperature(0.9)
                .build();
        ChatCompletionResponse response = chatGPT.chatCompletion(chatCompletion);
        Message res = response.getChoices().get(0).getMessage();
        System.out.println(res);



    }
}
