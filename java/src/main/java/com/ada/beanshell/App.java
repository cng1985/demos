package com.ada.beanshell;

import bsh.EvalError;
import bsh.Interpreter;

import java.util.Date;

public class App {

    public static void main(String[] args) throws EvalError {
        Interpreter i = new Interpreter();  // Construct an interpreter

        i.set("foo", 5);                    // Set variables
        i.set("date", new Date());

        Date date = (Date) i.get("date");    // retrieve a variable
        System.out.println(date);

        StringBuffer buffer = new StringBuffer();
        buffer.append("bar = foo/3.0; \n");
        buffer.append("demo=new com.ada.beanshell.Demo(); \n");
        buffer.append("demo.hi(); \n");
        buffer.append("demo.say(5); \n");

        buffer.append("for (int i=0; i<5; i++) \n");
        buffer.append("print(i) \n");


// Eval a statement and get the result
        i.eval(buffer.toString());
        System.out.println(i.get("bar"));
        System.out.println(i.get("demo"));
        Object o = i.get("demo");
        if (o instanceof Demo) {
            Demo demo = (Demo) o;
            demo.hi();
        }

    }
}
