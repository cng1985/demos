package com.ada.meilisearch;

import com.alibaba.fastjson2.JSON;
import com.meilisearch.sdk.Client;
import com.meilisearch.sdk.Config;
import com.meilisearch.sdk.Index;
import com.meilisearch.sdk.exceptions.MeilisearchException;
import com.meilisearch.sdk.model.Results;
import com.meilisearch.sdk.model.SearchResult;
import com.meilisearch.sdk.model.TaskInfo;
import jodd.io.FileUtil;

import java.io.File;
import java.io.IOException;

public class App2 {

    public static void main(String[] args) throws MeilisearchException, IOException {


        Client client = new Client(new Config("http://localhost:7700", "coMllgOZN2Kk5hK0vtbf6sCn8GtqFojjrcDKeR0zN14"));
        Results<Index> indexs = client.getIndexes();
        Index[] list = indexs.getResults();
        for (Index index : list) {
            System.out.println(index);
        }
        TaskInfo state = client.createIndex("movies");
        System.out.println(state.getIndexUid());

        Index index = client.index("movies");

        TaskInfo temp = index.addDocuments(JSON.toJSONString(User.builder().name("ada").build()), "1");
        System.out.println(temp.getStatus());

        index.addDocuments(JSON.toJSONString(User.builder().name("botman").build()),"2");
        SearchResult res = client.index("movies").search("botman");
        System.out.println(res);
    }
}
