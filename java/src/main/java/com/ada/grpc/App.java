package com.ada.grpc;

import com.google.protobuf.InvalidProtocolBufferException;

public class App {
    public static void main(String[] args) {

        Helloworld.HelloRequest request= Helloworld.HelloRequest.newBuilder().setName("Ada").build();

        byte[] serializedPerson = request.toByteArray();

        // 反序列化
        try {
            Helloworld.HelloRequest parsedPerson = Helloworld.HelloRequest.parseFrom(serializedPerson);
            System.out.println(parsedPerson.toString());
        } catch (InvalidProtocolBufferException e) {
            e.printStackTrace();
        }
    }
}
