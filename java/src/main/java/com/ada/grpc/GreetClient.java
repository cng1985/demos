package com.ada.grpc;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

public class GreetClient {
    public static void main(String[] args) {


        Long time=System.currentTimeMillis();
        for (int i = 0; i < 2000; i++) {
            ManagedChannel channel = ManagedChannelBuilder.forAddress("localhost", 50051)
                    .usePlaintext()
                    .build();

//            GreetServiceGrpc.GreetServiceBlockingStub stub = GreetServiceGrpc.newBlockingStub(channel);
//
//            Greet.GreetRequest request = Greet.GreetRequest.newBuilder().setName("John").build();
//            Greet.GreetResponse response = stub.greet(request);

            //System.out.println("Response from server: " + response.getGreeting()+" message= "+response.getMessage()+" code= "+response.getCode());
            channel.shutdown();

        }
        time=System.currentTimeMillis()-time;
        System.out.println(time);
    }
}
