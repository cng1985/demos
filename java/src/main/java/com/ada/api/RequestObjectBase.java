package com.ada.api;

import java.util.UUID;

/**
 * Created by ada on 2017/5/16.
 */
public class RequestObjectBase implements RequestObject {

    private String salt;

    private Long time;

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    @Override
    public long time() {
        return time;
    }

    @Override
    public String salt() {
        return salt;
    }
}
