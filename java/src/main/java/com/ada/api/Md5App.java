package com.ada.api;

import com.ada.api.impl.AppRequestObject;
import com.ada.api.impl.AppSign;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by ada on 2017/5/16.
 */
public class Md5App {
    //静态方法，便于作为工具类
    public static String getMd5(String plainText) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(plainText.getBytes());
            byte b[] = md.digest();

            int i;

            StringBuffer buf = new StringBuffer("");
            for (int offset = 0; offset < b.length; offset++) {
                i = b[offset];
                if (i < 0)
                    i += 256;
                if (i < 16)
                    buf.append("0");
                buf.append(Integer.toHexString(i));
            }
            //32位加密
            return buf.toString();
            // 16位的加密
            //return buf.toString().substring(8, 24);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            return null;
        }

    }

    public static void main(String[] args) {
        SignManager manager=new SignManager();
        manager.put(AppRequestObject.class,new AppSign());

        Signable sign=  manager.get(AppRequestObject.class);
        AppRequestObject requestObject=new AppRequestObject();
        requestObject.setAppKey("dfsdf");
        requestObject.setAppSecretKey("dsfsf");
        System.out.println(sign.sing(requestObject));
        System.out.println(getMd5(sign.sing(requestObject)));
    }
}
