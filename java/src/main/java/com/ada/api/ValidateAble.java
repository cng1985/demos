package com.ada.api;

/**
 * Created by ada on 2017/5/16.
 */
public interface ValidateAble {

    boolean validate(RequestObject object);
}
