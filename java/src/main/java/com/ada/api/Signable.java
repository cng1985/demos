package com.ada.api;

/**
 * Created by ada on 2017/5/16.
 */
public interface Signable {
    String sing(RequestObject object);
}
