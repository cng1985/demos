package com.ada.api.impl;

import com.ada.api.RequestObject;
import com.ada.api.Signable;

/**
 * Created by ada on 2017/5/16.
 */
public class AppSign implements Signable {
    @Override
    public String sing(RequestObject object) {
        StringBuffer result=new StringBuffer();
        if (object instanceof AppRequestObject){
            AppRequestObject appRequestObject=(AppRequestObject)object;
            result.append(appRequestObject.getAppKey());
            result.append("-");
            result.append(appRequestObject.getAppSecretKey());
        }
        result.append("x");
        result.append(object.time());
        result.append("y");
        result.append(object.salt());
        return result.toString();
    }
}
