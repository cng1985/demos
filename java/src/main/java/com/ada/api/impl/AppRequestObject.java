package com.ada.api.impl;

import com.ada.api.RequestObjectBase;

/**
 * Created by ada on 2017/5/16.
 */
public class AppRequestObject extends RequestObjectBase {

    private String appKey;

    private String appSecretKey;

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    public String getAppSecretKey() {
        return appSecretKey;
    }

    public void setAppSecretKey(String appSecretKey) {
        this.appSecretKey = appSecretKey;
    }
}
