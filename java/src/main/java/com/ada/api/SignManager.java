package com.ada.api;

import java.util.HashMap;

/**
 * Created by ada on 2017/5/16.
 */
public class SignManager {

    private HashMap<Class<?>,Signable> signs=new HashMap<>();
    public Signable get(Class<?> requestObjectClass){
       return signs.get(requestObjectClass);
    }
    public void put(Class<?> requestObjectClass,Signable sign){
        signs.put(requestObjectClass,sign);
    }
}
