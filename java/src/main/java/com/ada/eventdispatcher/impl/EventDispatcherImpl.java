/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ada.eventdispatcher.impl;


import com.ada.eventdispatcher.Event;
import com.ada.eventdispatcher.EventDispatcher;
import com.ada.eventdispatcher.EventListener;

/**
 * Class capable of dispatching events.
 * 

 */
public class EventDispatcherImpl implements EventDispatcher {

  protected EventSupport eventSupport;
  protected boolean enabled = true;

  public EventDispatcherImpl() {
    eventSupport = new EventSupport();
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public boolean isEnabled() {
    return enabled;
  }

  @Override
  public void addEventListener(EventListener listenerToAdd) {
    eventSupport.addEventListener(listenerToAdd);
  }

  @Override
  public void addEventListener(EventListener listenerToAdd, String... types) {
    eventSupport.addEventListener(listenerToAdd, types);
  }

  @Override
  public void removeEventListener(EventListener listenerToRemove) {
    eventSupport.removeEventListener(listenerToRemove);
  }

  @Override
  public void dispatchEvent(Event event) {
    if (enabled) {
      eventSupport.dispatchEvent(event);
    }
  }


}
