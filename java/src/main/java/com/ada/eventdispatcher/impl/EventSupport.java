/* Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.ada.eventdispatcher.impl;

import com.ada.eventdispatcher.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Class that allows adding and removing event listeners and dispatching events to the appropriate listeners.
 * 

 */
public class EventSupport {

  private static final Logger LOG = LoggerFactory.getLogger(EventSupport.class);

  protected List<EventListener> eventListeners;
  protected Map<String, List<EventListener>> typedListeners;

  public EventSupport() {
    eventListeners = new CopyOnWriteArrayList<EventListener>();
    typedListeners = new HashMap<String, List<EventListener>>();
  }

  public synchronized void addEventListener(EventListener listenerToAdd) {
    if (listenerToAdd == null) {
      throw new EventIllegalArgumentException("Listener cannot be null.");
    }
    if (!eventListeners.contains(listenerToAdd)) {
      eventListeners.add(listenerToAdd);
    }
  }

  public synchronized void addEventListener(EventListener listenerToAdd, String... types) {
    if (listenerToAdd == null) {
      throw new EventIllegalArgumentException("Listener cannot be null.");
    }

    if (types == null || types.length == 0) {
      addEventListener(listenerToAdd);

    } else {
      for (String type : types) {
        addTypedEventListener(listenerToAdd, type);
      }
    }
  }

  public void removeEventListener(EventListener listenerToRemove) {
    eventListeners.remove(listenerToRemove);

    for (List<EventListener> listeners : typedListeners.values()) {
      listeners.remove(listenerToRemove);
    }
  }

  public void dispatchEvent(Event event) {
    if (event == null) {
      throw new EventIllegalArgumentException("Event cannot be null.");
    }

    if (event.getType() == null) {
      throw new EventIllegalArgumentException("Event type cannot be null.");
    }

    // Call global listeners
    if (!eventListeners.isEmpty()) {
      for (EventListener listener : eventListeners) {
        dispatchEvent(event, listener);
      }
    }

    // Call typed listeners, if any
    List<EventListener> typed = typedListeners.get(event.getType());
    if (typed != null && !typed.isEmpty()) {
      for (EventListener listener : typed) {
        dispatchEvent(event, listener);
      }
    }
  }

  protected void dispatchEvent(Event event, EventListener listener) {
    try {
      listener.onEvent(event);
    } catch (Throwable t) {
      if (listener.isFailOnException()) {
        throw new EventException("Exception while executing event-listener", t);
      } else {
        // Ignore the exception and continue notifying remaining listeners. The listener
        // explicitly states that the exception should not bubble up
        LOG.warn("Exception while executing event-listener, which was ignored", t);
      }
    }
  }

  protected synchronized void addTypedEventListener(EventListener listener, String type) {
    List<EventListener> listeners = typedListeners.get(type);
    if (listeners == null) {
      // Add an empty list of listeners for this type
      listeners = new CopyOnWriteArrayList<EventListener>();
      typedListeners.put(type, listeners);
    }

    if (!listeners.contains(listener)) {
      listeners.add(listener);
    }
  }
}
