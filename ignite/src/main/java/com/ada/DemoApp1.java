package com.ada;

import org.apache.ignite.Ignite;
import org.apache.ignite.IgniteCache;
import org.apache.ignite.Ignition;
import org.apache.ignite.client.ClientAddressFinder;
import org.apache.ignite.client.ClientCache;
import org.apache.ignite.client.IgniteClient;
import org.apache.ignite.configuration.ClientConfiguration;
import org.apache.ignite.configuration.IgniteConfiguration;
import org.apache.ignite.spi.discovery.tcp.TcpDiscoverySpi;
import org.apache.ignite.spi.discovery.tcp.ipfinder.multicast.TcpDiscoveryMulticastIpFinder;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Collections;

public class DemoApp1 {

    public static void main(String[] args) throws Exception {

        @NotNull IgniteConfiguration cfg = getClientConfiguration1();
        try (Ignite client = Ignition.start(cfg)) {
            //client.createCache("myCache");
            Collection<String> collection= client.cacheNames();
            for (String s : collection) {
                System.out.println(s);
            }
            IgniteCache<String, String> cache = client.getOrCreateCache("myCache2");
            System.out.println(cache.get("1"));
            cache.put("1", "Hello");
            cache.put("2", "World!");
            // Get data from the cache
        }
    }

    @NotNull
    private static ClientConfiguration getClientConfiguration() {
        ClientConfiguration cfg = new ClientConfiguration().setAddresses("127.0.0.1:10800");
        return cfg;
    }
    @NotNull
    private static ClientConfiguration getClientConfiguration2() {
        ClientAddressFinder finder = null;
        ClientConfiguration cfg = new ClientConfiguration().setAddressesFinder(finder);
        return cfg;
    }
    @NotNull
    private static IgniteConfiguration getClientConfiguration1() {
        IgniteConfiguration cfg = new IgniteConfiguration();

        // The node will be started as a client node.
        cfg.setClientMode(true);

        // Classes of custom Java logic will be transferred over the wire from this app.
        cfg.setPeerClassLoadingEnabled(true);

        // Setting up an IP Finder to ensure the client can locate the servers.
        TcpDiscoveryMulticastIpFinder ipFinder = new TcpDiscoveryMulticastIpFinder();
        ipFinder.setAddresses(Collections.singletonList("127.0.0.1:47500..47509"));
        cfg.setDiscoverySpi(new TcpDiscoverySpi().setIpFinder(ipFinder));
        cfg.setClientMode(true);
        return cfg;
    }


}
