package com.ada;

import org.apache.commons.chain.Chain;
import org.apache.commons.chain.impl.ChainBase;
import org.apache.commons.chain.impl.ContextBase;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws Exception {
        System.out.println( "Hello World!" );
        Chain chain=new ChainBase();
        chain.addCommand(new CountCommand());
        chain.execute(new ContextBase());
    }
}
