package com.ada.easymock;

import com.ada.api.Work;
import org.easymock.EasyMock;

/**
 * Created by ada on 2016/11/29.
 */
public class App {
    public static void main(String[] args) {
        Work work = EasyMock.createMock(Work.class);
        EasyMock.expect(work.name()).andReturn("ada").times(2);
        EasyMock.replay(work);
        System.out.println(work.name());
        System.out.println(work.name());
        EasyMock.verify(work);
    }
}
