package com.ada;

import org.apache.shiro.event.Subscribe;
import org.apache.shiro.event.support.DefaultEventBus;

/**
 * Created by ada on 2017/6/19.
 */
public class EventBusApps {
    public static void main(String[] args) {
        DefaultEventBus bus=new DefaultEventBus();
        bus.register(new EventBusApps());
        bus.publish("ada");
    }

    @Subscribe
    public void event(String msg){
        System.out.println(msg);
    }
}
