package com.ada;

import com.ada.interceptor.*;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {

        System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "TRACE");
        CommandInterceptor first = new SoutInterceptor();
        CommandInterceptor soutInterceptor = new SoutInterceptor();
        first.setNext(soutInterceptor);


        RetryInterceptor retryInterceptor = new RetryInterceptor();
        retryInterceptor.setWaitIncreaseFactor(2);
        retryInterceptor.setNumOfRetries(5);
        retryInterceptor.setWaitTimeInMs(1000);
        soutInterceptor.setNext(retryInterceptor);

        CommandInvoker invoker = new CommandInvoker();
        retryInterceptor.setNext(invoker);
        CommandExecutor commandExecutor = new CommandExecutorImpl( first);
        Command<String> demo=new StringCommand();
        String msg = commandExecutor.execute(demo);
        System.out.println(msg);
    }
}
