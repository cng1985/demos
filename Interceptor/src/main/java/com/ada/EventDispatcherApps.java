package com.ada;

import com.ada.event.ActivitiEvent;
import com.ada.event.ActivitiEventDispatcher;
import com.ada.event.ActivitiEventListener;
import com.ada.event.ActivitiEventType;
import com.ada.event.impl.ActivitiEventDispatcherImpl;

/**
 * Created by ada on 2017/6/19.
 */
public class EventDispatcherApps {

    public static void main(String[] args) {
        ActivitiEventDispatcher eventDispatcher = new ActivitiEventDispatcherImpl();

        eventDispatcher.addEventListener(new ActivitiEventListener() {
            @Override
            public void onEvent(ActivitiEvent event) {
                System.out.println("收到事件了！");
            }

            @Override
            public boolean isFailOnException() {
                return false;
            }
        },ActivitiEventType.ACTIVITY_CANCELLED);
        eventDispatcher.dispatchEvent(new ActivitiEvent() {
            @Override
            public ActivitiEventType getType() {
                return ActivitiEventType.ACTIVITY_CANCELLED;
            }

            @Override
            public String getExecutionId() {
                return null;
            }

            @Override
            public String getProcessInstanceId() {
                return null;
            }

            @Override
            public String getProcessDefinitionId() {
                return null;
            }
        });
    }

}
