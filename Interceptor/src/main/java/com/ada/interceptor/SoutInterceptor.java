package com.ada.interceptor;

public class SoutInterceptor extends AbstractCommandInterceptor {
    @Override
    public <T> T execute(Command<T> command) {
        System.out.println("************start*************");
        T result = next.execute(command);
        System.out.println("************end***************");
        return result;
    }

}
