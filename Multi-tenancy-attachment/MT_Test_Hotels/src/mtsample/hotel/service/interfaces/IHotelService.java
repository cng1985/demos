package mtsample.hotel.service.interfaces;

import java.util.Date;
import java.util.List;

import mtsample.hotel.model.HotelGuest;
import mtsample.hotel.model.RentHistory;
import mtsample.hotel.model.Room;

public interface IHotelService {
	public boolean login_admin(String login_id, String passwd);
	public List<Room> findRooms(String status);
	public List<Room> findRooms(String status, int bedNum);
	public List<Room> findRooms(String status, String name);
	public boolean checkin(String roomSerialNum, int guestId, Date start, Date end);
	public boolean checkout(String roomSerialNum, Date end);
	public List<HotelGuest> findAllHotelGuest();
	public List<HotelGuest> findHotelGuestByName(String name);
	public void addHotelGuest(HotelGuest guest);
	public List<RentHistory> findAllRentHistory();
	public List<RentHistory> findRentHistoryByName(String name);
}
