package mtsample.hotel.test;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import mtsample.hotel.dao.HotelGuestDao;
import mtsample.hotel.dao.RentHistoryDao;
import mtsample.hotel.exception.TestException;
import mtsample.hotel.model.HotelGuest;
import mtsample.hotel.model.RentHistory;

public class ScratchTest extends TestFramework{
	private static final String PU_BASE = "MT_HOTEL_SERVICE";
	
	private static final String DATE_FORMAT_PATTERN = "yyyy.MM.dd H:mm";
	private static SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);
	
	
	
	private static final String TENANT_ID = "tenant.id";
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try{
			ScratchTest tester = new ScratchTest();
			tester.testDate();
			tester.testDoubleInt();
			tester.testSave();
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	private void testSave() throws ParseException{
		HotelGuestDao guestDao = new HotelGuestDao(PU_BASE);
		Map<String, String> contextProps = new HashMap<String, String>();
		contextProps.put(TENANT_ID, "GE_LIN");
		RentHistoryDao rentHistoryDao = new RentHistoryDao(PU_BASE, contextProps);
		try{
			HotelGuest guest = new HotelGuest();
			guest.setId(101);
			guest.setAddress("aaa");
			guest.setName("g1");
			guest.setTelephone("12345678901");
			//guest.setTenantId("GE_LIN");
			guest.setCreateTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			
			guestDao.start();
			guestDao.transactionBegin();
			guestDao.addHotelGuest(guest);
			guestDao.transactionCommit();
			guestDao.end();
			
			RentHistory history = new RentHistory();
			//history.setId(103); !!!auto_increment
			history.setRoomId(3);
			history.setHotelGuestId(4);
			history.setCreateTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			String inStr = "2013.09.19 17:00";
			String outStr = "2013.10.10 19:30";
			Date in = formatter.parse(inStr);
			Date out = formatter.parse(outStr);
			history.setStartTime(in);
			history.setEndTime(out);
			history.setAmount(0);
			//history.setTenantId("GE_LIN"); !!! NO NEED with Mutlitenancy support
			rentHistoryDao.start();
			rentHistoryDao.transactionBegin();
			rentHistoryDao.add(history);
			rentHistoryDao.transactionCommit();
			rentHistoryDao.end();
			
			guest = new HotelGuest();
			guest.setId(102);
			guest.setAddress("bbb");
			guest.setName("g2");
			guest.setTelephone("12345678902");
			//guest.setTenantId("GE_LIN");
			guest.setCreateTime(new Timestamp(Calendar.getInstance().getTimeInMillis()));
			
			guestDao.start();
			guestDao.addHotelGuest(guest);
			guestDao.end();
			
		} catch (TestException e) {
			e.printStackTrace();
		}
	}
	
	private void testDate() throws ParseException{
		String inStr = "2013.09.19 17:00";
		String outStr = "2013.10.10 19:30";
		Date in = formatter.parse(inStr);
		Date out = formatter.parse(outStr);
		System.out.println(out.compareTo(in));
		float interval_days = (float)( (out.getTime() - in.getTime()) / (float) (1000 * 60 * 60 * 24) );
		System.out.println("interval days:" + interval_days);
	}
	
	private void testDoubleInt(){
		double a = 21.104166;
		double b = 21.504;
		System.out.println((int)a);
		System.out.println((int)b);
		System.out.println(a - (int)a);
		System.out.println(b - (int)b);
	}

}
