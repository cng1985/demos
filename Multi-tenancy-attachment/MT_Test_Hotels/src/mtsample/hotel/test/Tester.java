package mtsample.hotel.test;

import java.util.List;

public class Tester {

	protected <T> void print(T t){
		System.out.println(t);
	}
	
	protected void print(char c){
		System.out.print(c);
	}
	
	protected <T> void printList(List<T> list){
		for (T t:list){
			print(t);
		}
	}

	protected void print(int num, char c){
		for (int i = 0; i < num; i++) print(c);
	}
	
	protected void print_separator_line(){
		print(80, '-');
		print('\n');
	}
}
