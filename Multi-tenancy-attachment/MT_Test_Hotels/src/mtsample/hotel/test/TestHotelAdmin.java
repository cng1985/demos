package mtsample.hotel.test;

import java.io.Console;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import mtsample.hotel.client.HotelAdminClient;
import mtsample.hotel.model.HotelGuest;
import mtsample.hotel.model.RentHistory;
import mtsample.hotel.model.Room;

public class TestHotelAdmin extends TestFramework {
	private HotelAdminClient client = null;
	private Console console = null;
	private static final String[] MENUS = {
		//0
		"保留",
		//1
		"1.查房  2.订房 3.退房 4.登出:",
		//2
		"1.获取可用房间  2.获取可用房间（床位数） 3.获取可用房间（类型）4.返回:",
		//3
		"1.1床 2.2床 3.3床:",
		//4
		"1.标准房 2.大床房 3.豪华房:",
		//5
		"1.创建房客信息 2.查询房客信息 3.办理入住 4.返回:",
		//6
		"1.获取所有房客 2.查询房客(姓名) 3.返回:",
		//7
		"1.查询房客信息 2.办理退房 3.查看入住历史信息 4.返回:",
		//8
		"1.获取所有入住历史信息 2.查询入住历史信息(姓名) 3.返回:",
	};
	
	private static final String[] HINTS = {
		//0
		"保留",
		//1
		"*****登录*****",
		//2
		"用户名:",
		//3
		"密码:",
		//4
		"*****入住*****",
		//5
		"请输入房间号:",
		//6
		"请输入房客编号:",
		//7
		"请输入房客姓名:",
		//8
		"请输入房客id:",
		//9
		"请输入入住时间(yyyy.MM.dd H:mm):",
		//10
		"请输入退房时间(yyyy.MM.dd H:mm):",
		//11
		"*****退房*****",
		//12
		"*****查房*****"
	};
	
	private int bedNums[] = {-1, 1, 2, 3};
	
	private String roomNames[] = {
		"",
		"标准房 ",
		"大床房",
		"豪华房"
	};
	
	private static final String DATE_FORMAT_PATTERN = "yyyy.MM.dd H:mm";
	private static SimpleDateFormat formatter = new SimpleDateFormat(DATE_FORMAT_PATTERN);

	// ERROR MESSAGE
	private static final String ERR_MSG_INVALID_DATE_FORMATE = "日期格式可能有错.";
	
	public TestHotelAdmin(){
		client = new HotelAdminClient();
		client.init();
		console = System.console();
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// 初始化测试程序
		TestHotelAdmin tester = new TestHotelAdmin();
		// 测试
		tester.test();
	}
	
	public void test(){
		while (true) {
			// 登录
			if(!login()){
				continue;
			}
			boolean done = false;
			while(!done){
				switch (menu(1)) {
				case 1: // 查房
					search();
					break;
				case 2: // 入住
					checkin();
					break;
				case 3: // 退房
					checkout();
					break;
				case 4: // 登出
					logout();
				default:
					done = true;
				}
			}
		}
	}
	

	private boolean login(){
		hint(1);
		String user = console.readLine(HINTS[2]);
		char[] passwd = console.readPassword(HINTS[3]);
		return client.login(user, String.valueOf(passwd));
	}
	
	private void logout(){
		client.logout();
	}
	
	private void search(){
		hint(12);
		List<Room> rooms = null;
		boolean done = false;
		while (!done) {
			switch (menu(2)) {
			case 1:
				rooms = client.getAvailableRoom();
				printList(rooms);
				break;
			case 2:
				int bedNum = bedNums[menu(3)];
				rooms = client.getAvailableRoomByBedNum(bedNum);
				printList(rooms);
				break;
			case 3:
				String name = roomNames[menu(4)];
				rooms = client.getAvailableRoomByName(name);
				printList(rooms);
				break;
			case 4:
				done = true;
			default:
				break;
			}
		}
	}
	
	private void checkin(){
		hint(4);
		boolean done = false;
		while (!done) {
			switch (menu(5)) {
			case 1:
				create_hotel_guest();
				break;
			case 2:
				search_hotel_guest();
				break;
			case 3:
				try {
					do_checkin();
				} catch (ParseException e) {
					print(ERR_MSG_INVALID_DATE_FORMATE);
					e.printStackTrace();
				}
				break;
			case 4:
				done = true;
			default:
				break;
			}
		}
	}
	
	private void create_hotel_guest(){
		
	}
	
	private void search_hotel_guest(){
		List<HotelGuest> guests = null;
		boolean done = false;
		while (!done) {
			switch (menu(6)) {
			case 1:
				guests = client.getAllHotelGuest();
				printList(guests);
				break;
			case 2:
				String guestName = console.readLine(HINTS[7]);
				guests = client.getHotelGuestByName(guestName);
				printList(guests);
				break;
			case 3:
				done = true;
			default:
				break;
			}
		}
	}
	
	private void do_checkin() throws ParseException{
		String roomSerialNumber = console.readLine(HINTS[5]);
		String guestId = console.readLine(HINTS[6]);
		String inStr = console.readLine(HINTS[9]);
		String outStr = console.readLine(HINTS[10]);
		Date in = formatter.parse(inStr);
		Date out = formatter.parse(outStr);
		client.checkin(roomSerialNumber, Integer.valueOf(guestId), in, out);
	}
	
	private void checkout(){
		hint(11);
		boolean done = false;
		while (!done) {
			switch (menu(7)) {
			case 1:
				search_hotel_guest();
				break;
			case 2:
				try {
					do_checkout();
				} catch (ParseException e) {
					print(ERR_MSG_INVALID_DATE_FORMATE);
					e.printStackTrace();
				}
				break;
			case 3:
				search_rent_history();
				break;
			case 4:
				done = true;
			default:
				break;
			}
		}
	}
	
	private void search_rent_history(){
		List<RentHistory> histories = null;
		boolean done = false;
		while (!done) {
			switch (menu(8)) {
			case 1:
				histories = client.getAllRentHistory();
				printList(histories);
				break;
			case 2:
				String guestName = console.readLine(HINTS[7]);
				histories = client.getRentHistoryByHotelGuestName(guestName);
				printList(histories);
				break;
			case 3:
				done = true;
			default:
				break;
			}
		}
	}
	private void do_checkout() throws ParseException{
		String roomSerialNumber = console.readLine(HINTS[5]);
		String outStr = console.readLine(HINTS[10]);
		Date out = formatter.parse(outStr);
		client.checkout(roomSerialNumber, out);
	}
	
	private int menu(int id){
		print(MENUS[id]);
		return Integer.valueOf(console.readLine());
	}
	
	private void hint(int id){
		print(HINTS[id]);
	}

}
