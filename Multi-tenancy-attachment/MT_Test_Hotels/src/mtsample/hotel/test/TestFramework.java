package mtsample.hotel.test;

import java.util.List;

public class TestFramework {
	protected static <T> void print(T t){
		System.out.println(t);
	}
	
	protected static <T> void printList(List<T> list){
		if(list == null){
			return;
		}
		for (T t:list){
			print(t);
		}
	}
}
