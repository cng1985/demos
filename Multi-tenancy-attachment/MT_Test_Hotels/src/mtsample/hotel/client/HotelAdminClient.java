package mtsample.hotel.client;

import java.util.Date;
import java.util.List;

import mtsample.hotel.model.HotelGuest;
import mtsample.hotel.model.RentHistory;
import mtsample.hotel.model.Room;
import mtsample.hotel.service.HotelService;
import mtsample.hotel.service.interfaces.IHotelService;


public class HotelAdminClient {
	private String login_id = null;
	private IHotelService hotel_service = null;

	public void init() {
		// get remote service
		if (hotel_service == null) {
			hotel_service = new HotelService();
		}
	}

	public boolean login(String login_id, String passwd){
		if(hotel_service.login_admin(login_id, passwd)){
			System.out.println(login_id + " 已登录.");
			// set login_id
			this.login_id = login_id;
			return true;
		} else {
			return false;
		}
	}

	public void logout() {
		System.out.println(login_id + " 正在登出...");
		// null the service
		hotel_service = null;
		// null the login_id
		login_id = null;
		System.out.println("再见");
	}
	
	public List<Room> getAvailableRoom(){
		return hotel_service.findRooms("free");
	}
	
	public List<Room> getAvailableRoomByBedNum(int bedNum){
		return hotel_service.findRooms("free", bedNum);
	}

	public List<Room> getAvailableRoomByName(String name){
		return hotel_service.findRooms("free", name);
	}
	
	public boolean checkin(String roomSerialNum, int guestId, Date start, Date end){
		return hotel_service.checkin(roomSerialNum, guestId, start, end);
	}
	
	public boolean checkout(String roomSerialNum, Date end){
		return hotel_service.checkout(roomSerialNum, end);
	}
	
	public List<HotelGuest> getAllHotelGuest(){
		return hotel_service.findAllHotelGuest();
	}
	
	public List<HotelGuest> getHotelGuestByName(String name){
		return hotel_service.findHotelGuestByName(name);
	}
	
	public List<RentHistory> getAllRentHistory(){
		return hotel_service.findAllRentHistory();
	}
	
	public List<RentHistory> getRentHistoryByHotelGuestName(String name){
		return hotel_service.findRentHistoryByName(name);
	}
	
	public void addHotelGuest(HotelGuest guest){
		hotel_service.addHotelGuest(guest);
	}
}
