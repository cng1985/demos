package mtsample.hotel.dao;

import java.util.List;
import java.util.Map;

import mtsample.hotel.dao.interfaces.IRentHistoryDao;
import mtsample.hotel.model.RentHistory;

public class RentHistoryDao extends BaseDao implements IRentHistoryDao {
	private static final String NQ_FIND_RENTHISTORY_BY_ROOMID = "find_renthistory_by_room_id";
	private static final String NQ_FIND_RENTHISTORY_BY_ROOMID_AND_AMOUNT_NOT_CHECKOUT = "find_renthistory_by_room_id_and_amount_not_checkout";
	private static final String NQ_FIND_RENTHISTORY_BY_HOTELGUESTID = "find_renthistory_by_hotel_guest_id";
	private static final String NQ_FIND_RENTHISTORY_BY_HOTELGUESTNAME = "find_renthistory_by_hotel_guest_name";
	private static final String NQ_FIND_RENTHISTORY_BY_ID = "find_renthistory_by_id";
	private static final String NQ_FIND_RENTHISTORY_ALL = "find_renthistory_all";
	private static final String NQ_FIND_RENTHISTORY_BY_ROOM_ID_AND_HOTELGUESTID = "find_renthistory_by_room_id_and_hotel_guest_id";
	private static final String NQ_FIND_RENTHISTORY_BY_ROOM_ID_AND_HOTELGUESTID_AND_AMOUNT_NOT_CHECKOUT = "find_renthistory_by_room_id_and_hotel_guest_id_and_amount_not_checkout";
	private static final String NQ_ROOMID = "roomId";
	private static final String NQ_HOTELGUESTID = "hotelGuestId";
	private static final String NQ_HOTELGUESTNAME = "hotelGuestName";
	private static final String NQ_ID = "id";
	public RentHistoryDao(String pu_name){
		super(pu_name);
	}
	
	public RentHistoryDao(String pu_name, Map<String, String> props){
		super(pu_name, props);
	}

	@Override
	public void add(RentHistory history) {
		super.save(history);
	}
	
	@Override
	public void update(RentHistory history){
		super.update(history);
	}

	@Override
	public List<RentHistory> loadAll() {
		return super.queryResultList(NQ_FIND_RENTHISTORY_ALL, RentHistory.class, (String[])null, (Object[])null);
	}

	@Override
	public List<RentHistory> queryRentHistoryByRoomId(int roomId){
		return super.queryResultList(NQ_FIND_RENTHISTORY_BY_ROOMID, RentHistory.class, NQ_ROOMID, roomId);
	}
	
	@Override
	public List<RentHistory> queryRentHistoryByRoomIdAmountNotCheckout(int roomId){
		return super.queryResultList(NQ_FIND_RENTHISTORY_BY_ROOMID_AND_AMOUNT_NOT_CHECKOUT, RentHistory.class, NQ_ROOMID, roomId);
	}
	
	@Override
	public List<RentHistory> queryRentHistoryByHotelGuestId(int hotelGuestId){
		return super.queryResultList(NQ_FIND_RENTHISTORY_BY_HOTELGUESTID, RentHistory.class, NQ_HOTELGUESTID, hotelGuestId);
	}
	
	@Override
	public List<RentHistory> queryRentHistoryByHotelGuestName(String hotelGuestName){
		return super.queryResultList(NQ_FIND_RENTHISTORY_BY_HOTELGUESTNAME, RentHistory.class, NQ_HOTELGUESTNAME, hotelGuestName);
	}

	@Override
	public RentHistory queryRentHistoryById(int id) {
		return super.querySingleResult(NQ_FIND_RENTHISTORY_BY_ID, RentHistory.class, NQ_ID, id);
	}

	@Override
	public List<RentHistory> queryRentHistoryByRoomIdGuestId(int roomId,
			int hotelGuestId) {
		String argNames[] = {NQ_ROOMID, NQ_HOTELGUESTID};
		Object argValues[] = {roomId, hotelGuestId};
		return super.queryResultList(NQ_FIND_RENTHISTORY_BY_ROOM_ID_AND_HOTELGUESTID, RentHistory.class, argNames, argValues);
	}
	
	@Override
	public List<RentHistory> queryRentHistoryByRoomIdGuestIdAmountNotCheckout(int roomId,
			int hotelGuestId) {
		String argNames[] = {NQ_ROOMID, NQ_HOTELGUESTID};
		Object argValues[] = {roomId, hotelGuestId};
		return super.queryResultList(NQ_FIND_RENTHISTORY_BY_ROOM_ID_AND_HOTELGUESTID_AND_AMOUNT_NOT_CHECKOUT, RentHistory.class, argNames, argValues);
	}
}
