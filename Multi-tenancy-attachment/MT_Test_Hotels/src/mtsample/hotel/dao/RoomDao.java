package mtsample.hotel.dao;

import java.util.List;
import java.util.Map;

import mtsample.hotel.dao.interfaces.IRoomDao;
import mtsample.hotel.model.Room;

public class RoomDao extends BaseDao implements IRoomDao {

	private static final String NQ_FIND_ROOM_BY_STATUS = "find_room_by_status";
	private static final String NQ_STATUS = "status";
	private static final String NQ_FIND_ROOM_BY_STATUS_AND_BEDNUM = "find_room_by_status_and_bednum";
	private static final String NQ_BEDNUM = "bedNum";
	private static final String NQ_FIND_ROOM_BY_STATUS_AND_NAME = "find_room_by_status_and_name";
	private static final String NQ_NAME = "name";
	private static final String NQ_FIND_ROOM_BY_ID = "find_room_by_id";
	private static final String NQ_ID = "id";
	private static final String STATUS_FREE = "Free";
	private static final String STATUS_BUSY = "Busy";
	private static final String NQ_FIND_ROOM_BY_SERIALNUM = "find_room_by_serialNumber";
	private static final String NQ_SERIALNUM = "serialNumber";

	public RoomDao(String pu_name) {
		super(pu_name);
	}

	public RoomDao(String pu_name, Map<String, String> props) {
		super(pu_name, props);
	}

	@Override
	public void add(Room room) {
	}

	@Override
	public Room load(Integer id) {
		return null;
	}

	@Override
	public List<Room> loadAll() {
		return null;
	}
	
	@Override
	public List<Room> queryRoomByStatus(String status){
		return super.queryResultList(NQ_FIND_ROOM_BY_STATUS, Room.class, NQ_STATUS, status);
	}
	
	@Override
	public Room queryRoomBySerialNum(String serialNum){
		return super.querySingleResult(NQ_FIND_ROOM_BY_SERIALNUM, Room.class, NQ_SERIALNUM, serialNum);
	}
	
	@Override
	public List<Room> queryRoomByStatusAndBedNum(String status, int bedNum){
		String argNames[] = {NQ_STATUS, NQ_BEDNUM};
		Object argValues[] = {status, new Integer(bedNum)};
		return super.queryResultList(NQ_FIND_ROOM_BY_STATUS_AND_BEDNUM, Room.class, argNames, argValues);
	}

	@Override
	public List<Room> queryRoomByStatusAndName(String status, String name){
		String argNames[] = {NQ_STATUS, NQ_NAME};
		Object argValues[] = {status, name};
		return super.queryResultList(NQ_FIND_ROOM_BY_STATUS_AND_NAME, Room.class, argNames, argValues);
	}
	
	@Override
	public Room queryRoomById(int id){
		return super.querySingleResult(NQ_FIND_ROOM_BY_ID, Room.class, NQ_ID, id);
	}
	
	@Override
	public int checkinRoom(String roomSerialNum) {
		//change Room status to "busy"
		Room room = queryRoomBySerialNum(roomSerialNum);
		room.setStatus(STATUS_BUSY);
		super.update(room);
		return room.getId();
	}

	@Override
	public int checkoutRoom(String roomSerialNum) {
		//change Room status to "free"
		Room room = queryRoomBySerialNum(roomSerialNum);
		room.setStatus(STATUS_FREE);
		super.update(room);
		return room.getId();
	}

}

//@Override
//public List<Room> queryRoomByStatus(String status) {
//	CriteriaBuilder cb = getCriteriaBuilder();
//	CriteriaQuery<Room> cq = cb.createQuery(Room.class);
//	Root<Room> room = cq.from(Room.class);
//	cq.where(cb.equal(room.get("status"), status));
//	return super.queryResultList(cq);
//}
//
//@Override
//public Room queryRoomBySeiralNum(String serial) {
//	CriteriaBuilder cb = getCriteriaBuilder();
//	CriteriaQuery<Room> cq = cb.createQuery(Room.class);
//	Root<Room> room = cq.from(Room.class);
//	cq.where(cb.equal(room.get("serialNumber"), serial));
//	return super.querySingleResult(cq);
//}