package mtsample.hotel.dao.interfaces;

import java.util.List;

import mtsample.hotel.model.Room;

public interface IRoomDao {
	public void add(Room room);
	
	public Room load(Integer id);
	
	public List<Room> loadAll();
	
	public Room queryRoomById(int id);
	
	public List<Room> queryRoomByStatus(String status);
	
	public List<Room> queryRoomByStatusAndBedNum(String status, int bedNum);
	
	public List<Room> queryRoomByStatusAndName(String status, String name);
	
	public Room queryRoomBySerialNum(String serialNum);
	
	public int checkinRoom(String roomSerialNum);
	
	public int checkoutRoom(String roomSerialNum);
}
//public List<Room> queryRoomByStatus(String status);
//
//public Room queryRoomBySeiralNum(String serial);