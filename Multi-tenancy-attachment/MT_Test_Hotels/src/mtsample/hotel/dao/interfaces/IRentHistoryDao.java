package mtsample.hotel.dao.interfaces;

import java.util.List;

import mtsample.hotel.model.RentHistory;

public interface IRentHistoryDao {
	public void add(RentHistory history);
	public void update(RentHistory history);
	public List<RentHistory> loadAll();
	public RentHistory queryRentHistoryById(int id);
	public List<RentHistory> queryRentHistoryByHotelGuestName(String hotelGuestName);
	public List<RentHistory> queryRentHistoryByRoomId(int roomId);
	public List<RentHistory> queryRentHistoryByRoomIdAmountNotCheckout(int roomId);
	public List<RentHistory> queryRentHistoryByHotelGuestId(int hotelGuestId);
	public List<RentHistory> queryRentHistoryByRoomIdGuestId(int roomId, int hotelGuestId);
	public List<RentHistory> queryRentHistoryByRoomIdGuestIdAmountNotCheckout(int roomId, int hotelGuestId);
}
