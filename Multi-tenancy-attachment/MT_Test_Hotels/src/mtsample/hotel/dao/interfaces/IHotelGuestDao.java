package mtsample.hotel.dao.interfaces;

import java.util.List;

import mtsample.hotel.model.HotelGuest;

public interface IHotelGuestDao {
	public HotelGuest load(String guest_id);
	public void addHotelGuest(HotelGuest admin);
	public List<HotelGuest> loadAll();
	public List<HotelGuest> queryHoteGuestByNId(String name);
	public List<HotelGuest> queryHoteGuestByName(String name);
	public HotelGuest removeHotelGuest(String guest_id);
}
