package mtsample.hotel.dao.interfaces;

import java.util.List;

import mtsample.hotel.model.HotelAdmin;

public interface IHotelAdminDao {
	public HotelAdmin load(String admin_id);
	public List<HotelAdmin> loadAll();
	public void addHotelAdmin(HotelAdmin admin);
	public HotelAdmin removeHotelAdmin(String admin_id);
}
