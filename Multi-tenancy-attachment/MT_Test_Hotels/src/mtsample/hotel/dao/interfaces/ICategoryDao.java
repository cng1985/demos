package mtsample.hotel.dao.interfaces;

import java.util.List;

import mtsample.hotel.model.Category;

public interface ICategoryDao {
	
	public void save(Category room);
	
	public Category load(Integer id);
	
	public List<Category> loadAll();

}
