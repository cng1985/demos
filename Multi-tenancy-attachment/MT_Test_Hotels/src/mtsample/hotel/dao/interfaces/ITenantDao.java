package mtsample.hotel.dao.interfaces;

import java.util.List;

import mtsample.hotel.model.Tenant;

public interface ITenantDao {
	public Tenant load(String tenant_id);
	public List<Tenant> loadAll();
	public void addTenant(Tenant tenant);
	public Tenant removeTenant(String tenant_id);
}
