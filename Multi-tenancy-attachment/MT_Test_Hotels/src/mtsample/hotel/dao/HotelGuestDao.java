package mtsample.hotel.dao;

import java.util.List;

import java.util.Map;

import mtsample.hotel.dao.interfaces.IHotelGuestDao;
import mtsample.hotel.model.HotelGuest;

public class HotelGuestDao extends BaseDao implements IHotelGuestDao {
	private static final String NQ_FIND_HOTELGUEST_ALL = "find_hotelguest_all";
	private static final String NQ_FIND_HOTELGUEST_BY_ID = "find_hotelguest_by_id";
	private static final String NQ_FIND_HOTELGUEST_BY_NAME = "find_hotelguest_by_name";
	private static final String NQ_NAME = "name";
	private static final String NQ_ID = "id";
	
	public HotelGuestDao(String pu_name) {
		super(pu_name);
	}

	public HotelGuestDao(String pu_name, Map<String, String> props) {
		super(pu_name, props);
	}
	
	@Override
	public HotelGuest load(String guest_id) {
		return null;
	}

	@Override
	public List<HotelGuest> loadAll() {
		return super.queryResultList(NQ_FIND_HOTELGUEST_ALL, HotelGuest.class, (String[])null, (Object[])null);
	}
	
	@Override
	public List<HotelGuest> queryHoteGuestByName(String name){
		return super.queryResultList(NQ_FIND_HOTELGUEST_BY_NAME, HotelGuest.class, NQ_NAME, name);
	}
	
	@Override
	public List<HotelGuest> queryHoteGuestByNId(String name){
		return super.queryResultList(NQ_FIND_HOTELGUEST_BY_ID, HotelGuest.class, NQ_ID, name);
	}

	@Override
	public void addHotelGuest(HotelGuest guest) {
		super.save(guest);
	}

	@Override
	public HotelGuest removeHotelGuest(String guest_id) {
		return null;
	}

}
