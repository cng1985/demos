package mtsample.hotel.dao;

import java.util.List;
import java.util.Map;

import mtsample.hotel.dao.interfaces.IHotelAdminDao;
import mtsample.hotel.model.HotelAdmin;

public class HotelAdminDao extends BaseDao implements IHotelAdminDao {

	public HotelAdminDao(String pu_name) {
		super(pu_name);
	}

	public HotelAdminDao(String pu_name, Map<String, String> props) {
		super(pu_name, props);
	}

	@Override
	public HotelAdmin load(String admin_id) {
		return super.load(HotelAdmin.class, admin_id);
	}

	@Override
	public List<HotelAdmin> loadAll() {
		return super.loadAll(HotelAdmin.class);
	}

	@Override
	public void addHotelAdmin(HotelAdmin admin) {
		
	}

	@Override
	public HotelAdmin removeHotelAdmin(String admin_id) {

		return null;
	}

}
