package mtsample.hotel.dao;

import java.util.List;

import mtsample.hotel.dao.interfaces.ITenantDao;
import mtsample.hotel.model.Tenant;

public class TenantDao extends BaseDao implements ITenantDao {

	public TenantDao(String pu_name)  {
		super(pu_name);
	}

	@Override
	public Tenant load(String tenant_id) {
		return super.load(Tenant.class, tenant_id);
	}

	@Override
	public List<Tenant> loadAll() {
		return super.loadAll(Tenant.class);
	}

	@Override
	public void addTenant(Tenant tenant) {
		super.save(tenant);
	}

	@Override
	public Tenant removeTenant(String tenant_id) {

		return null;
	}

}
