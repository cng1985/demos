
/* tenant */
insert into tenant(tenant_id, name, description) values ('XIN_YA', '新亚酒店', '上海新亚酒店');
insert into tenant(tenant_id, name, description) values ('GE_LIN', '格林酒店', '上海格林酒店');
insert into tenant(tenant_id, name, description) values ('SHI_BO', '世博酒店', '上海世博酒店');

/* hotel_admin */
insert into hotel_admin(id, login_id, password, name, email, tenant_id) values (1, 'victor', '123456', '刘盛彬', 'victor@gmail.com', 'XIN_YA');
insert into hotel_admin(id, login_id, password, name, email, tenant_id) values (2, 'letian', '123456', '任乐天', 'letian@gmail.com', 'GE_LIN');
insert into hotel_admin(id, login_id, password, name, email, tenant_id) values (3, 'zhengyun', '123456', '陈争云', 'zhengyun@gmail.com', 'SHI_BO');

/* hotel_guest */
insert into hotel_guest(id, name, telephone, address, tenant_id) values (1, 'Victor', '56008888', '上海科苑路399号', 'XIN_YA');
insert into hotel_guest(id, name, telephone, address, tenant_id) values (2, 'Jacky', '66668822', '上海科苑路399号', 'XIN_YA');
insert into hotel_guest(id, name, telephone, address, tenant_id) values (3, 'Anton', '33355566', '上海南京路8号', 'GE_LIN');
insert into hotel_guest(id, name, telephone, address, tenant_id) values (4, 'Gus', '33355566', '北京大道3号', 'GE_LIN');
insert into hotel_guest(id, name, telephone, address, tenant_id) values (5, 'Grace', '23456789', '上海张扬路10号', 'SHI_BO');
insert into hotel_guest(id, name, telephone, address, tenant_id) values (6, 'Mary', '34567890', '北京东方路20号', 'SHI_BO');

/* category */
insert into category(id, name, bed_num, price, tenant_id) values (1, '标准房', 2, 100.00, 'XIN_YA');
insert into category(id, name, bed_num, price, tenant_id) values (2, '豪华房', 3, 200.00, 'XIN_YA');
insert into category(id, name, bed_num, price, tenant_id) values (3, '标准房', 2, 100.00, 'GE_LIN');
insert into category(id, name, bed_num, price, tenant_id) values (4, '豪华房', 3, 200.00, 'GE_LIN');
insert into category(id, name, bed_num, price, tenant_id) values (5, '标准房', 2, 100.00, 'SHI_BO');
insert into category(id, name, bed_num, price, tenant_id) values (6, '豪华房', 3, 200.00, 'SHI_BO');

/* room */
insert into room(id, serial_number, position, category_id, status, tenant_id) values (1, 'R1011', '南楼', 1, 'Free', 'XIN_YA');
insert into room(id, serial_number, position, category_id, status, tenant_id) values (2, 'R1012', '北楼', 2, 'Free', 'XIN_YA');
insert into room(id, serial_number, position, category_id, status, tenant_id) values (3, 'R2011', '南楼', 3, 'Free', 'GE_LIN');
insert into room(id, serial_number, position, category_id, status, tenant_id) values (4, 'R2012', '北楼', 4, 'Free', 'GE_LIN');
insert into room(id, serial_number, position, category_id, status, tenant_id) values (5, 'R3011', '南楼', 5, 'Free', 'SHI_BO');
insert into room(id, serial_number, position, category_id, status, tenant_id) values (6, 'R3012', '北楼', 6, 'Free', 'SHI_BO');
insert into room(id, serial_number, position, category_id, status, tenant_id) values (7, 'R7011', '南楼', 1, 'Free', 'XIN_YA');
insert into room(id, serial_number, position, category_id, status, tenant_id) values (8, 'R8012', '北楼', 2, 'Free', 'XIN_YA');

/* rent_history */
insert into rent_history(id, start_time, end_time, room_id, amount, hotel_guest_id, create_time, tenant_id) values (1, '2013-09-18 13:28:00', '2013-09-20 11:28:00', 3, 1040.0, 3, '2013-09-17 13:28:00', 'GE_LIN');
insert into rent_history(id, start_time, end_time, room_id, amount, hotel_guest_id, create_time, tenant_id) values (2, '2013-09-18 13:30:00', '2013-09-20 10:47:00', 4, 1140.0, 4, '2013-09-18 0:30:00', 'XIN_YA');
