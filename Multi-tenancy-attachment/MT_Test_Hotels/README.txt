run the sample in cmd line:

Run
java -classpath build/classes/;D:/workspace/eclipselink/jlib/jpa/javax.persistence_2.1.0.v201304241213.jar;D:/workspace/eclipselink/jlib/eclipselink.jar;D:/workspace/mysql/mysql-connector-java-5.1.26/mysql-connector-java-5.1.26-bin.jar mtsample.hotel.test.TestHotelAdmin

Debug
java -Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=n,address=8000 -classpath build/classes/;D:/workspace/eclipselink/jlib/jpa/javax.persistence_2.1.0.v201304241213.jar;D:/workspace/eclipselink/jlib/eclipselink.jar;D:/workspace/mysql/mysql-connector-java-5.1.26/mysql-connector-java-5.1.26-bin.jar mtsample.hotel.test.TestHotelAdmin


Sample results:

D:\workspace\eWS\MT_Test_Hotels>java -classpath build\classes\;D:\workspace\eclipselink\jlib\jpa\javax.persistence_2.1.0.v201304241213.jar;D:\workspace\eclipsel
ink\jlib\eclipselink.jar;D:\workspace\mysql\mysql-connector-java-5.1.26\mysql-connector-java-5.1.26-bin.jar mtsample.hotel.test.TestHotelAdmin
Tenant [tenantId=GE_LIN, createTime=2013-09-26 13:02:30.0, description=上海格林酒店, name=格林酒店]
Tenant [tenantId=SHI_BO, createTime=2013-09-26 13:02:31.0, description=上海世博酒店, name=世博酒店]
Tenant [tenantId=XIN_YA, createTime=2013-09-26 13:02:29.0, description=上海新亚酒店, name=新亚酒店]
HotelAdmin [id=1, createTime=2013-09-26 13:02:32.0, email=victor@gmail.com, loginId=victor, name=刘盛彬, password=123456]
HotelAdmin [id=2, createTime=2013-09-26 13:02:33.0, email=letian@gmail.com, loginId=letian, name=任乐天, password=123456]
HotelAdmin [id=3, createTime=2013-09-26 13:02:34.0, email=zhengyun@gmail.com, loginId=zhengyun, name=陈争云, password=123456]
*****登录*****
用户名:letian
密码:
letian 已登录.
1.查房  2.订房 3.退房 4.登出:
1
*****查房*****
1.获取可用房间  2.获取可用房间（床位数） 3.获取可用房间（类型）4.返回:
1
Room [id=3, categoryId=3, createTime=2013-09-26 13:02:49.0, position=南楼, serialNumber=R2011, status=Free]
Room [id=4, categoryId=4, createTime=2013-09-26 13:02:50.0, position=北楼, serialNumber=R2012, status=Free]
1.获取可用房间  2.获取可用房间（床位数） 3.获取可用房间（类型）4.返回:
4
1.查房  2.订房 3.退房 4.登出:
2
*****入住*****
1.创建房客信息 2.查询房客信息 3.办理入住 4.返回:
2
1.获取所有房客 2.查询房客(姓名) 3.返回:
1
HotelGuest [id=3, address=上海南京路8号, createTime=2013-09-26 13:02:37.0, name=Anton, telephone=33355566, ]
HotelGuest [id=4, address=北京大道3号, createTime=2013-09-26 13:02:38.0, name=Gus, telephone=33355566, ]
1.获取所有房客 2.查询房客(姓名) 3.返回:
3
1.创建房客信息 2.查询房客信息 3.办理入住 4.返回:
2
1.获取所有房客 2.查询房客(姓名) 3.返回:
3
1.创建房客信息 2.查询房客信息 3.办理入住 4.返回:
3
请输入房间号:R2012
请输入房客编号:4
请输入入住时间(yyyy.MM.dd H:mm):2014.02.14 14:02
请输入退房时间(yyyy.MM.dd H:mm):2014.02.17 11:00
1.创建房客信息 2.查询房客信息 3.办理入住 4.返回:
4
1.查房  2.订房 3.退房 4.登出:
1
*****查房*****
1.获取可用房间  2.获取可用房间（床位数） 3.获取可用房间（类型）4.返回:
1
Room [id=3, categoryId=3, createTime=2013-09-26 13:02:49.0, position=南楼, serialNumber=R2011, status=Free]
1.获取可用房间  2.获取可用房间（床位数） 3.获取可用房间（类型）4.返回:
4
1.查房  2.订房 3.退房 4.登出:
3
*****退房*****
1.查询房客信息 2.办理退房 3.查看入住历史信息 4.返回:
3
1.获取所有入住历史信息 2.查询入住历史信息(姓名) 3.返回:
1
RentHistory [id=3, amount=-1.0, createTime=2013-09-26 13:07:04.0, endTime=Mon Feb 17 11:00:00 CST 2014, hotelGuestId=4, roomId=4, startTime=Fri Feb 14 14:02:00
CST 2014, ]
RentHistory [id=1, amount=1040.0, createTime=2013-09-17 13:28:00.0, endTime=Fri Sep 20 11:28:00 CST 2013, hotelGuestId=3, roomId=3, startTime=Wed Sep 18 13:28:0
0 CST 2013, ]
1.获取所有入住历史信息 2.查询入住历史信息(姓名) 3.返回:
3
1.查询房客信息 2.办理退房 3.查看入住历史信息 4.返回:
2
请输入房间号:R2012
请输入退房时间(yyyy.MM.dd H:mm):2014.02.15 5:20
1.查询房客信息 2.办理退房 3.查看入住历史信息 4.返回:
3
1.获取所有入住历史信息 2.查询入住历史信息(姓名) 3.返回:
1
RentHistory [id=3, amount=260.0, createTime=2013-09-26 13:07:48.0, endTime=Sat Feb 15 05:20:00 CST 2014, hotelGuestId=4, roomId=4, startTime=Fri Feb 14 14:02:00
 CST 2014, ]
RentHistory [id=1, amount=1040.0, createTime=2013-09-17 13:28:00.0, endTime=Fri Sep 20 11:28:00 CST 2013, hotelGuestId=3, roomId=3, startTime=Wed Sep 18 13:28:0
0 CST 2013, ]
1.获取所有入住历史信息 2.查询入住历史信息(姓名) 3.返回:
3
1.查询房客信息 2.办理退房 3.查看入住历史信息 4.返回:
4
1.查房  2.订房 3.退房 4.登出:
1
*****查房*****
1.获取可用房间  2.获取可用房间（床位数） 3.获取可用房间（类型）4.返回:
1
Room [id=3, categoryId=3, createTime=2013-09-26 13:02:49.0, position=南楼, serialNumber=R2011, status=Free]
Room [id=4, categoryId=4, createTime=2013-09-26 13:07:48.0, position=北楼, serialNumber=R2012, status=Free]
1.获取可用房间  2.获取可用房间（床位数） 3.获取可用房间（类型）4.返回:
4
1.查房  2.订房 3.退房 4.登出:
4
letian 正在登出...
再见
*****登录*****
用户名:密码:
D:\workspace\eWS\MT_Test_Hotels>
