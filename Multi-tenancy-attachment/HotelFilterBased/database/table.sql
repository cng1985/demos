CREATE DATABASE hotel DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
use hotel

drop table if exists tenant;
drop table if exists hotel_admin;
drop table if exists hotel_guest;
drop table if exists category;
drop table if exists room;
drop table if exists rent_history;

create table tenant
(
tenant_id varchar(30) not null,
name varchar(255) not null,
description text,
create_time timestamp,
CONSTRAINT PK_tenant PRIMARY KEY  (tenant_id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table hotel_admin
(
id int not null auto_increment,
login_id varchar(255) not null,
password  varchar(255) not null,
name varchar(255) not null,
email varchar(255) not null,
create_time timestamp,
tenant_id varchar(50) not null,
CONSTRAINT PK_user PRIMARY KEY  (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table hotel_guest
(
id int not null auto_increment,
name varchar(64) not null,
telephone varchar(64) not null,
address varchar(255) not null,
create_time timestamp,
tenant_id varchar(50) not null,
CONSTRAINT PK_customer PRIMARY KEY  (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table category
(
id int not null auto_increment,
name varchar(255) not null,
bed_num int,
price double,
create_time timestamp,
tenant_id varchar(50) not null,
CONSTRAINT PK_category PRIMARY KEY  (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table room
(
id int not null auto_increment,
serial_number varchar(255),
position   varchar(255),
category_id int not null,
status varchar(50) not null,
create_time timestamp,
tenant_id varchar(50) not null,
CONSTRAINT PK_room PRIMARY KEY  (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table rent_history
(
id int not null auto_increment,
start_time datetime not null,
end_time datetime not null,
room_id int not null,
amount double not null,
hotel_guest_id int not null,
create_time timestamp,
tenant_id varchar(50) not null,
CONSTRAINT PK_history PRIMARY KEY  (id)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8;

create unique index index_hotel_admin_login_id on hotel_admin
(
   login_id
);

alter table hotel_admin add constraint FK_hotel_admin_tenant_id foreign key (tenant_id)
      references tenant (tenant_id) on delete restrict on update restrict;
      
alter table hotel_guest add constraint FK_hotel_guest_tenant_id foreign key (tenant_id)
      references tenant (tenant_id) on delete restrict on update restrict;
      
alter table category add constraint FK_category_tenant_id foreign key (tenant_id)
      references tenant (tenant_id) on delete restrict on update restrict;      
      
alter table room add constraint FK_room_tenant_id foreign key (tenant_id)
      references tenant (tenant_id) on delete restrict on update restrict;  
      
alter table room add constraint FK_room_category_id foreign key (category_id)
      references category (id) on delete restrict on update restrict;

alter table rent_history add constraint FK_rent_history_hotel_guest_id foreign key (hotel_guest_id)
      references hotel_guest (id) on delete restrict on update restrict;
      
alter table rent_history add constraint FK_rent_history_room_id foreign key (room_id)
      references room (id) on delete restrict on update restrict;
      
alter table rent_history add constraint FK_rent_history_tenant_id foreign key (tenant_id)
      references tenant (tenant_id) on delete restrict on update restrict;
      