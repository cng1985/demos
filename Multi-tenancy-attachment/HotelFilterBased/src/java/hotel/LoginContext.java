package hotel;

import hotel.model.HotelAdmin;
import hotel.model.Tenant;

import java.util.HashMap;
import java.util.Map;

public class LoginContext {
	//实际应用中可以从用户的登录信息中取得当前用户的tenantId
	private static Map<String, HotelAdmin> map = new HashMap<String, HotelAdmin>();

	static {
		Tenant tenant = new Tenant();
		tenant.setTenantId("XIN_YA");
		tenant.setName("新亚酒店");

		HotelAdmin hotelAdmin = new HotelAdmin();
		hotelAdmin.setId(1);
		hotelAdmin.setLoginId("Victor");
		hotelAdmin.setTenant(tenant);
		login(hotelAdmin);
	}

	public static void login(HotelAdmin hotelAdmin) {
		map.put("hotelAdmin", hotelAdmin);
	}

	public static String getLoginId() {
		HotelAdmin hotelAdmin = map.get("hotelAdmin");
		return hotelAdmin.getLoginId();
	}

	public static String getTenantId() {
		HotelAdmin hotelAdmin = map.get("hotelAdmin");
		Tenant tenant = hotelAdmin.getTenant();
		return tenant.getTenantId();
	}

	public static HotelAdmin getHotelAdmin() {
		return map.get("hotelAdmin");
	}

}
