package hotel;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import hotel.dao.RoomDao;
import hotel.dao.hibernate.RoomDaoHibernate;
import hotel.model.Room;

public class HotelServiceMain {

	private RoomDao roomDao;

	public HotelServiceMain() {
		this.roomDao = new RoomDaoHibernate();
	}

	public List<Room> findRooms(int bedNum, String status) {
		return roomDao.queryRoom(bedNum, status);
	}

	public void bookRoomExample(Integer roomId, Integer hotelGuestId, Date startTime, Date endTime) {
		roomDao.bookRoom(roomId, hotelGuestId, startTime, endTime);
	}
	
	public void checkoutRoomExample(Integer roomId, Integer hotelGuestId, Date startTime, Date endTime, double amount) {
		roomDao.checkoutRoom(roomId, hotelGuestId, startTime, endTime, amount);
	}

	public static void main(String[] args) {
		HotelServiceMain hotelService = new HotelServiceMain();
		int hotelGuestId = 1;
		int bedNum = 2;
		Calendar calendar = Calendar.getInstance();
		Date startTime = calendar.getTime();
		calendar.roll(Calendar.DAY_OF_YEAR, 3);
		Date endTime = calendar.getTime();
		Integer selectedRoomId = null;  
		
		//查询房间
		List<Room> rooms = hotelService.findRooms(bedNum, "Free");
		if (rooms.size() == 0) {
			System.out.println("没有满足条件的可用房间！");
			return;
		}
		
		System.out.println("======当前可用房间列表======");
		for (Room room : rooms) {
			System.out.println(room.toString());
		}
		
		//预订房间
		Room selectedRoom = rooms.get(0);
		selectedRoomId = selectedRoom.getId();
		hotelService.bookRoomExample(selectedRoom.getId(), hotelGuestId, startTime, endTime);
		
		//退房结账
		double amount = 300.00;
		hotelService.checkoutRoomExample(selectedRoomId, hotelGuestId, startTime, endTime, amount);
	}

}
