package hotel.model;

public class Room extends BaseObject {

	private String serialNumber;
	private String position;
	private Category category;
	private String status;
	private Tenant tenant;

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	@Override
	public String toString() {
		return "Room [ID=" + getId() + ", 房间编号=" + serialNumber + ", 位置=" + position + ", 床数=" + category.getBedNum()
				+ ", 状态=" + status + ", 租户=" + tenant.getName() + "]";
	}

}
