package hotel.model;

import hotel.Constants;

import java.io.Serializable;
import java.util.Date;

public class BaseObject implements Serializable {

	private Integer id;
	private Date createTime;
	private Tenant tenant;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Tenant getTenant() {
		return tenant;
	}

	public void setTenant(Tenant tenant) {
		this.tenant = tenant;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		BaseObject that = (BaseObject) o;
		if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		if (this.getId() != null && this.getId() != Constants.INVALID_ID) {
			int results = 17;
			results = 37 * results + this.getId();
			return results;
		} else {
			return super.hashCode();
		}

	}
}
