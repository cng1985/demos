package hotel.model;

import java.util.Date;

public class RentHistory extends BaseObject {

	private Date startTime;
	private Date endTime;
	private Room room;
	private double amount;
	private HotelGuest hotelGuest;

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	public Date getEndTime() {
		return endTime;
	}

	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public HotelGuest getHotelGuest() {
		return hotelGuest;
	}

	public void setHotelGuest(HotelGuest hotelGuest) {
		this.hotelGuest = hotelGuest;
	}

	@Override
	public String toString() {
		return "RentHistory [ID=" + getId() + ", 开始时间=" + startTime + ", 结束时间=" + endTime + ", Room ID=" + room.getId()
				+ ", 房间编号=" + room.getSerialNumber() + ", 房间状态=" + room.getStatus() + ", 金额=" + amount + "元, 客人="
				+ hotelGuest.getName() + "]";
	}

}
