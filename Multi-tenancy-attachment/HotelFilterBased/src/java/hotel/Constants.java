package hotel;

public final class Constants {

	private Constants() {
		// hide me
	}

	/**
	 * NULL Value for Integers
	 */
	public static final Integer INVALID_ID = 0;

}
