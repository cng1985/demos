package hotel.dao.hibernate;

import java.io.Serializable;

import org.hibernate.Session;

public class BaseDaoHibernate {

	public Session getSession() {
		Session session = HibernateUtil.currentSession();
		return session;
	}
	
	public void closeSession(){
		HibernateUtil.closeSession();
	}
	
	
	public void save(Object object){
		Session session = this.getSession();
		session.saveOrUpdate(object);
		closeSession();
	}
	
	public Object load(Class clazz, Serializable id){
		Session session = this.getSession();
		Object object = session.load(clazz, id);		
		closeSession();
		return object;
	}
	

}
