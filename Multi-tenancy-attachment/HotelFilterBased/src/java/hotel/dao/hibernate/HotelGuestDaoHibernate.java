package hotel.dao.hibernate;

import java.util.List;

import org.hibernate.Session;

import hotel.dao.CustomerDao;
import hotel.model.HotelGuest;

public class HotelGuestDaoHibernate extends BaseDaoHibernate implements CustomerDao {

	@Override
	public void save(HotelGuest hoteGuest) {
		super.save(hoteGuest);
	}

	@Override
	public HotelGuest load(Integer id) {
		return (HotelGuest)super.load(HotelGuest.class, id);
	}

	
	@Override
	@SuppressWarnings("unchecked")
	public List<HotelGuest> loadAll() {
		Session session = super.getSession();
		List<HotelGuest> list = session.createCriteria(HotelGuest.class).setCacheable(true).list();
		super.closeSession();
		return list;
		
	}

}
