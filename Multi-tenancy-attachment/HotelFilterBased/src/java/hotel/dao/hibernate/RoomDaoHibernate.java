package hotel.dao.hibernate;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

import hotel.LoginContext;
import hotel.dao.RoomDao;
import hotel.model.HotelGuest;
import hotel.model.RentHistory;
import hotel.model.Room;
import hotel.model.Tenant;

public class RoomDaoHibernate extends BaseDaoHibernate implements RoomDao {

	@Override
	public void save(Room room) {
		super.save(room);
	}

	@Override
	public Room load(Integer id) {
		return (Room) super.load(Room.class, id);
	}

	@Override
	@SuppressWarnings("unchecked")
	public List<Room> loadAll() {
		Session session = super.getSession();
		List<Room> list = session.createCriteria(Room.class).setCacheable(true).list();
		super.closeSession();
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<Room> queryRoom(int bedNum, String status) {
		Session session = super.getSession();
		String queryString = "from Room room" + " where room.status = :status and" + " room.category.bedNum = :bedNum";
		Query query = session.createQuery(queryString).setParameter("status", status).setParameter("bedNum", bedNum);
		List<Room> results = query.list();
		super.closeSession();
		return results;
	}

	public void bookRoom(Integer roomId, Integer hotelGuestId, Date startTime, Date endTime) {
		Session session = super.getSession();
		Transaction tx = session.beginTransaction();
		try {
			Room room = (Room) session.load(Room.class, roomId);
			room.setStatus("Booked");
			session.saveOrUpdate(room);

			RentHistory rentHistory = new RentHistory();
			rentHistory.setRoom(room);
			HotelGuest hotelGuest = (HotelGuest) session.load(HotelGuest.class, hotelGuestId);
			rentHistory.setHotelGuest(hotelGuest);
			rentHistory.setStartTime(startTime);
			rentHistory.setEndTime(endTime);
			Tenant tenant = LoginContext.getHotelAdmin().getTenant();
			rentHistory.setTenant(tenant);
			session.saveOrUpdate(rentHistory);
			tx.commit();
			
			System.out.println("======预订后======");
			session.evict(rentHistory);
			RentHistory rent = (RentHistory)session.load(RentHistory.class, rentHistory.getId());
			System.out.println(rent.toString());
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
			tx.rollback();
		} finally {
			super.closeSession();
		}
	}
	
	
	public void checkoutRoom(Integer roomId, Integer hotelGuestId, Date startTime, Date endTime, double amount) {
		Session session = super.getSession();
		Transaction tx = session.beginTransaction();
		try {
			Room room = (Room) session.load(Room.class, roomId);
			room.setStatus("Free");
			session.saveOrUpdate(room);
			
			String rentQuery ="from RentHistory rent"+ " where rent.room.id = :roomId and" + " rent.hotelGuest.id = :hotelGuestId and"
			+" rent.startTime = :startTime and " +" rent.endTime = :endTime";
			Query query = session.createQuery(rentQuery)
									.setParameter("roomId", roomId)
									.setParameter("hotelGuestId", hotelGuestId)
									.setParameter("startTime", startTime)
									.setParameter("endTime", endTime);
			RentHistory rentHistory = (RentHistory)query.uniqueResult();			
			rentHistory.setAmount(amount);			
			session.saveOrUpdate(rentHistory);
			System.out.println("======结账后======");
			System.out.println(rentHistory.toString());	
			tx.commit();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			tx.rollback();
		} finally {
			super.closeSession();
		}
	}

}
