package hotel.dao.hibernate;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import hotel.dao.CategoryDao;
import hotel.model.Category;
import hotel.model.Room;

public class CategoryDaoHibernate extends BaseDaoHibernate implements CategoryDao {

	@Override
	public void save(Category category) {
		super.save(category);
	}

	@Override
	public Category load(Integer id) {
		return (Category)super.load(Category.class, id);
	}
	
	@SuppressWarnings("unchecked")
	public List<Category> queryCategory(int bedNum) {
		Session session = super.getSession();
		List<Category> list = session.createCriteria(Category.class)
				.add(Restrictions.eq("bed_num", bedNum))
				.list();
		super.closeSession();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<Room> queryRoom(int bedNum) {
		List<Room> results = new ArrayList<Room>();
		Session session = super.getSession();
		List<Category> categories = session.createCriteria(Category.class)
				.add(Restrictions.eq("bed_num", bedNum))
				.list();
				
		for (Category category : categories){
			 Set<Room> rooms = category.getRooms();
			 results.addAll(rooms);
		}
		super.closeSession();
		return results;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Category> loadAll() {
		Session session = super.getSession();		
		List<Category> list = session.createCriteria(Category.class).setCacheable(true).list();
		super.closeSession();
		return list;
	}

}
