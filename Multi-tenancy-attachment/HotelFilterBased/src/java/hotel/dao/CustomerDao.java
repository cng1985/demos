package hotel.dao;

import java.util.List;

import hotel.model.HotelGuest;

public interface CustomerDao {

	public void save(HotelGuest customer);

	public HotelGuest load(Integer id);

	public List<HotelGuest> loadAll();

}
