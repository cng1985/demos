package hotel.dao;

import hotel.model.Room;

import java.util.Date;
import java.util.List;

public interface RoomDao {

	public void save(Room room);

	public Room load(Integer id);

	public List<Room> queryRoom(int bedNum, String status);

	public List<Room> loadAll();

	public void bookRoom(Integer roomId, Integer hotelGuestId, Date startTime, Date endTime);

	public void checkoutRoom(Integer roomId, Integer hotelGuestId, Date startTime, Date endTime, double amount);

}
