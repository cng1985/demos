package hotel.dao;

import hotel.model.Category;

import java.util.List;

public interface CategoryDao {

	public void save(Category room);

	public Category load(Integer id);

	public List<Category> loadAll();

}
