package com.ada;

import com.itextpdf.html2pdf.HtmlConverter;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

import java.io.*;

public class HtmlApp {

    public static void main(String[] args) throws IOException {
        InputStream stream = null;
        HttpResponse response = HttpRequest.get("http://www.newbyte.ltd/").send();
        stream = new ByteArrayInputStream(response.bodyBytes());
        File pdfDest = new File("e:\\output.pdf");
        HtmlConverter.convertToPdf(stream, new FileOutputStream(pdfDest));
    }
}
