package com.ada;

import com.itextpdf.text.*;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfWriter;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws FileNotFoundException, DocumentException, IOException, FontFormatException {
        System.out.println("Hello World!");

        Document document = new Document(PageSize.A4, 50, 50, 50, 50);

        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("d:\\ITextTest.pdf"));

        // 3.打开文档
        BaseFont bfChinese = BaseFont.createFont("STSongStd-Light", "UniGB-UCS2-H", false);

        Font fontChinese = new Font(bfChinese, 12, Font.NORMAL, BaseColor.BLACK);

        Font fontChinese18 = new Font(bfChinese, 18, Font.NORMAL, BaseColor.BLACK);


        document.open();

        document.addTitle("工作计划");
        document.add(new Phrase("工作计划.", fontChinese18));
        document.add(new Paragraph("陈联高 2016年11月21日", fontChinese18));

        Paragraph text = new Paragraph("iText中用文本块(Chunk)、短语(Phrase)和段落(paragraph)处理文本。" +
                "文本块(Chunk)是处理文本的最小单位，有一串带格式（包括字体、颜色、大小）的字符串组成。如以下代码就是产生一个字体为HELVETICA、大小为10、带下划线的字符串", fontChinese);
        text.setFirstLineIndent(30);
        document.add(text);
        document.add(new Paragraph("__________________________________________", fontChinese18));
        document.close();


    }
}
