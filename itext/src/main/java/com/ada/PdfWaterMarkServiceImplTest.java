package com.ada;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Base64;


public class PdfWaterMarkServiceImplTest {
 
    public static void main(String[] args) throws Exception {
        InputStream is = Files.newInputStream(Paths.get("D:\\ITextTest.pdf"));
        OutputStream outputStream = Files.newOutputStream(Paths.get("D:\\ITextTest1.pdf"));
        ArrayList<WaterMarkInfoRequest> requests = new ArrayList<>();
 
 
        WaterMarkInfoRequest waterMarkInfo = addSign(1);
        WaterMarkInfoRequest waterMarkInfo2 = addSign(2);
 
 
        requests.add(waterMarkInfo);
        requests.add(waterMarkInfo2);
        //调用你自己服务的pdf水印方法
        new WaterApp().signPdf(is, () -> outputStream, requests);
        outputStream.close();
    }
 
    private static WaterMarkInfoRequest addSign(Integer pageNum) {
        WaterMarkInfoRequest waterMarkInfo = new WaterMarkInfoRequest();
 
        ImageWaterMarkRequest imageWaterMark = new ImageWaterMarkRequest();
        imageWaterMark.setPageNo(pageNum);
        imageWaterMark.setPositionY(100);
        imageWaterMark.setPositionX(20);
        imageWaterMark.setTop(true);
        imageWaterMark.setAlpha(1.0f);
        imageWaterMark.setBase64Str(fileToBase64("E:\\aa.jpg"));

        waterMarkInfo.setImageWaterMark(imageWaterMark);
 
        TextWaterMarkRequest textWaterMark = new TextWaterMarkRequest();
        textWaterMark.setText("我是一个水印");
        textWaterMark.setRotation(30);
        textWaterMark.setAlpha(0.1f);
        textWaterMark.setFontSize(24);
        textWaterMark.setIntervalHorizontal(10);
        textWaterMark.setIntervalVertical(10);
        textWaterMark.setFontColor("800000");
        textWaterMark.setTop(true);
        textWaterMark.setPageNo(pageNum);
 
        waterMarkInfo.setTextWaterMark(textWaterMark);
 
        return waterMarkInfo;
    }
    private static String fileToBase64(String filePath)  {
        File file = new File(filePath);
        byte[] fileContent = new byte[(int) file.length()];

        try (FileInputStream fis = new FileInputStream(file)) {
            fis.read(fileContent);
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return Base64.getEncoder().encodeToString(fileContent);
    }
    private static String convertImageToBase64(String imagePath) {

        String base64Image = null;
        try {
            BufferedImage image = ImageIO.read(new File(imagePath));
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            // 将图像写入字节数组输出流
            ImageIO.write(image, "png", baos);

            // 将字节数组输出流转换为Base64编码的字符串
            base64Image = Base64.getEncoder().encodeToString(baos.toByteArray());

            baos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return base64Image;
    }
 
}