package com.ada;

import lombok.Data;

@Data
public class WaterMarkInfoRequest {
    //@ApiModelProperty(value = "文字水印信息")
    private TextWaterMarkRequest textWaterMark;
    //@ApiModelProperty(value = "图片水印信息")
    private ImageWaterMarkRequest imageWaterMark;
}