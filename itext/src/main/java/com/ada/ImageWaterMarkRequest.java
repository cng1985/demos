package com.ada;

import lombok.Data;

@Data
public class ImageWaterMarkRequest{
 
    //@ApiModelProperty(value = "需要添加的页码，默认所有页码都添加")
    private Integer pageNo;
    //@ApiModelProperty(value = "水平位置  左下角坐标(0,0)")
    private Integer positionX;
    //@ApiModelProperty(value = "垂直位置  左下角坐标(0,0)")
    private Integer positionY;
   // @ApiModelProperty(value = "指定图像宽度  ")
    private Integer scaleAbsoluteX;
    //@ApiModelProperty(value = "指定图像高度 ")
    private Integer scaleAbsoluteY;
    //@ApiModelProperty(value = "缩放比例 与指定尺寸冲突 优先使用指定图像尺寸")
    private Integer scalePercent;
   // @ApiModelProperty(value = "图片的二进制数据")
   // @NotNull
    private String base64Str;
    //@ApiModelProperty(value = "透明度  0--1")
    private Float alpha;
 
    //@ApiModelProperty(value = "透明度  0--1",hidden = true)
    private Float opacity;
   // @ApiModelProperty(value = "是否水印打在最顶层  默认true")
    private Boolean top;
}