package com.ada;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Image;
import com.itextpdf.text.pdf.*;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Base64;
import java.util.List;
import java.util.function.Supplier;

public class WaterApp {

    public void signPdf(InputStream is, Supplier<OutputStream> os, List<WaterMarkInfoRequest> signList) {
        PdfStamper stamper = null;
        PdfReader reader = null;
        try {
            reader = new PdfReader(is);
            stamper = new PdfStamper(reader, os.get());
            for (WaterMarkInfoRequest s : signList) {
                TextWaterMarkRequest textWaterMark = s.getTextWaterMark();
                if (textWaterMark != null) {
                    //添加文字
                    BaseFont baseFont = null;
                    try {
                        baseFont = BaseFont.createFont("STSong-Light", "UniGB-UCS2-H", BaseFont.NOT_EMBEDDED);
                    } catch (DocumentException | IOException e) {
                        //log.info("pdf签名获取字体信息异常:{}", e.getMessage(), e);
                        //throw new BusinessException(IO_ERROR.toErrorInfos("pdf签名获取字体信息异常"));
                    }
                    PdfContentByte content = stamper.getOverContent(textWaterMark.getPageNo());
                    PdfGState gState = new PdfGState();
                    //设置透明度
                    gState.setFillOpacity(textWaterMark.getAlpha());
                    content.setGState(gState);
                    content.beginText();
                    //设置字体
                    content.setFontAndSize(baseFont, textWaterMark.getFontSize());
                    //设置颜色
                    content.setColorFill(new BaseColor(0xFF << 24 | Integer.parseInt(textWaterMark.getFontColor(), 16)));

                    //设置水印内容和位置
                    content.showTextAligned(Element.ALIGN_MIDDLE, textWaterMark.getText(), textWaterMark.getIntervalVertical(), textWaterMark.getIntervalHorizontal(), textWaterMark.getRotation());


                    float pageHeight = reader.getPageSize(textWaterMark.getPageNo()).getHeight();
                    float pageWidth = reader.getPageSize(textWaterMark.getPageNo()).getWidth();

                    float columnWidth = pageWidth / 3; // 每列宽度
                    float rowHeight = 100; // 每行高度，根据需要调整
                    int num= (int) rowHeight;
                    for (int i = 0; i < num; i++) {
                        for (int j = 0; j < 3; j++) {
                            float x=j*columnWidth+20;
                            float y=i*rowHeight;
                            content.showTextAligned(Element.ALIGN_MIDDLE, textWaterMark.getText(), x, y, textWaterMark.getRotation());
                        }


                    }

                    System.out.println(pageHeight/50);

                    content.endText();

                }
                ImageWaterMarkRequest imageWaterMark = s.getImageWaterMark();
                if (imageWaterMark != null) {
                    //添加图片
                    PdfContentByte content = stamper.getOverContent(imageWaterMark.getPageNo());
                    PdfGState gState = new PdfGState();
                    //设置透明度
                    gState.setFillOpacity(imageWaterMark.getAlpha());
                    //image
                    byte[] imageBytes = Base64.getDecoder().decode(imageWaterMark.getBase64Str());
                    Image image = Image.getInstance(imageBytes);
                    image.setAbsolutePosition(imageWaterMark.getPositionX(), imageWaterMark.getPositionY());
                    if (imageWaterMark.getScaleAbsoluteX() != null && imageWaterMark.getScaleAbsoluteY() != null) {
                        image.scaleToFit(imageWaterMark.getScaleAbsoluteX(), imageWaterMark.getScaleAbsoluteY());
                    } else if (imageWaterMark.getScalePercent() != null && imageWaterMark.getScalePercent() > 0) {
                        image.scalePercent(imageWaterMark.getScalePercent());
                    }
                    content.addImage(image);
                    content.stroke();
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
            //log.info("添加签名失败：{}", e.getLocalizedMessage(), e);
            //WM_WRITE_ERR.throwException();
        } finally {
            if (null != stamper) {
                try {
                    stamper.close();
                } catch (Exception e) {
                   // log.info("关闭失败：{}", e.getMessage(), e);
                }
            }
            if (null != reader) {
                reader.close();
            }
        }

    }
}
