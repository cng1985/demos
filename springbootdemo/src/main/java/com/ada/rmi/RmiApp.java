package com.ada.rmi;

import com.ada.hessian.MessageApi;
import org.springframework.remoting.rmi.RmiProxyFactoryBean;

public class RmiApp {

    public static void main(String[] args) {
        RmiProxyFactoryBean bean = new RmiProxyFactoryBean();
        bean.setServiceUrl("rmi://127.0.0.1:8083/messageApi");
        bean.setServiceInterface(MessageApi.class);
        bean.setRefreshStubOnConnectFailure(true);



        try {
            bean.afterPropertiesSet();
            MessageApi helloHessian = (MessageApi) bean.getObject();
            String response = helloHessian.say("ada66");
            System.out.println("=======> " + response + " <=======");

            Long time = System.currentTimeMillis();
            int num = 50000;
            for (int i = 0; i < num; i++) {
                StringBuffer buffer = new StringBuffer();
                for (int j = 0; j < 1000; j++) {
                    buffer.append("ada" + i);
                }
                //request.setCallback(item->item+":66");
                String res = helloHessian.say(buffer.toString());
                //System.out.println(res);
            }
            time = System.currentTimeMillis() - time;
            System.out.println(time);
            System.out.println(num / (time / 1000.0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
