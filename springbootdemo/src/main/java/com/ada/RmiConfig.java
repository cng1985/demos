package com.ada;

import com.ada.hessian.MessageApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;

import javax.annotation.Resource;
import java.rmi.RemoteException;

@Configuration
public class RmiConfig {

    @Resource
    private MessageApi messageApi;

    @Bean
    public RmiServiceExporter RmiServiceExporter() {
        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setService(messageApi);
        exporter.setServiceName("messageApi");
        exporter.setServiceInterface(MessageApi.class);
        exporter.setRegistryPort(8083);//注意这里必须是注册端口
        try {
            exporter.afterPropertiesSet();
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        return exporter;
    }
}
