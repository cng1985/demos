package com.ada.hessian;

import com.caucho.hessian.client.HessianProxyFactory;

public class HessianProxyFactoryUtil {


    /**
     * 获取调用端对象
     *
     * @param clazz 实体对象泛型
     * @param url   客户端url地址
     * @param <T>
     * @return 业务对象
     */
    public static <T> T getHessianClientBean(Class<T> clazz, String url) throws Exception {
        // 客户端连接工厂,这里只是做了最简单的实例化，还可以设置超时时间，密码等安全参数
        HessianProxyFactory factory = new HessianProxyFactory();

        return (T) factory.create(clazz, url);
    }

    //
    public static void main1(String[] args) {

        // 服务器暴露出的地址
        String url = "http://localhost:8080/helloHessian.do";

        // 客户端接口，需与服务端对象一样
        try {
            MessageApi helloHessian = HessianProxyFactoryUtil.getHessianClientBean(MessageApi.class, url);
            String msg = helloHessian.say("你好");

            System.out.println(msg);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void main(String[] args) {
        try {
            String url = "http://localhost:8080/helloHessian.do";

            MessageApi helloHessian = HessianProxyFactoryUtil.getHessianClientBean(MessageApi.class, url);
            String response = helloHessian.say("ada66");
            System.out.println("=======> " + response + " <=======");

            Long time = System.currentTimeMillis();
            int num = 10000;
            for (int i = 0; i < num; i++) {
                StringBuffer buffer = new StringBuffer();
                for (int j = 0; j < 1000; j++) {
                    buffer.append("ada" + i);
                }
                //request.setCallback(item->item+":66");
                String res = helloHessian.say(buffer.toString());
                //System.out.println(res);
            }
            time = System.currentTimeMillis() - time;
            System.out.println(time);
            System.out.println(num / (time / 1000.0));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}