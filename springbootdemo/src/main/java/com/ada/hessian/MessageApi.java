package com.ada.hessian;

public interface MessageApi {

    String say(String msg);
}
