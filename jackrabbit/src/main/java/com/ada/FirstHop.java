package com.ada;

import org.apache.jackrabbit.commons.JcrUtils;

import javax.jcr.*;

/**
 * Created by ada on 2016/12/13.
 */
public class FirstHop {

    /**
     * The main entry point of the example application.
     *
     * @param args command line arguments (ignored)
     * @throws Exception if an error occurs
     */
    public static void main(String[] args) throws Exception {
        Repository repository = JcrUtils.getRepository();
        Session session = repository.login(new GuestCredentials());
        try {
            String user = session.getUserID();
            String name = repository.getDescriptor(Repository.REP_NAME_DESC);
            System.out.println(
                    "Logged in as " + user + " to a " + name + " repository.");

            Node root = session.getRootNode();
            NodeIterator iterator = root.getNodes();
            while (iterator.hasNext()) {
                Node node = iterator.nextNode();
                System.out.println(node.getPath());
            }

            Node node = root.getNode("hello/world");
            System.out.println(node.getPath());
            System.out.println(node.getProperty("message").getString());
        } finally {
            session.logout();
        }
    }
}
