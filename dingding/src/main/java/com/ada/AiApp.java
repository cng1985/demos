package com.ada;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiSmartworkHrmEmployeeQueryonjobRequest;
import com.dingtalk.api.request.OapiSmartworkHrmEmployeeQuerypreentryRequest;
import com.dingtalk.api.response.OapiSmartworkHrmEmployeeQueryonjobResponse;
import com.dingtalk.api.response.OapiSmartworkHrmEmployeeQuerypreentryResponse;
import com.taobao.api.ApiException;

public class AiApp {
    public static void main(String[] args) throws ApiException {

        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/smartwork/hrm/employee/queryonjob");
        OapiSmartworkHrmEmployeeQueryonjobRequest req = new OapiSmartworkHrmEmployeeQueryonjobRequest();
        req.setStatusList("2,3,5,-1");
        req.setOffset(0L);
        req.setSize(5L);
        OapiSmartworkHrmEmployeeQueryonjobResponse response = client.execute(req , App.getAccessToken());
        System.out.println(response.getBody());


    }

    private static void app() {
        try {
            DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/ai/mt/translate");
            OapiAiMtTranslateRequest req = new OapiAiMtTranslateRequest();
            req.setQuery("接收者");
            req.setSourceLanguage("zh");
            req.setTargetLanguage("en");
            OapiAiMtTranslateResponse rsp = client.execute(req, App.getAccessToken());
            System.out.println(rsp.getBody());
            System.out.println(rsp.getResult());


        } catch (ApiException e) {
            e.printStackTrace();
        }
    }
}
