package com.ada;

import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiGettokenRequest;
import com.dingtalk.api.request.OapiMessageSendToConversationRequest;
import com.dingtalk.api.response.OapiGettokenResponse;
import com.dingtalk.api.response.OapiMessageSendToConversationResponse;
import com.taobao.api.ApiException;

public class MessageApp {
    public static void main(String[] args) throws ApiException {
        DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/message/send_to_conversation");

        DingTalkClient client1 = new DefaultDingTalkClient("https://oapi.dingtalk.com/gettoken");

        OapiGettokenRequest request = new OapiGettokenRequest();
        request.setAppkey("dinghgipkq3i1vhmjtdo");
        request.setAppsecret("Gnd8W29vWLFDPFC16ClIn5HBbN2Mt3MzGpIicIkKk1tLaZfF5UAX8K7lzFr8RSEI");
        request.setHttpMethod("GET");
        OapiGettokenResponse response = client1.execute(request);
        System.out.println(response.getBody());

        OapiMessageSendToConversationRequest req = new OapiMessageSendToConversationRequest();
        req.setSender("manager2441");
        req.setCid("14ac70d94e79377b88aa5fc75759fe84");
        OapiMessageSendToConversationRequest.Msg msg = new OapiMessageSendToConversationRequest.Msg();

// 文本消息
        OapiMessageSendToConversationRequest.Text text = new OapiMessageSendToConversationRequest.Text();
        text.setContent("测试测试");
        msg.setText(text);
        msg.setMsgtype("text");
        req.setMsg(msg);

// 图片
        OapiMessageSendToConversationRequest.Image image = new OapiMessageSendToConversationRequest.Image();
        image.setMediaId("@lADOdvRYes0CbM0CbA");
        msg.setImage(image);
        msg.setMsgtype("image");
        req.setMsg(msg);

// 文件
        OapiMessageSendToConversationRequest.File file = new OapiMessageSendToConversationRequest.File();
        file.setMediaId("@lADOdvRYes0CbM0CbA");
        msg.setFile(file);
        msg.setMsgtype("file");
        req.setMsg(msg);


        OapiMessageSendToConversationRequest.Markdown markdown = new OapiMessageSendToConversationRequest.Markdown();
        markdown.setText("# 这是支持markdown的文本 \\n## 标题2  \\n* 列表1 \\n![alt 啊](https://img.alicdn.com/tps/TB1XLjqNVXXXXc4XVXXXXXXXXXX-170-64.png)");
        markdown.setTitle("首屏会话透出的展示内容");
        msg.setMarkdown(markdown);
        msg.setMsgtype("markdown");
        req.setMsg(msg);


        OapiMessageSendToConversationRequest.ActionCard actionCard = new OapiMessageSendToConversationRequest.ActionCard();
        actionCard.setTitle("是透出到会话列表和通知的文案");
        actionCard.setMarkdown("持markdown格式的正文内");
        actionCard.setSingleTitle("查看详情");
        actionCard.setSingleUrl("https://open.dingtalk.com");
        msg.setActionCard(actionCard);
        msg.setMsgtype("action_card");
        req.setMsg(msg);

// link消息
        OapiMessageSendToConversationRequest.Link link = new OapiMessageSendToConversationRequest.Link();
        link.setMessageUrl("https://www.baidu.com/");
        link.setPicUrl("@lADOdvRYes0CbM0CbA");
        link.setText("测试");
        link.setTitle("oapi");
        msg.setLink(link);
        msg.setMsgtype("link");
        req.setMsg(msg);

        OapiMessageSendToConversationResponse response1 = client.execute(req, response.getAccessToken());
        System.out.println(response1.getBody());
    }
}
