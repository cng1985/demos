package com.ada;

import org.redisson.Redisson;
import org.redisson.api.RScoredSortedSet;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

public class ScoreApp {
    public static void main(String[] args) {
        Config config = new Config();
//        config.useClusterServers()
//                // use "rediss://" for SSL connection
//                .addNodeAddress("redis://127.0.0.1:6379");
        config.useSingleServer().setAddress("redis://121.36.13.75:6379").setPassword("he1618").setDatabase(5);

        RedissonClient redisson = Redisson.create(config);
        RScoredSortedSet<Object> score = redisson.getScoredSortedSet("score");
        score.add(100,User.builder().age(18).build());
        score.add(37,User.builder().age(17).build());
        redisson.shutdown();
    }
}
