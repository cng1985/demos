package com.ada;

import org.redisson.Redisson;
import org.redisson.api.RBucket;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

import java.util.Set;

public class RedisApp {


    public static void main(String[] args) {
        Config config = new Config();
//        config.useClusterServers()
//                // use "rediss://" for SSL connection
//                .addNodeAddress("redis://127.0.0.1:6379");
        config.useSingleServer().setAddress("redis://121.36.13.75:6379").setPassword("he1618").setDatabase(9);

        RedissonClient redisson = Redisson.create(config);
        RMap<String, Object> map = redisson.getMap("myMap");
        Set<String> keys = map.keySet();
        for (String key : keys) {
            System.out.println(key);
        }
        RBucket<Object> bucket = redisson.getBucket("a");

        map.put("ada1", "ada");

        map.put("user", User.builder().age(15).name("ada").build());

        redisson.shutdown();

// or read config from file
        //config = Config.fromYAML(new File("config-file.yaml"));
    }
}
