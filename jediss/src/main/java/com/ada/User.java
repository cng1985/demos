package com.ada;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    private String name;

    private Integer age;
}
