package com.ada;

import org.redisson.Redisson;
import org.redisson.api.*;
import org.redisson.config.Config;

import java.util.Set;

public class RedisSetApp {


    public static void main(String[] args) throws InterruptedException {
        Config config = new Config();
//        config.useClusterServers()
//                // use "rediss://" for SSL connection
//                .addNodeAddress("redis://127.0.0.1:6379");
        config.useSingleServer().setAddress("redis://121.36.13.75:6379").setPassword("he1618").setDatabase(10);

        RedissonClient redisson = Redisson.create(config);
        RList<Object> map = redisson.getList("user");
        for (Object o : map) {
            if (o instanceof User){
                User user= (User) o;
                System.out.println(user.getName());
            }
        }

        map.add(User.builder().age(15).name("ada").build());
        RGeo<Object> geo = redisson.getGeo("geo");
        geo.add(31.23d,45.112,User.builder().age(15).name("ada").build());

        RLock lock = redisson.getLock("lock");
        lock.lock();
        Thread.sleep(3000);
        lock.unlock();
        redisson.shutdown();

// or read config from file
        //config = Config.fromYAML(new File("config-file.yaml"));
    }
}
