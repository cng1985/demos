package com.ada;

import org.redisson.Redisson;
import org.redisson.api.*;
import org.redisson.config.Config;

import java.util.Set;

public class RedisTestApp {


    public static void main(String[] args) {
        Config config = new Config();
//        config.useClusterServers()
//                // use "rediss://" for SSL connection
//                .addNodeAddress("redis://127.0.0.1:6379");
        config.useSingleServer().setAddress("redis://121.36.13.75:6379").setPassword("he1618").setDatabase(9);

        RedissonClient redisson = Redisson.create(config);
        RBatch batch = redisson.createBatch();
        for (int i = 1; i < 10000*10000; i++) {
            RMapAsync<String, Object> map = batch.getMap("map:myMapA"+i);
            map.putAsync("ada1", "ada");

            map.putAsync("user", User.builder().age(15).name("ada").build());
            if (i%20000==0){
                BatchResult result = batch.execute();
                System.out.println(result.getSyncedSlaves());
                batch = redisson.createBatch();
                System.out.println(redisson.getKeys().count());
            }
        }


        redisson.shutdown();

// or read config from file
        //config = Config.fromYAML(new File("config-file.yaml"));
    }
}
