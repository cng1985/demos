package com.ada;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisShardInfo;

import java.util.Random;
import java.util.Set;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        JedisShardInfo info=new JedisShardInfo("106.12.217.120");
        //6379
        info.setPassword("goodluck123");
        Jedis jedis = new Jedis(info);

        jedis.set("foo", "bar");
        String value = jedis.get("foo");
        System.out.println(value);
        //jedis.zrevrange()

        jedis.zadd("adax",50.1,"adax");
        jedis.zadd("adax",52,"ada1");
        jedis.zadd("adax",50,"ada2");
        jedis.zadd("adax",50,"ada3");
        jedis.zadd("adax",54,"ada4");
        jedis.zadd("adax",100,"ada3");


        long time=System.currentTimeMillis();
        for (int i = 0; i < 500; i++) {
            jedis.zadd("adax",new Random().nextInt(1000),"ada"+new Random().nextInt(1000));
        }
        time=System.currentTimeMillis()-time;
        System.out.println(time);
        Set<String> rs=   jedis.zrevrange("adax",0,-1);
        for (int i = 0; i < 10; i++) {
            Object item=rs.toArray()[i];
            System.out.println(item);
        }
    }
}
