package com.ada.unirest;

import kong.unirest.Unirest;

public class App {

    public static void main(String[] args) {
        String res = "";

        res = Unirest.post("https://jobs.bytedance.com/api/v1/search/job/posts")
                .header("accept", "application/json")
                .field("userName", "admin")
                .field("userPassword", "dolphinscheduler123")
                .asString()
                .getBody();
        System.out.println(res);
    }
}
