package com.ada.okhttp;

import okhttp3.*;

import java.io.IOException;
import java.net.URLDecoder;

public class HttpApp {

    public static void main(String[] args) throws IOException {
        OkHttpClient client = new OkHttpClient();

        RequestBody requestBody= new FormBody.Builder()
                .add("offset", "10")
                .add("limit", "10")
                .build();
        Request request = new Request.Builder()
                .url("https://jobs.bytedance.com/api/v1/search/job/posts")
                .addHeader("content-type", "application/json;charset:utf-8")
                .addHeader("Referer","https://jobs.bytedance.com/experienced/position?keywords=&category=6704215864591255820&location=&project=&type=&job_hot_flag=&current=2&limit=10")
                .addHeader("User-Agent","Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0")
                .addHeader("x-csrf-token","aruWVVW1jcHXMBL6Nt6pRqbAsLvlcHumdzp8VtQzEBM=")
                // .post(RequestBody.create(MEDIA_TYPE_TEXT, postBody))
                .post(requestBody)
                .build();
        Response response = client.newCall(request).execute();
        System.out.println(response);
        if (response.isSuccessful()) {
            String json = response.body().string();
            System.out.println(json);
            String post ="";// JSON.parseObject(json).getString("postBody");
            System.out.println("转义之前：" + post);
            System.out.println("转义之后：" + URLDecoder.decode(post));
        }
    }
}
