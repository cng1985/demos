package com.ada;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class Demo {

  @EqualsAndHashCode.Exclude
  private String name;

  private Long id;

}
