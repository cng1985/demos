package com.ada;

import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateData;
import me.chanjar.weixin.mp.bean.template.WxMpTemplateMessage;
import me.chanjar.weixin.mp.config.WxMpConfigStorage;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws WxErrorException {
        System.out.println( "Hello World!" );

        WxMpService mpService=new WxMpServiceImpl();
        WxMpDefaultConfigImpl config=new WxMpDefaultConfigImpl();
        config.setAppId("");
        config.setSecret("");
        mpService.addConfigStorage("default",config);
        WxMpTemplateMessage message=new WxMpTemplateMessage();
        WxMpTemplateData userName=new WxMpTemplateData();
        userName.setName("");
        userName.setValue("");
        message.addData(userName);
        mpService.getTemplateMsgService().sendTemplateMsg(message);
    }
}
