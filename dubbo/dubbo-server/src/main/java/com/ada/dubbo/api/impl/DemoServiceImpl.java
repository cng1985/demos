package com.ada.dubbo.api.impl;

import com.ada.dubbo.api.DemoService;
import com.ada.dubbo.domain.User;

public class DemoServiceImpl implements DemoService {
    public String sayHello(String name) {
        return "Hello " + name;
    }

    @Override
    public User hello(Integer age) {
        User result=new User();
        result.age=age;
        return result;
    }
}
