package com.ada.dubbo.api;

import com.ada.dubbo.domain.User;

public interface DemoService {

    String sayHello(String name);


    User hello(Integer age);
}
