package com.ada;

import com.ada.dubbo.api.DemoService;
import com.ada.dubbo.domain.User;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(new String[]{"applicationContext.xml"});
        context.start();
        DemoService demoService = (DemoService) context.getBean("demoService"); // 获取远程服务代理
        for (int i = 0; i < 100; i++) {
            String hello = demoService.sayHello("world"); // 执行远程方法
            System.out.println(hello); // 显示调用结果
           User user= demoService.hello(i);
            System.out.println(user.age);
        }
    }
}
