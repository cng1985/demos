package com.ada;

import org.flowable.engine.IdentityService;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.ProcessEngineConfiguration;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.impl.cfg.StandaloneProcessEngineConfiguration;
import org.flowable.idm.api.User;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) {
        ProcessEngineConfiguration cfg = new StandaloneProcessEngineConfiguration()
                .setJdbcUrl("jdbc:mysql://192.168.0.199:3306/flowable?characterEncoding=UTF-8")
                .setJdbcUsername("tongna")
                .setJdbcPassword("tongna")
                .setJdbcDriver("com.mysql.jdbc.Driver")
                .setDatabaseSchemaUpdate(ProcessEngineConfiguration.DB_SCHEMA_UPDATE_TRUE);

        ProcessEngine processEngine = cfg.buildProcessEngine();
        IdentityService identityService = processEngine.getIdentityService();
        identityService.setUserInfo("ada","sd55f","sdfertsd");
    }
}
