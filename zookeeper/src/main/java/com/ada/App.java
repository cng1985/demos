package com.ada;

import org.apache.zookeeper.*;

import java.io.IOException;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main(String[] args) throws IOException, KeeperException, InterruptedException {
        ZooKeeper zk = new ZooKeeper("192.168.0.199:2181", 500000, new Watcher() {
            // 监控所有被触发的事件
            public void process(WatchedEvent event) {
                // dosomething
                System.out.println(event.getType());
            }
        });
        // zk.create("/root", "mydata".getBytes(), Ids.OPEN_ACL_UNSAFE,
        // CreateMode.PERSISTENT);
        // zk.create("/root/datas", ("childone").getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE,CreateMode.PERSISTENT);

//        long time=System.currentTimeMillis();
//        for (int i = 0; i < 10; i++) {
//            zk.create("/root/datas/data"+i, ("childone"+i).getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE,CreateMode.PERSISTENT);
//        }
//        time=System.currentTimeMillis()-time;
//        System.out.println(time);
        byte[] datas = zk.getData("/root", true, null);
        System.out.println(new String(datas));
        // 取得/root节点下的子节点名称,返回List<String>
        List<String> ss = zk.getChildren("/root/datas", true);
        for (String string : ss) {
            System.out.println(string);
        }
    }

    /**
     * @param zk
     * @throws KeeperException
     * @throws InterruptedException
     */
    private static void create(ZooKeeper zk) throws KeeperException, InterruptedException {
        // 创建一个节点root，数据是mydata,不进行ACL权限控制，节点为永久性的(即客户端shutdown了也不会消失)
        zk.create("/root", "mydata".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);

        // 在root下面创建一个childone znode,数据为childone,不进行ACL权限控制，节点为永久性的
        zk.create("/root/childone", "childone".getBytes(), ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.PERSISTENT);

        // 取得/root节点下的子节点名称,返回List<String>
        zk.getChildren("/root", true);

        // 取得/root/childone节点下的数据,返回byte[]
        zk.getData("/root/childone", true, null);

        // 修改节点/root/childone下的数据，第三个参数为版本，如果是-1，那会无视被修改的数据版本，直接改掉
        zk.setData("/root/childone", "childonemodify".getBytes(), -1);

        // 删除/root/childone这个节点，第二个参数为版本，－1的话直接删除，无视版本
        zk.delete("/root/childone", -1);

        // 关闭session
        zk.close();
    }
}
