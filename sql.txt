DELIMITER //

CREATE FUNCTION test_bit(input_number INT, position INT) RETURNS INT DETERMINISTIC
BEGIN
    DECLARE shifted_number INT;

    -- Left shift 1 by (position - 1) positions
    SET shifted_number = 1 << (position - 1);

    -- Perform bitwise AND operation
    RETURN IF((input_number & shifted_number) > 0, 1, 0);
END //

DELIMITER ;
