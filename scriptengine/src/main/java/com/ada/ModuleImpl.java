package com.ada;

/**
 * Created by ada on 2017/6/26.
 */
public class ModuleImpl implements IModule {
    public void msg(String msg) {
        System.out.println("model:"+msg);
    }

    @Override
    public User id(Integer num) {
        User result=new User();
        result.setAge(num);
        result.setName("ada");
        return result;
    }
}
