package com.ada;

import jdk.nashorn.api.scripting.ScriptObjectMirror;

import java.util.Map;


public class Logger {

    private static final ThreadLocal<StringBuffer> logValue = new ThreadLocal<StringBuffer>();

    private static final Logger LOG = new Logger();

    private Logger() {
    }

    public static Logger getInstance() {
        return LOG;
    }

    public static void init() {
        logValue.set(new StringBuffer());
    }

    public void info(Object msg) {
        if (msg instanceof ScriptObjectMirror){
            ScriptObjectMirror objectMirror=(ScriptObjectMirror)  msg;
            objectMirror.get("name");
        }
        System.out.println("msg:"+msg);
    }

    public static String getValue() {
        return logValue.get().toString();
    }

}
