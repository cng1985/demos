package com.ada;


import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;

/**
 * Hello world!
 */
public class App {
    public static void main(String[] args) throws InterruptedException {
        CacheManager cacheManager
                = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache("preConfigured",
                        CacheConfigurationBuilder.newCacheConfigurationBuilder(Long.class, String.class, ResourcePoolsBuilder.heap(10)))
                .build();
        cacheManager.init();

        Cache<Long, String> preConfigured =
                cacheManager.getCache("preConfigured", Long.class, String.class);

        Cache<Long, String> myCache = cacheManager.createCache("myCache",
                CacheConfigurationBuilder.newCacheConfigurationBuilder(Long.class, String.class, ResourcePoolsBuilder.heap(10)).build());

        myCache.put(1L, "da one!");

        Long time=System.currentTimeMillis();
        for (int i = 0; i < 10000000; i++) {
            myCache.put((long) i, "da one!"+i);

        }
        time=System.currentTimeMillis()-time;
        System.out.println(time);
        String value = myCache.get(1L);
        System.out.println(value);

        cacheManager.removeCache("preConfigured");

        Thread.sleep(10000);
        cacheManager.close();
    }
}
