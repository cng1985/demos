package com.ada;

import java.io.Serializable;

/**
 * Hello world!
 */
public class App implements Serializable {
    private static final long serialVersionUID =1l;
    private String name;

    @Override
    public String toString() {
        return "App{" +
                "name='" + name + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public static void main(String[] args) {
        System.out.println("Hello World!");
    }
}
