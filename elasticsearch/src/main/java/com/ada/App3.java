package com.ada;

import co.elastic.clients.elasticsearch.ElasticsearchClient;
import co.elastic.clients.elasticsearch._types.query_dsl.Query;
import co.elastic.clients.elasticsearch._types.query_dsl.TermQuery;
import co.elastic.clients.elasticsearch.core.IndexRequest;
import co.elastic.clients.elasticsearch.core.IndexResponse;
import co.elastic.clients.elasticsearch.core.SearchRequest;
import co.elastic.clients.elasticsearch.core.SearchResponse;
import co.elastic.clients.elasticsearch.core.search.Hit;
import co.elastic.clients.json.jackson.JacksonJsonpMapper;
import co.elastic.clients.transport.ElasticsearchTransport;
import co.elastic.clients.transport.rest_client.RestClientTransport;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.client.RestClient;

import java.io.IOException;

/**
 * Hello world!
 */
public class App3 {
    public static void main(String[] args) throws IOException {

        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("elastic", "Asdf2345@"));

        RestClient restClient = RestClient.builder(
                new HttpHost("es-aqyjlfrm.public.tencentelasticsearch.com", 9200)).setHttpClientConfigCallback(builder -> {
            return builder.setDefaultCredentialsProvider(credentialsProvider);
        }).build();

// Create the transport with a Jackson mapper
        ElasticsearchTransport transport = new RestClientTransport(
                restClient, new JacksonJsonpMapper());

// And create the API client
        ElasticsearchClient client = new ElasticsearchClient(transport);


        System.out.println(client.count());

        Product product = new Product("1","启动后 elastic search bat，访问9200失败，提示连接被重置", 123.0);

        IndexResponse response = client.index(i -> i
                .index("products")
                .id(product.getSku())
                .document(product)
        );
        System.out.println(response.version());

        IndexRequest.Builder<Product> indexReqBuilder = new IndexRequest.Builder<>();
        indexReqBuilder.index("product");
        indexReqBuilder.id(product.getSku());
        indexReqBuilder.document(product);

        response = client.index(indexReqBuilder.build());
        System.out.println(response.version());

        SearchRequest.Builder searchRequestBuilder = new SearchRequest.Builder();
        searchRequestBuilder.size(2);
        TermQuery termQuery = new TermQuery.Builder().field("code").value("重置").build();
        Query query = new Query.Builder().term(termQuery).build();
        searchRequestBuilder.query(query);
        searchRequestBuilder.index("products");
        SearchResponse<Product> temps = client.search(searchRequestBuilder.build(), Product.class);
        extracted(temps);

        SearchResponse<Product> search = client.search(s -> s
                        .index("products")
                        .query(q -> q
                                .term(t -> t
                                        .field("code")
                                        .value("bike")
                                )),
                Product.class);

        extracted(search);
        restClient.close();
    }

    private static void extracted(SearchResponse<Product> search) {
        System.out.println("***************************");
        System.out.println(search.profile());
        for (Hit<Product> hit : search.hits().hits()) {
            System.out.println(hit);
            //processProduct(hit.source());
        }
    }
}
