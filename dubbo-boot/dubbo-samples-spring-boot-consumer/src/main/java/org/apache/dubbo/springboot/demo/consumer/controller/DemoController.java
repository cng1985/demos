package org.apache.dubbo.springboot.demo.consumer.controller;

import org.apache.dubbo.config.annotation.DubboReference;
import org.apache.dubbo.springboot.demo.DemoService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @DubboReference
    private DemoService demoService;

    @RequestMapping("/index")
    public String index(){

        Long time=System.currentTimeMillis();
        int num=10000;
        for (int i = 0; i < num; i++) {
            StringBuffer buffer=new StringBuffer();
            for (int j = 0; j < 1000; j++) {
                buffer.append("ada"+i);
            }
            demoService.sayHello(buffer.toString());
            //System.out.println(res);
        }
        time=System.currentTimeMillis()-time;
        System.out.println(time);
        System.out.println(num/(time/1000.0));

        return "ada";
    }
}
