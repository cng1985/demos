package com.ada;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericBooleanPrefUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException, TasteException {
        System.out.println( "Hello World!" );

        DataModel dataModel=new FileDataModel(new File(""));

        UserSimilarity userSimilarity=new PearsonCorrelationSimilarity(dataModel);

        UserNeighborhood neighborhood=new ThresholdUserNeighborhood(0.0,userSimilarity,dataModel);

        UserBasedRecommender recommender=new GenericBooleanPrefUserBasedRecommender(dataModel,neighborhood,userSimilarity);

        List<RecommendedItem>  items=recommender.recommend(1L,3);
        for (RecommendedItem item : items) {
            System.out.println(item);
        }

    }
}
